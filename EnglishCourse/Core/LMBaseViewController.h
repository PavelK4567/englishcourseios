//  LMBaseViewController.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

@interface LMBaseViewController : UIViewController

+ (id)new;
+ (id)newFromXibNamed:(NSString *)xib;

@property(nonatomic)float viewWidth,viewHeight;

- (void)configureAppearance;
- (void)configureUI;
- (void)configureObservers;
- (void)configureNavigation;
- (void)configureData;
- (void)loadData;
- (void)layout;
- (void)dismissObservers;


- (void)addActivityIndicator;
- (void)addActivityIndicatorForFramePresent;
- (void)removeActivityIndicator;

- (void)makeDoneBtn;

@end
