//
//  AppDelegate.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/24/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMBaseViewController.h"
#import "SplashScreenViewController.h"
#import "MainViewController.h"
#import "LessonViewController.h"
#import <ASIHTTP/Reachability.h>

#import "SyncService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) UINavigationController *navigationController;
@property(weak, nonatomic) LMBaseViewController *baseController;
//@property (nonatomic, assign) NSInteger currentLevelIndex;
//@property (nonatomic, assign) NSIndexPath *currentModuleLessonIndex;
@property (nonatomic, assign) BOOL isCourseDataSynced;

@property (nonatomic, strong) MainViewController *mainController;
@property (nonatomic, strong) LessonViewController *lessonController;
@property (nonatomic) CFTimeInterval startTime;
@property (nonatomic) NSDate *startSessionDate;
@property (nonatomic) BOOL NoInternetConnectionPopUpShow,ServerUnreachablePopUpShow;

- (void)buildLoadingStack;
- (void)buildSplashStack;
- (void)buildMainStack;
- (void)buildMainStackWithLevelIndex:(NSInteger)levelIndex;
- (void)buildLessonStack:(NSArray *)arrayLevelData withIndex:(NSIndexPath *)indexPath :(NSInteger)levelIndex;
- (void)buildLessonStack:(NSArray *)arrayLevelData withLevel:(NSInteger)levelIndex section:(NSInteger)section unit:(NSInteger)unit;

- (void)backToLevelPage;
- (void)buildLoginStack;
- (void)presentLoginStack;

- (void)backToLevelPageAfterFinishThePrevious;

- (void)openLevelSelectionAfterFinishingLevels;

@end

#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])
