//  UIViewController+Extras.m
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "UIViewController+Extras.h"


@implementation UIViewController (Extras)

- (void)presentModalViewController:(UIViewController *)modalViewController withPushDirection:(NSString *)direction {
  [CATransaction begin];

  CATransition *transition = [CATransition animation];
  transition.type = kCATransitionPush;
  transition.subtype = direction;
  transition.duration = 0.25f;
  transition.fillMode = kCAFillModeForwards;
  transition.removedOnCompletion = YES;

  [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
  [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
  [CATransaction setCompletionBlock:^{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration * NSEC_PER_SEC)), dispatch_get_main_queue(),
                   ^{ [[UIApplication sharedApplication] endIgnoringInteractionEvents]; });
  }];

  [self presentViewController:modalViewController animated:NO completion:^{}];
  [CATransaction commit];
}

- (void)dismissModalViewControllerWithPushDirection:(NSString *)direction {
  [CATransaction begin];

  CATransition *transition = [CATransition animation];
  transition.type = kCATransitionPush;
  transition.subtype = direction;
  transition.duration = 0.4;
  transition.fillMode = kCAFillModeForwards;
  transition.subtype = kCATransitionFromLeft;
  transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
  transition.removedOnCompletion = YES;

  [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
  [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
  [CATransaction setCompletionBlock:^{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration * NSEC_PER_SEC)), dispatch_get_main_queue(),
                   ^{ [[UIApplication sharedApplication] endIgnoringInteractionEvents]; });
  }];

  [self dismissViewControllerAnimated:NO completion:^{}];
  [CATransaction commit];
}

#pragma mark -

- (BOOL)isModal {
  return ((self.presentingViewController && self.presentingViewController.presentedViewController == self) ||
          // or if I have a navigation controller, check if its parent modal view controller is self navigation controller
          (self.navigationController && self.navigationController.presentingViewController &&
           self.navigationController.presentingViewController.presentedViewController == self.navigationController) ||
          // or if the parent of my UITabBarController is also a UITabBarController class, then there is no way to do that, except by using a modal presentation
          [[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]]);
}


- (void)autorotate {
  UIViewController *c = [[UIViewController alloc] init];
  [self presentViewController:c animated:NO completion:^{}];
  [self dismissViewControllerAnimated:NO completion:^{}];
}

@end
