//  NSDictionary+Extras.m
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "NSDictionary+Extras.h"


@implementation NSDictionary (Extras)

- (NSString *)queryString {
  assert_valid_zero([self count]) NSMutableString *query = [NSMutableString string];
  for (NSString *parameter in [self allKeys]) {
    [query appendFormat:@"&%@=%@", [parameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding],
                        [[self valueForKey:parameter] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
  }
  return [NSString stringWithFormat:@"%@", [query substringFromIndex:1]];
}

- (id)parseField:(NSString *)fieldKey withClass:(Class)fieldClass {
  id field = [self objectForKey:fieldKey];
  return [field isKindOfClass:fieldClass] ? field : nil;
}

@end
