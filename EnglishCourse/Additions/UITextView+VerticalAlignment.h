//
//  UITextView+VerticalAlignment.h
//  VerbalShoppingList
//
//  Created by Tom Susel on 1/9/14.
//

#import <UIKit/UIKit.h>

@interface UITextView (VerticalAlignment)

- (void)alignToTop;

/**
  Client should call this to stop KVO.
*/
- (void)disableAlignment;

@end
