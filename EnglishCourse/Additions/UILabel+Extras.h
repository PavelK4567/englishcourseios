//  UILabel+Extras.h
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


@interface UILabel (Extras)

+ (void)decreaseHeightOfLabel:(UILabel *)label;
+ (void)adjustHeightOfLabel:(UILabel *)label maxHeight:(CGFloat)maxHeight;
+ (void)decreaseWidthOfLabel:(UILabel *)label;
+ (void)adjustWidthOfLabel:(UILabel *)label maxWidth:(CGFloat)maxWidth;

- (void)decreaseHeight;
- (void)decreaseWidth;
- (void)adjustHeightWithMaxHeight:(CGFloat)maxHeight;
- (void)adjustWidthWithMaxWidth:(CGFloat)maxWidth;
- (void)adjustHeightToFit;
- (void)adjustWidthToFit;


@end
