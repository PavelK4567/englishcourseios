//
//  UIView-Nib.m
//  RusTelecom
//
//  Created by Action Item on 11/13/11.
//  Copyright 2011 RosTelecom. All rights reserved.
//

#import "UIView-Nib.h"

@implementation UIView (Nib)

+ (instancetype)loadViewFromNib:(NSString *)nibName forClass:(Class)forClass
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) // not an ipad
	{
		NSString *ipadNib = [NSString stringWithFormat:@"%@~iPad", nibName];
        BOOL nibExists = [[NSBundle mainBundle] pathForResource:ipadNib ofType:@"nib"] != nil;
        if (nibExists)
        {
			nibName = ipadNib;
		}
	}

	NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] ;
	for(id currentObject in topLevelObjects)
	{
		if([currentObject isKindOfClass:forClass])
		{
			//[currentObject retain];
			//[topLevelObjects release];
			//return [currentObject autorelease];
      return currentObject ;
		}
	}
  //  [topLevelObjects release];
	return nil;
}

+ (instancetype)loadFromNib
{
	return [[self class] loadViewFromNib:NSStringFromClass([self class]) forClass:[self class]];
}

@end
