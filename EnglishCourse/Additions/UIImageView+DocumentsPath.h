//
//  UIImageView+DocumentsPath.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (DocumentsPath)

- (void)setImageFromDocumentsResourceFile:(NSString *)fileName;

@end
