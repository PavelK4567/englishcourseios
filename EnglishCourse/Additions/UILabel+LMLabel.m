//
//  UILabel+LMLabel.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/15/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "UILabel+LMLabel.h"

@implementation UILabel (LMLabel)

#pragma mark - adjustHeightWithMaxHeight

- (void)adjustHeightWithMaxHeight:(CGFloat)maxHeight
{
    CGRect frame;
    frame = self.frame;
    CGRect heightThatFits = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:self.font}
                                              context:nil];
    frame.size.height = ceil(MIN(heightThatFits.size.height, maxHeight));
    self.frame = frame;
}


#pragma mark - adjustHeightToFit

- (void)adjustHeightToFit
{
    [self adjustHeightWithMaxHeight:MAXFLOAT];
}


#pragma mark -
@end
