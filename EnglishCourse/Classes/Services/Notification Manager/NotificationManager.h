//
//  NotificationManager.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 1/20/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationManager : NSObject


+ (NotificationManager *)sharedInstance;
- (void)remindEveryDayNotifications;
- (void)cancelRemindEveryDayNotifications;

- (void)makeNotifications:(int)NumberOfNotification;
- (void)cancelAllNotifications;

@end

#define NOTIFICATION_MANAGER ((NotificationManager *)[NotificationManager sharedInstance])