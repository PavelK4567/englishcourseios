 //
//  NotificationManager.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 1/20/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "NotificationManager.h"
#import "UserStatistic.h"

@implementation NotificationManager

SINGLETON_GCD(NotificationManager)

-(void) EnableLocalNotificationIOS8
{
  UIApplication *app = [UIApplication sharedApplication];
  
  if ([app respondsToSelector:@selector(registerUserNotificationSettings:)])
  {
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
    [app registerUserNotificationSettings:settings];
    [app registerForRemoteNotifications];
  }
}

- (void)remindEveryDayNotifications
{
  [self EnableLocalNotificationIOS8];
  [self cancelRemindEveryDayNotifications];
  
  
  UILocalNotification *usageNotification = [UILocalNotification new];
  if (!usageNotification) {
    return;
  }

  
  NSDate *startDate = [NSDate date];
  NSDate *notificationDate = [startDate dateByAddingTimeInterval:D_MINUTE*1];
  
  usageNotification.fireDate = notificationDate;
  usageNotification.timeZone = [NSTimeZone defaultTimeZone];
  usageNotification.soundName = UILocalNotificationDefaultSoundName;
  usageNotification.applicationIconBadgeNumber = 1;
  usageNotification.userInfo = @{ @"notification_date" : notificationDate, @"type" : @"last_usage" };
  usageNotification.alertBody = @"Potsetnik";
  usageNotification.alertAction = @"OK";
  
  [[UIApplication sharedApplication] scheduleLocalNotification:usageNotification];
  
 // [[NSUserDefaults standardUserDefaults] setObject:notificationDate forKey:@"used"];
}

- (void)cancelRemindEveryDayNotifications
{
  [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
  NSArray *eventArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
  for (UILocalNotification *notification in eventArray) {
    NSDictionary *userInfoCurrent = notification.userInfo;
    NSString *notificationType = [NSString stringWithFormat:@"%@", userInfoCurrent[@"type"]];
    if ([notificationType isEqualToString:@"last_usage"]) {
      [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
  }
}

- (void)cancelAllNotifications
{
  [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
  NSArray *eventArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
  for (UILocalNotification *notification in eventArray) {
    [[UIApplication sharedApplication] cancelLocalNotification:notification];
  }
}


- (void)makeNotifications:(int)NumberOfNotification
{
  [self EnableLocalNotificationIOS8];
  [self cancelAllNotifications];
  if( ![[NSUserDefaults standardUserDefaults] boolForKey:@"NotificationEnabled"]){
    return;
  }else if(![USER_MANAGER isActive]){
    return;
  }else{
    
    NSDate* beginDate = [[NSUserDefaults standardUserDefaults]  objectForKey:kStartAplicationDate];
    int numOfValideDate = 0;
    int count = 0;
    
    while (numOfValideDate < NumberOfNotification) {
      count++;
      //DLog(@"XXX");
      NSDate *notificationDate = [beginDate dateByAddingTimeInterval:D_WEEK*count];
      //take next week day from instaling app
      if ([notificationDate timeIntervalSinceNow] > kParam044*D_DAY) {
        //TODO: function for levelScore
        //DLog(@"@@@");
        //int levelScore = 500;
        //BOOL levelComplete = YES;
        numOfValideDate++;
        
        UILocalNotification *usageNotification = [UILocalNotification new];
        if (!usageNotification) {
          return;
        }
        usageNotification.fireDate = notificationDate;
        usageNotification.timeZone = [NSTimeZone defaultTimeZone];
        usageNotification.soundName = UILocalNotificationDefaultSoundName;
        usageNotification.userInfo = @{ @"notification_date" : notificationDate, @"type" : @"last_usage" };
        
        if([USER_MANAGER userWeeklyUsage] < kParam046){
          usageNotification.alertBody = [NSString stringWithFormat:Localized(@"T540"),kParam047];
        }else if([USER_MANAGER userWeeklyUsage] < kParam046){
          usageNotification.alertBody = [NSString stringWithFormat:Localized(@"T541"),kParam047];;
        }else if([PROGRESS_MANAGER scoreForCurrentLevel] < kParam043){
          //TODO: set texts
          usageNotification.alertBody = [NSString stringWithFormat:Localized(@"T542"),[PROGRESS_MANAGER scoreForCurrentLevel],(int)(kParam043-[PROGRESS_MANAGER scoreForCurrentLevel])];
        }else if(![PROGRESS_MANAGER  currentLevelCompleted]){
          usageNotification.alertBody =  [NSString stringWithFormat:Localized(@"T543"),[PROGRESS_MANAGER alreadyCompletedLessonsInCurrentLevel],[PROGRESS_MANAGER lessonsForDiplomaInCurrentLevel]- [PROGRESS_MANAGER alreadyCompletedLessonsInCurrentLevel] ];
        }else{
          return;
        }
        usageNotification.alertAction = @"OK";
        [[UIApplication sharedApplication] scheduleLocalNotification:usageNotification];
      }
    }
    
  }
}

@end

