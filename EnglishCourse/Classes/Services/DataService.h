//
//  DataService.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/7/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModel.h"
#import "Constants.h"
#import "BaseEntity.h"
#import "BaseInstance.h"

#import "CourseStructure.h"
#import "LevelData.h"
#import "Level.h"
#import "Section.h"
#import "Unit.h"

#import "Characters.h"
#import "Character.h"

#define DATA_SERVICE ((DataService *)[DataService sharedInstance])

@interface DataService : NSObject

@property (nonatomic, strong) DataModel *dataModel;

+ (DataService *)sharedInstance;

- (void)saveDataModel:(DataModel *)dataModel;
- (DataModel *)loadAppDataModel;
- (void)loadDataModel;

- (void)deleteDataModel;

- (BaseEntity *)findCourseByID:(NSInteger)courseID;
- (NSArray *)findAllLevelsForCourse;
- (BaseEntity *)findLevelByID:(NSInteger)levelID;
- (NSArray *)findAllModulesFromLevel:(NSInteger)levelID;
- (BaseEntity *)findSpecificModuleFromLevelByID:(NSInteger)levelID moduleID:(NSInteger)moduleID;
- (NSArray *)findAllLessonsFromSpecificModuleInLevel:(NSInteger)levelID moduleID:(NSInteger)moduleID;
- (BaseEntity *)findSpecificLessonFromModuleInLevel:(NSInteger)levelID moduleID:(NSInteger)moduleID lessonID:(NSInteger)lessonID;
- (NSArray *)findAllInstancesFromSpecificLesson:(NSInteger)levelID moduleID:(NSInteger)moduleID lessonID:(NSInteger)lessonID;
- (BaseEntity *)findSpecificInstnaceFromLesson:(NSInteger)levelID moduleID:(NSInteger)moduleID lessonID:(NSInteger)lessonID instnaceID:(NSInteger)instanceID;

- (NSArray *)findAllCharacters;

- (Character *)findCharacterByID:(NSInteger)characterID;

- (BaseEntity *)replaceInstance:(BaseInstance *)oldInstance newInstnace:(BaseInstance *)newInstance;

- (NSInteger) numOfLessonInLevel:(NSInteger)levelID;

@end
