//
//  LMRequest.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/4/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <ASIHTTP/ASIHTTP.h>
#import "LMRequestManager.h"

enum {
    kRequestUpdateProgress = 0,
    kSendDiploma = 1,
    kDiplomaDetails = 2,
    kUserProgression = 3,
    kSessionStatistic = 4,
    kPlacementTest = 5
};
typedef NSUInteger API_REQUEST_TYPE;

@interface LMRequest : ASIHTTPRequest <NSCoding>

@property(nonatomic) API_REQUEST_TYPE requestType;
@property(nonatomic ,strong) NSDictionary *header;

@end
