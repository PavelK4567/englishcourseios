//
//  LMApiRequestManager.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/14/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LMRequest.h"
#import <ASIHTTP/ASINetworkQueue.h>

#define API_REQUEST_MANAGER ((LMApiRequestManager *)[LMApiRequestManager sharedInstance])

typedef void (^ApiResultBlock)(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary);
typedef void (^DiplomaDetailsResultBlock)(ASIHTTPRequest *asiHttpRequest, NSError *error, NSDictionary *responseDict);

@interface LMApiRequestManager : NSObject

@property (nonatomic, strong) NSMutableArray *requestList;
@property (retain) ASINetworkQueue *networkQueue;

+ (id)sharedInstance;
- (void)updateProgressForUser:(NSString *)msisdn
                      groupID:(NSString *)groupId
                    contentID:(NSString *)contentId
                       unitID:(NSString *)unitId
                    unitScore:(NSString *)unitScore
                  unitAchived:(NSString *)unitAchived
                     progress:(NSString *)progress
              CompletionBlock:(ApiResultBlock)completionBlock;

- (void)sendDiplomaForUser:(NSString *)msisdn
                   groupID:(NSString *)groupId
                 contentID:(NSString *)contentId
                   levelId:(NSString *)levelId
                salutation:(NSString *)salutation
                 firstName:(NSString *)firstName
                  lastName:(NSString *)lastName
                     email:(NSString *)email
                    resend:(NSString *)resend
           CompletionBlock:(ApiResultBlock)completionBlock;

- (void)diplomaDetailsForUser:(NSString *)msisdn
                      groupID:(NSString *)groupId
                    contentID:(NSString *)contentId
              CompletionBlock:(DiplomaDetailsResultBlock)completionBlock;

- (void)userProgression:(NSString *)msisdn
              contentId:(NSString *)contentId
             phoneModel:(NSString *)phoneModel
                 osType:(NSString *)osType
              osVersion:(NSString *)osVersion
                levelId:(NSString *)levelId
                groupId:(NSString *)groupId
              lessionId:(NSString *)lessionId
                skipped:(NSString *)skipped
                correct:(NSString *)correct
              inCorrect:(NSString *)inCorrect
        CompletionBlock:(ApiResultBlock)completionBlock;


- (void)placementTestForUser:(NSString *)msisdn
                   contentId:(NSString *)contentId
                     groupId:(NSString *)groupId
                  phoneModel:(NSString *)phoneModel
                      osType:(NSString *)osType
                   osVersion:(NSString *)osVersion
                   completed:(NSString *)completed
                       grade:(NSString *)grade
            recommendedLevel:(NSString *)recommendedLevel
                    dateTime:(NSString *)datetime
             CompletionBlock:(ApiResultBlock)completionBlock;

- (void)sessionStatisticForUser:(NSString *)msisdn
                        groupId:(NSString *)groupId
                      contentId:(NSString *)contentId
                       deviceId:(NSString *)deviceId
                      startDate:(NSString *)startDate
                        endDate:(NSString *)endDate
                CompletionBlock:(ApiResultBlock)completionBlock;

- (void)executeApiRequest:(LMRequest *)request
      withCompletionBlock:(ApiResultBlock)completionBlock;
- (NSMutableArray *)getRequestList;

- (void)doNetworkOperations;
- (BOOL)checkApiResponseStatus:(NSInteger)status;

@end
