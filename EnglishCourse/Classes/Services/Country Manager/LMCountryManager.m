//  CountryManager.m
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMCountryManager.h"


@interface LMCountryManager ()

@property(strong, nonatomic) NSMutableArray *map;

@end


@implementation LMCountryManager


#ifdef COUNTRY_SINGLETON
SINGLETON_GCD(LMCountryManager)
#endif


- (id)init {
  self = [super init];
  if (self) {
  }
  return self;
}

- (NSString *)countryNameFromMSISDN:(NSString *)msisdn {
  NSArray *filtered = [self.map filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ BEGINSWITH[c] phone", msisdn]];
  if (filtered && filtered.count) {
    return filtered[0][@"name"];
  }
  return 0;
}

- (NSString *)countryNameFromCode:(NSString *)code {
  NSArray *filtered = [self.map filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ LIKE[c] iso", code]];
  if (filtered && filtered.count) {
    return filtered[0][@"name"];
  }
  return 0;
}

- (NSString *)countryCodeFromMSISDN:(NSString *)msisdn {
  NSArray *filtered = [self.map filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ BEGINSWITH[c] phone", msisdn]];
  if (filtered && filtered.count) {
    return filtered[0][@"iso"];
  }
  return 0;
}

- (NSString *)countryCodeFromName:(NSString *)name {
  NSArray *filtered = [self.map filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ LIKE[c] name", name]];
  if (filtered && filtered.count) {
    return filtered[0][@"name"];
  }
  return 0;
}

- (NSString *)countryPrefixFromName:(NSString *)name {
  NSArray *filtered = [self.map filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ LIKE[c] name", name]];
  if (filtered && filtered.count) {
    return filtered[0][@"phone"];
  }
  return 0;
}

- (NSString *)countryPrefixFromCode:(NSString *)code {
  NSArray *filtered = [self.map filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ LIKE[c] iso", code]];
  if (filtered && filtered.count) {
    return filtered[0][@"phone"];
  }
  return 0;
}

@end
