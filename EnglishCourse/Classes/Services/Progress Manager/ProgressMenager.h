//
//  ProgressMenager.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/26/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserStatistic.h"
#import "DataModel.h"
#import "LevelData.h"
#import "ProgressFactory.h"
#import "LevelStatusData.h"
#import "UnitData.h"
#import "PageStatistic.h"

#define PROGRESS_MANAGER ((ProgressMenager *)[ProgressMenager sharedInstance])

@interface ProgressMenager : NSObject

@property (nonatomic, strong) UserStatistic *userStatistic;
@property (nonatomic, strong) UserStatistic *guestUserStatistic;

@property (nonatomic, assign) NSInteger currentLevelIndex;
@property (nonatomic, assign) NSInteger currentSectionIndex;
@property (nonatomic, assign) NSInteger currentUnitIndex;

- (id)initWithMSISDN:(NSString *)userMSISDN
          userLevels:(NSArray *)levels;
+ (id)sharedInstance;
- (void)saveUserStatistic:(UserStatistic *)userStatistic;
- (void)deleteStatistic;
- (void)transferPointsFromGuestToINAPUser;
- (UserStatistic *)getGuestUserStatistic;

- (UserStatistic *)getProgressForUser:(NSString *)msisdn;

- (InstanceStatistic *)getInstanceStatisticForLevel:(NSInteger)level
                                            section:(NSInteger)section
                                               unit:(NSInteger)unit
                                           instance:(NSInteger)instance
                                      userStatistic:(UserStatistic *)userStatistic;
- (PageStatistic *)getPageStatisticForLevel:(NSInteger)level
                                            section:(NSInteger)section
                                               unit:(NSInteger)unit
                                           instance:(NSInteger)instance
                                                page:(NSInteger)page
                                      userStatistic:(UserStatistic *)userStatistic;

- (NSArray *)getLessonInstanceStatisticsForLevel:(NSInteger)level
                                         section:(NSInteger)section
                                            unit:(NSInteger)unit
                                   userStatistic:(UserStatistic *)userStatistic;
- (void)saveGestUserStatistic:(UserStatistic *)statistic;
- (BOOL)isUnitAvailable:(NSInteger)level
                section:(NSInteger)section
                   unit:(NSInteger)unit;

- (void)updateProgressForUserLevel:(NSInteger)level
                           section:(NSInteger)section
                              unit:(NSInteger)unit
                       unitAchived:(NSInteger)unitAchived;


- (void)updateInstanceStatisticForLevel:(NSInteger)level
                                section:(NSInteger)section
                                   unit:(NSInteger)unit
                               instance:(NSInteger)instance
                 numberOfCorrectAnswers:(NSInteger)numberOfCorrectAnswers
               numberOfIncorrectAnswers:(NSInteger)numberOfIncorrectAnswers
               numberOfSkippedQuestions:(NSInteger)numberOfSkippedQuestions
           numberOfNotAnsweredQuestions:(NSInteger)numberOfNotAnsweredQuestions
                 numberOfTotalQuestions:(NSInteger)numberOfTotalQuestions
                                 status:(BOOL)status
                            isFirstTime:(BOOL)isFirstTime;

- (void)updatePageStatisticForLevel:(NSInteger)level
                            section:(NSInteger)section
                               unit:(NSInteger)unit
                           instance:(NSInteger)instance
                               page:(NSInteger)page
             numberOfCorrectAnswers:(NSInteger)numberOfCorrectAnswers
           numberOfIncorrectAnswers:(NSInteger)numberOfIncorrectAnswers
           numberOfSkippedQuestions:(NSInteger)numberOfSkippedQuestions
       numberOfNotAnsweredQuestions:(NSInteger)numberOfNotAnsweredQuestions
             numberOfTotalQuestions:(NSInteger)numberOfTotalQuestions
                         pageStatus:(BOOL)status
                         isExposure:(BOOL)isExposure;

- (void)unitInstancesDownloadCompleted:(NSInteger)levelOrder
                          sectionIndex:(NSInteger)sectionIndex
                             unitIndex:(NSInteger)unitIndex;

- (void)updateLevelScore:(NSInteger)level
                   score:(NSInteger)score;

- (void)resetInstanceValues:(NSInteger)level
                    section:(NSInteger)section
                       unit:(NSInteger)unit;

- (NSDictionary *)lessonSummary:(NSInteger)level
                        section:(NSInteger)section
                           unit:(NSInteger)unit;

- (BOOL)isLessonCompleted:(NSInteger)level
                  section:(NSInteger)section
                     unit:(NSInteger)unit;

- (BOOL)areAllInstancesVisited:(NSInteger)level
                     section:(NSInteger)section
                        unit:(NSInteger)unit;

- (BOOL)isLevelCompleted:(NSInteger)level;

- (BOOL)checkForModuleCertificate:(NSInteger)moduleNumber inLevel:(NSInteger)level;

- (NSInteger)lastVisitedInstanceInLevel:(NSInteger)level
                                section:(NSInteger)section
                                   unit:(NSInteger)unit;

- (BOOL)isInstanceVisited:(NSInteger)level
                  section:(NSInteger)section
                     unit:(NSInteger)unit
                 instance:(NSInteger)instanceIndex
                     page:(NSInteger)pageNumber;

- (BOOL)isInstanceFinishedWithCorrectAnswer:(NSInteger)level
                                    section:(NSInteger)section
                                       unit:(NSInteger)unit
                                   instance:(NSInteger)instanceIndex
                                 pageNumber:(NSInteger)pageIndex;

- (BOOL)isPageFinishedWithCorrectAnswer:(NSInteger)level
                                section:(NSInteger)section
                                   unit:(NSInteger)unit
                               instance:(NSInteger)instanceIndex
                             pageNumber:(NSInteger)pageIndex;

- (double)getLessonProgression:(NSInteger)level
                      section:(NSInteger)section
                         unit:(NSInteger)unit;

- (void)setLastVisitedInstance:(NSInteger)level
                       section:(NSInteger)section
                          unit:(NSInteger)unit
                 instanceIndex:(NSInteger)instanceIndex;

- (NSInteger)getLastVisitedInstance:(NSInteger)level
                            section:(NSInteger)section
                               unit:(NSInteger)unit;


- (BOOL)isLevelCompletedWithHonor:(NSInteger)level;

- (NSInteger)getNumberOfLessonsToCompleteLevel:(NSInteger)level;

- (NSInteger)getTheFirstSkippedOrWrongAnsweredInstance:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit;

- (NSArray *)statusLevelData;
- (NSInteger)scoreForCurrentLevel;
- (void)updateProgressModel;
- (BOOL)currentLevelCompleted;
- (NSInteger)alreadyCompletedLessonsInCurrentLevel;
- (BOOL)areAllLessonsAvaliableInLevel:(NSInteger)level;
- (NSInteger)lessonsForDiplomaInCurrentLevel;
- (NSInteger)lessonsInCurrentModule;
- (NSInteger)currentModule;
- (NSString*)takeCurentLevelName;
- (NSString *)takeCurentLevelName:(int)levelNum;
- (NSInteger)earnedStars;
- (BOOL)haveEnoughStarsInLevel:(NSInteger)level;
- (NSInteger)weekPracticeTime;
- (NSInteger)dailyAverageTime;
- (void)addPlayTime:(CFTimeInterval)elapsedTime;
- (NSInteger)numOfLessonToCompleteTheLevel:(NSInteger)levelID;
- (NSInteger)numOfStartLesson:(NSInteger)level;
- (BOOL)isLevelCompletedWithoutHonor:(NSInteger)level;
- (BOOL)haveEnoughPointsInLevel:(NSInteger)level;
- (BOOL)areAllInstancesVisited:(NSInteger)level;
- (NSInteger)numberOfVisitedLessonsInLevel:(NSInteger)level;
- (BOOL)areAllLessonsVisitedInLevel:(NSInteger)level;
- (void)setIsUnitVisited:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit;
- (BOOL)isUnitVisited:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit;
- (NSInteger)numberOfLessonsInLevel:(NSInteger)level;
- (NSDictionary *)maxUnitIndexForLevel:(NSInteger)level;
- (void)setMaxUnitForLevel:(NSInteger)level maxUnitId:(NSInteger)maxUnitId;
- (NSDictionary *)getModuleUnitIndex:(NSInteger)levelIndex maxUnitId:(NSInteger)maxUnitId;

- (BOOL)isLastLessonInLevel:(NSInteger)level
                    section:(NSInteger)section
                       unit:(NSInteger)unitIndex;

- (void)saveCurrentLevelIndex:(NSInteger)currentLevelIndex forUser:(NSString *)user;
- (void)saveCurrentModulIndex:(NSInteger)currentModulIndex forUser:(NSString *)user;
- (void)saveCurrentUnitIndex:(NSInteger)currentUnitIndex forUser:(NSString *)user;

- (NSInteger)getCurrentLevelIndexForUser:(NSString *)user;
- (NSInteger)getCurrentModulIndexForUser:(NSString *)user;
- (NSInteger)getCurrentUnitndexForUser:(NSString *)user;

- (void)setLastVisitedUnitIndexForLevel:(NSInteger)level index:(NSInteger)index;
- (void)setLastVisitedSectionIndexForLevel:(NSInteger)level index:(NSInteger)index;
- (NSInteger)getLastVisitedUnitIndexForLevel:(NSInteger)level;
- (NSInteger)getLastVisitedSectionIndexForLevel:(NSInteger)level;
- (NSInteger)getFirstLessonWithoutStar:(NSInteger)level;
- (NSInteger)getFirstSectionWithoutStar:(NSInteger)level;
- (void)setIsUnitOpen:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit;
- (NSDictionary *)nextMaxUnitIndexForLevel:(NSInteger)level;

- (NSInteger)getTheNumberOfTheLesson:(NSInteger)module unit:(NSInteger)unit;

@end
