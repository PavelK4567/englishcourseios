//
//  BaseStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/23/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModel.h"
@interface BaseStatistic : NSObject<NSCoding>

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;

+ (NSArray *)buldChildObjects:(NSArray *)childItems;

@end
