//
//  InstanceStatistic.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/23/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "InstanceStatistic.h"
#import "InstanceInfo.h"
#import "PageStatistic.h"
#import "ProgressFactory.h"

@implementation InstanceStatistic

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self                              = [self init];
    self.instanceID                   = [aDecoder decodeIntegerForKey:@"id"];
    self.order                        = [aDecoder decodeIntegerForKey:@"order"];
    self.pages                        = [aDecoder decodeIntegerForKey:@"pages"];
    self.type                         = [aDecoder decodeIntegerForKey:@"type"];
    self.numberOfCorrectAnswers       = [aDecoder decodeIntegerForKey:@"numberOfCorrectAnswers"];
    self.numberOfIncorrectAnswers     = [aDecoder decodeIntegerForKey:@"numberOfIncorrectAnswers"];
    self.numberOfNotAnsweredQuestions = [aDecoder decodeIntegerForKey:@"numberOfNotAnsweredQuestions"];
    self.numberOfSkippedQuestions     = [aDecoder decodeIntegerForKey:@"numberOfSkippedQuestions"];
    self.numberOfTotalQuestions       = [aDecoder decodeIntegerForKey:@"numberOfTotalQuestions"];
    self.status                       = [aDecoder decodeBoolForKey:@"status"];
    self.isFirstTime                  = [aDecoder decodeBoolForKey:@"isFirstTime"];
    self.isVisited                    = [aDecoder decodeBoolForKey:@"isVisited"];
    self.pagesArray                   = [aDecoder decodeObjectForKey:@"pagesArray"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeInteger:self.instanceID forKey:@"id"];
    [aCoder encodeInteger:self.order forKey:@"order"];
    [aCoder encodeInteger:self.pages forKey:@"pages"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    [aCoder encodeInteger:self.numberOfCorrectAnswers forKey:@"numberOfCorrectAnswers"];
    [aCoder encodeInteger:self.numberOfIncorrectAnswers forKey:@"numberOfIncorrectAnswers"];
    [aCoder encodeInteger:self.numberOfNotAnsweredQuestions forKey:@"numberOfNotAnsweredQuestions"];
    [aCoder encodeInteger:self.numberOfSkippedQuestions forKey:@"numberOfSkippedQuestions"];
    [aCoder encodeInteger:self.numberOfTotalQuestions forKey:@"numberOfTotalQuestions"];
    [aCoder encodeBool   :self.status forKey:@"status"];
    [aCoder encodeBool   :self.status forKey:@"isFirstTime"];
    [aCoder encodeBool   :self.isVisited forKey:@"isVisited"];
    [aCoder encodeObject :self.pagesArray forKey:@"pagesArray"];
 
}


+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{
    BaseInstance *instanceInfo = (BaseInstance *)dataObject;
    InstanceStatistic *statisticObject = [InstanceStatistic new];
    
    statisticObject.instanceID                   = instanceInfo.mID;
    statisticObject.order                        = instanceInfo.order;
    statisticObject.pages                        = instanceInfo.pages;
    statisticObject.type                         = instanceInfo.type;
    statisticObject.numberOfCorrectAnswers       = 0;
    statisticObject.numberOfIncorrectAnswers     = 0;
    statisticObject.numberOfNotAnsweredQuestions = 0;
    statisticObject.numberOfSkippedQuestions     = 0;
    statisticObject.numberOfTotalQuestions       = 0;
    statisticObject.status                       = NO;
    statisticObject.isFirstTime                  = YES;
    statisticObject.isVisited                    = NO;
    statisticObject.pagesArray = (instanceInfo.refObjectsArray) ? [[self buldChildObjects:instanceInfo.refObjectsArray] mutableCopy] : nil;

    return statisticObject;
}


+ (NSArray *)buldChildObjects:(NSArray *)childItems
{
    NSMutableArray *childObjectList = [[NSMutableArray alloc] initWithCapacity:[childItems count]];
    
    for(id item in childItems) {

        BaseStatistic *statistic = [ProgressFactory buildStatisticFromObject:nil childObject:nil statisticType:PageStatisticType];
        [childObjectList addObject:statistic];
    }
    
    return childObjectList;
}


- (float)getStatisticValueForInstance
{
    if(self.numberOfTotalQuestions > 0)
        return (self.numberOfCorrectAnswers / self.numberOfTotalQuestions) * 100;
    else return 0.0;
}

@end
