//
//  PageStatistic.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "PageStatistic.h"


@implementation PageStatistic

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self                              = [self init];
    self.pageOrderID                  = [aDecoder decodeIntegerForKey:@"id"];
    self.numberOfCorrectAnswers       = [aDecoder decodeIntegerForKey:@"numberOfCorrectAnswers"];
    self.numberOfIncorrectAnswers     = [aDecoder decodeIntegerForKey:@"numberOfIncorrectAnswers"];
    self.numberOfNotAnsweredQuestions = [aDecoder decodeIntegerForKey:@"numberOfNotAnsweredQuestions"];
    self.numberOfSkippedQuestions     = [aDecoder decodeIntegerForKey:@"numberOfSkippedQuestions"];
    self.numberOfTotalQuestions       = [aDecoder decodeIntegerForKey:@"numberOfTotalQuestions"];
    self.pageStatus                   = [aDecoder decodeBoolForKey:@"pageStatus"];
    self.isExposure                   = [aDecoder decodeBoolForKey:@"isExposure"];
    self.isVisited                    = [aDecoder decodeBoolForKey:@"isVisited"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.pageOrderID forKey:@"id"];
    [aCoder encodeInteger:self.numberOfCorrectAnswers forKey:@"numberOfCorrectAnswers"];
    [aCoder encodeInteger:self.numberOfIncorrectAnswers forKey:@"numberOfIncorrectAnswers"];
    [aCoder encodeInteger:self.numberOfNotAnsweredQuestions forKey:@"numberOfNotAnsweredQuestions"];
    [aCoder encodeInteger:self.numberOfSkippedQuestions forKey:@"numberOfSkippedQuestions"];
    [aCoder encodeInteger:self.numberOfTotalQuestions forKey:@"numberOfTotalQuestions"];
    [aCoder encodeBool:self.pageStatus forKey:@"pageStatus"];
    [aCoder encodeBool:self.isExposure forKey:@"isExposure"];
    [aCoder encodeBool:self.isVisited forKey:@"isVisited"];
}

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{

    PageStatistic *statisticObject               = [PageStatistic new];

    statisticObject.pageOrderID                  = 0;
    statisticObject.pageStatus                   = NO;
    statisticObject.numberOfCorrectAnswers       = 0;
    statisticObject.numberOfIncorrectAnswers     = 0;
    statisticObject.numberOfNotAnsweredQuestions = 0;
    statisticObject.numberOfSkippedQuestions     = 0;
    statisticObject.numberOfTotalQuestions       = 0;
    statisticObject.pageStatus                   = NO;
    statisticObject.isExposure                   = NO;
    statisticObject.isVisited                    = NO;
    
    return statisticObject;
}


@end
