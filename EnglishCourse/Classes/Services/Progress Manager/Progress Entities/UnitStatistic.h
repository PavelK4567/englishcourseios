//
//  UnitStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/25/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseStatistic.h"

@interface UnitStatistic : BaseStatistic

@property (nonatomic, assign) NSInteger      unitID;
@property (nonatomic, assign) NSInteger      sectionID;
@property (nonatomic, assign) NSInteger      order;
@property (nonatomic, assign) NSInteger      lessonProgress;
@property (nonatomic, assign) NSInteger      lastVisitedInstance;
@property (nonatomic, strong) NSMutableArray *instancesArray;
@property (nonatomic        ) BOOL           achievement;
@property (nonatomic        ) BOOL           isVisited;
@property (nonatomic        ) BOOL           isAvailable;
@property (nonatomic        ) BOOL           isOpen;

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;
@end
