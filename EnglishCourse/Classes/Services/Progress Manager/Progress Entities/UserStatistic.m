//
//  UserStatistic.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/8/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "UserStatistic.h"
#import "LevelStatistic.h"
#import "ProgressFactory.h"

@implementation UserStatistic


- (id)initWithCoder:(NSCoder *)aDecoder {

    self                     = [self init];
    self.userID              = [aDecoder decodeObjectForKey:@"id"];
    self.currentLevelId      = [aDecoder decodeIntegerForKey:@"currentLevelId"];
    self.lastVisitedLessonId = [aDecoder decodeIntegerForKey:@"lastVisitedLessonId"];
    self.weeklyUsageTime     = [aDecoder decodeIntegerForKey:@"weeklyUsageTime"];
    self.trackingWeek        = [aDecoder decodeIntegerForKey:@"trackingWeek"];
    self.levelsArray         = [aDecoder decodeObjectForKey: @"levelsArray"];
    self.isSync              = [aDecoder decodeBoolForKey: @"isSync"];
    self.userUsageTimeArray  = [aDecoder decodeObjectForKey: @"userUsageTimeArray"];
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.userID forKey:@"id"];
    [aCoder encodeInteger:self.currentLevelId forKey:@"sectionOrder"];
    [aCoder encodeInteger:self.lastVisitedLessonId forKey:@"lastVisitedLessonId"];
    [aCoder encodeInteger:self.weeklyUsageTime forKey:@"weeklyUsageTime"];
    [aCoder encodeInteger:self.trackingWeek forKey:@"trackingWeek"];
    [aCoder encodeObject:self.levelsArray forKey:@"levelsArray"];
    [aCoder encodeBool:self.isSync forKey:@"isSync"];
    [aCoder encodeObject:self.userUsageTimeArray forKey:@"userUsageTimeArray"];
}

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{

    UserStatistic *statistic      = [UserStatistic new];
    statistic.userID              = [USER_MANAGER userMsisdn];
    statistic.currentLevelId      = 0;
    statistic.lastVisitedLessonId = 0;
    statistic.weeklyUsageTime     = 0;
    statistic.trackingWeek        = 0;
    statistic.isSync              = NO;
    statistic.levelsArray         = [[self buldChildObjects:childObject] mutableCopy];
    statistic.userUsageTimeArray  = [[NSMutableArray alloc] initWithObjects:@0,@0,@0,@0,@0,@0,@0,@0,@0,@0, nil];
    return statistic;
}

+ (NSArray *)buldChildObjects:(NSArray *)childItems
{
    NSMutableArray *childObjectList = [[NSMutableArray alloc] initWithCapacity:[childItems count]];
    
    for(Unit *item in childItems) {
        
        BaseStatistic *statistic = [ProgressFactory buildStatisticFromObject:item childObject:nil statisticType:UserStatisticType];
        [childObjectList addObject:statistic];
    }
    
    return childObjectList;
}


@end
