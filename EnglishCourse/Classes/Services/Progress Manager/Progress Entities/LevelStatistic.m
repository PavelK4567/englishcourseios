//
//  LevelStatistic.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/25/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LevelStatistic.h"
#import "LevelData.h"
#import "ProgressFactory.h"

@implementation LevelStatistic

- (id)initWithCoder:(NSCoder *)aDecoder {

    self                         = [self init];
    self.levelID                 = [aDecoder decodeIntegerForKey:@"id"];
    self.score                   = [aDecoder decodeIntegerForKey:@"score"];
    self.lastVisitedUnitId       = [aDecoder decodeIntegerForKey:@"lastVisitedUnitId"];
    self.lastVisitedUnitIndex    = [aDecoder decodeIntegerForKey:@"lastVisitedUnitIndex"];
    self.lastVisitedSectionIndex = [aDecoder decodeIntegerForKey:@"lastVisitedSectionIndex"];
    self.lastVisitedInstanceId   = [aDecoder decodeIntegerForKey:@"lastVisitedInstanceId"];
    self.maxUnitId               = [aDecoder decodeIntegerForKey:@"maxUnitId"];
    self.sectionsArray           = [aDecoder decodeObjectForKey:@"sectionsArray"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeInteger:self.levelID forKey:@"id"];
    [aCoder encodeInteger:self.score forKey:@"score"];
    [aCoder encodeInteger:self.lastVisitedUnitId forKey:@"lastVisitedUnitId"];
    [aCoder encodeInteger:self.lastVisitedInstanceId forKey:@"lastVisitedInstanceId"];
    [aCoder encodeInteger:self.lastVisitedUnitIndex forKey:@"lastVisitedUnitIndex"];
    [aCoder encodeInteger:self.lastVisitedSectionIndex forKey:@"lastVisitedSectionIndex"];
    [aCoder encodeInteger:self.maxUnitId forKey:@"maxUnitId"];
    [aCoder encodeObject :self.sectionsArray forKey:@"sectionsArray"];

}

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type
{
    LevelData *levelInfo = (LevelData *)dataObject;
    LevelStatistic *levelStatistic = [LevelStatistic new];
    
    levelStatistic.levelID = levelInfo.levelID;
    levelStatistic.score = 0;
    levelStatistic.maxUnitId = 0;
    levelStatistic.sectionsArray = [[self buldChildObjects:levelInfo.sectionsArray] mutableCopy];
    
    return levelStatistic;
}

+ (NSArray *)buldChildObjects:(NSArray *)childItems
{
    NSMutableArray *childObjectList = [[NSMutableArray alloc] initWithCapacity:[childItems count]];
    
    for(Section *item in childItems) {
        
        BaseStatistic *statistic = [ProgressFactory buildStatisticFromObject:item childObject:nil statisticType:SectionStatisticType];
        [childObjectList addObject:statistic];
    }
    
    return childObjectList;
}

@end
