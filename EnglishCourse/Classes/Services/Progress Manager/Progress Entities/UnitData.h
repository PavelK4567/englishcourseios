//
//  UnitData.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/26/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnitData : NSObject

@property (nonatomic, assign) NSInteger unitID;
@property (nonatomic, assign) NSInteger progress;
@property (nonatomic)         BOOL      achieved;


#pragma mark - init

- (id)initWithUnitId:(NSInteger)unitId
            progress:(NSInteger)progress
             achived:(BOOL)achived;
@end
