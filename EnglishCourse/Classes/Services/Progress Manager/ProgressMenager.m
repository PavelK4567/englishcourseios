//
//  ProgressMenager.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/26/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "ProgressMenager.h"

@implementation ProgressMenager

#ifdef PROGRESS_SINGLETON
SINGLETON_GCD(ProgressMenager)
#endif


#pragma mark - initWithMSISDN

- (id)initWithMSISDN:(NSString *)userMSISDN
          userLevels:(NSArray *)levels
{
    self = [super init];
    if (self) {
        
        self.userStatistic        = [self buldUserStatistic:levels];
        self.userStatistic.userID = userMSISDN;
    }
    else {
        
        return nil;
    }
    return self;
}


#pragma mark - buldUserStatistic

- (UserStatistic *)buldUserStatistic:(NSArray *)levels
{
    NSMutableArray *levelStatistics = [[NSMutableArray alloc] initWithCapacity:levels.count];
    
    for(LevelData *level in levels) {

        LevelStatistic *levelStatistic = (LevelStatistic *)[ProgressFactory buildStatisticFromObject:level childObject:level.sectionsArray statisticType:LevelStatisticType];
        [levelStatistics addObject:levelStatistic];
    }
    
    UserStatistic *userStatistic = [UserStatistic new];
    userStatistic.levelsArray = levelStatistics;
    userStatistic.userUsageTimeArray  = [[NSMutableArray alloc] initWithObjects:@0,@0,@0,@0,@0,@0,@0,@0,@0,@0, nil];
    
    return userStatistic;
}


#pragma mark - saveUserStatistic

- (void)saveUserStatistic:(UserStatistic *)userStatistic
{
    
    NSData *progressData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"userprogress"];
    NSMutableArray *progressArray = [NSKeyedUnarchiver unarchiveObjectWithData:progressData];
    
    if(progressArray.count > 0) {
        
        int statisticIndex = -1;
        for (int i = 0; i < progressArray.count; i++) {
            UserStatistic *statistic = progressArray[i];
            if([statistic.userID isEqualToString:userStatistic.userID]) {
                statisticIndex = i;
                break;
            }
        }
        if(statisticIndex != -1)
            progressArray[statisticIndex] = userStatistic;
        else
            [progressArray addObject:userStatistic];
    }
    else
    {
        progressArray = [NSMutableArray new];
        [progressArray addObject:userStatistic];
    }
    
    self.userStatistic = userStatistic;
    
    NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:progressArray];
    [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"userprogress"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - deleteStatistic

- (void)deleteStatistic
{
    
    if ([USER_MANAGER isGuest])
    {
        self.guestUserStatistic = self.userStatistic;
        [self saveUserStatistic:self.userStatistic];
    }
    
    NSArray *levels = DATA_SERVICE.dataModel.courseData.levelsArray;
    UserStatistic *userStatistic = [self buldUserStatistic:levels];
    
    [self saveUserStatistic:userStatistic];
}


#pragma mark - saveGestUserStatistic

- (void)saveGestUserStatistic:(UserStatistic *)statistic
{
    
    NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:statistic];
    [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"gestprogress"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - getGuestUserStatistic

- (UserStatistic *)getGuestUserStatistic
{
    NSData *guestData             = [[NSUserDefaults standardUserDefaults] objectForKey:@"gestprogress"];
    UserStatistic *guestStatistic = [NSKeyedUnarchiver unarchiveObjectWithData:guestData];
    
    return guestStatistic;
}


#pragma mark - getGuestUserStatistic

- (void)transferPointsFromGuestToINAPUser
{
    UserStatistic *guestStatistic = [self getGuestUserStatistic];
    self.userStatistic            = guestStatistic;
    self.userStatistic.userID     = [USER_MANAGER userMsisdn];
    
    [self saveUserStatistic:self.userStatistic];
    
}


#pragma mark - getProgressForUser

- (UserStatistic *)getProgressForUser:(NSString *)msisdn
{
    NSData *progressData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"userprogress"];
    NSMutableArray *progressArray = [NSKeyedUnarchiver unarchiveObjectWithData:progressData];
    NSMutableArray *resultArray   = [NSMutableArray new];
    NSPredicate *predicate        = [NSPredicate predicateWithFormat:@"userID == %@",msisdn ? msisdn : @"55000000000"];
    resultArray                   = [[progressArray filteredArrayUsingPredicate:predicate] mutableCopy];
    self.userStatistic            = [resultArray lastObject];
    return self.userStatistic;
}


#pragma mark - getInstanceStatisticForLevel

- (InstanceStatistic *)getInstanceStatisticForLevel:(NSInteger)level
                                            section:(NSInteger)section
                                               unit:(NSInteger)unit
                                           instance:(NSInteger)instance
                                      userStatistic:(UserStatistic *)userStatistic
{
    LevelStatistic *levelStatistic     = self.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    
    if(unitStatistic.instancesArray.count == 0)
        return nil;
    else {
        
        if (instance >= [unitStatistic.instancesArray count]) {
            return nil;
        }
        InstanceStatistic *instanceStatistic = unitStatistic.instancesArray[instance];
        return instanceStatistic;
    }
}


#pragma mark - getPageStatisticForLevel

- (PageStatistic *)getPageStatisticForLevel:(NSInteger)level
                                    section:(NSInteger)section
                                       unit:(NSInteger)unit
                                   instance:(NSInteger)instance
                                       page:(NSInteger)page
                              userStatistic:(UserStatistic *)userStatistic {
    
    InstanceStatistic *statistic = [self getInstanceStatisticForLevel:level
                                                              section:section
                                                                 unit:unit
                                                             instance:instance
                                                        userStatistic:self.userStatistic];
    
    return statistic.pagesArray[page];
    
}


#pragma mark - Array instances in lesson statistics

- (NSArray *)getLessonInstanceStatisticsForLevel:(NSInteger)level
                                         section:(NSInteger)section
                                            unit:(NSInteger)unit
                                   userStatistic:(UserStatistic *)userStatistic
{
    
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];

    NSMutableArray *tempArray = [NSMutableArray new];
    
    NSArray *arrayInstancesStats = unitStatistic.instancesArray;
    
    for (InstanceStatistic *instanceStat in arrayInstancesStats) {
        
        if (instanceStat.type == kInstanceMultipleChoiceImage) {
            
            [tempArray addObject:instanceStat];
            
            for (PageStatistic *pageStatistic in instanceStat.pagesArray) {
                
                [tempArray addObject:pageStatistic];
            }
        }
        else if (instanceStat.type == kInstancePractice) {
            
            for (PageStatistic *pageStatistic in instanceStat.pagesArray) {
                
                [tempArray addObject:pageStatistic];
            }
        }
        else {
            
            [tempArray addObject:instanceStat];
            
        }
        
    }
    
    return tempArray;
}



#pragma mark - updateProgressForUser

- (void)updateProgressForUserLevel:(NSInteger)level
                           section:(NSInteger)section
                              unit:(NSInteger)unit
                       unitAchived:(NSInteger)unitAchived
{

    UserStatistic *userStatistic       = [self getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    
    unitStatistic.achievement = unitAchived;
    
    [self saveUserStatistic:userStatistic];
}


#pragma mark - update instance statistic

- (void)updateInstanceStatisticForLevel:(NSInteger)level
                                section:(NSInteger)section
                                   unit:(NSInteger)unit
                               instance:(NSInteger)instance
                 numberOfCorrectAnswers:(NSInteger)numberOfCorrectAnswers
               numberOfIncorrectAnswers:(NSInteger)numberOfIncorrectAnswers
               numberOfSkippedQuestions:(NSInteger)numberOfSkippedQuestions
           numberOfNotAnsweredQuestions:(NSInteger)numberOfNotAnsweredQuestions
                 numberOfTotalQuestions:(NSInteger)numberOfTotalQuestions
                                 status:(BOOL)status
                            isFirstTime:(BOOL)isFirstTime

{

    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    
    InstanceStatistic *statistic = [self getInstanceStatisticForLevel:level
                                                              section:section
                                                                 unit:unit
                                                             instance:instance
                                                        userStatistic:userStatistic];
        
    statistic.numberOfCorrectAnswers       = numberOfCorrectAnswers;
    statistic.numberOfIncorrectAnswers     = numberOfIncorrectAnswers;
    statistic.numberOfNotAnsweredQuestions = numberOfNotAnsweredQuestions;
    statistic.numberOfSkippedQuestions     = numberOfSkippedQuestions;
    statistic.numberOfTotalQuestions       = numberOfTotalQuestions;
    statistic.status                       = status;
    statistic.isFirstTime                  = isFirstTime;
    statistic.isVisited                    = YES;
    
    if(statistic && userStatistic) {
        if(instance <= [(((UnitStatistic *)((SectionStatistic *)((LevelStatistic *)userStatistic.levelsArray[level]).sectionsArray[section]).unitsArray[unit]).instancesArray) count] - 1) {
            [(((UnitStatistic *)((SectionStatistic *)((LevelStatistic *)userStatistic.levelsArray[level]).sectionsArray[section]).unitsArray[unit]).instancesArray) replaceObjectAtIndex:instance withObject:statistic];
        }

        [self saveUserStatistic:userStatistic];
    }
}


#pragma mark - update instance statistic

- (void)updatePageStatisticForLevel:(NSInteger)level
                            section:(NSInteger)section
                               unit:(NSInteger)unit
                           instance:(NSInteger)instance
                               page:(NSInteger)page
             numberOfCorrectAnswers:(NSInteger)numberOfCorrectAnswers
           numberOfIncorrectAnswers:(NSInteger)numberOfIncorrectAnswers
           numberOfSkippedQuestions:(NSInteger)numberOfSkippedQuestions
       numberOfNotAnsweredQuestions:(NSInteger)numberOfNotAnsweredQuestions
             numberOfTotalQuestions:(NSInteger)numberOfTotalQuestions
                         pageStatus:(BOOL)status
                         isExposure:(BOOL)isExposure

{

    InstanceStatistic *statistic = [self getInstanceStatisticForLevel:level
                                                              section:section
                                                                 unit:unit
                                                             instance:instance
                                                        userStatistic:self.userStatistic];
        
    PageStatistic *pageStatistic = statistic.pagesArray[page];
        
    pageStatistic.numberOfCorrectAnswers       = numberOfCorrectAnswers;
    pageStatistic.numberOfIncorrectAnswers     = numberOfIncorrectAnswers;
    pageStatistic.numberOfNotAnsweredQuestions = numberOfNotAnsweredQuestions;
    pageStatistic.numberOfSkippedQuestions     = numberOfSkippedQuestions;
    pageStatistic.numberOfTotalQuestions       = numberOfTotalQuestions;
    pageStatistic.pageStatus                   = status;
    pageStatistic.isExposure                   = isExposure;
    pageStatistic.isVisited                    = YES;
        
  @try {
    [((InstanceStatistic *)((UnitStatistic *)((SectionStatistic *)((LevelStatistic *)self.userStatistic.levelsArray[level]).sectionsArray[section]).unitsArray[unit]).instancesArray[instance]).pagesArray replaceObjectAtIndex:page withObject:pageStatistic];
    
    [self saveUserStatistic:self.userStatistic];
  }
  @catch (NSException *exception) {
    
  }
  

}


#pragma mark - last visited index

- (NSInteger)lastVisitedInstanceInLevel:(NSInteger)level
                                section:(NSInteger)section
                                   unit:(NSInteger)unit
{

    NSInteger lastVisitedIndex = 0;
    NSArray *instances = [self getLessonInstanceStatisticsForLevel:level section:section unit:unit userStatistic:self.userStatistic];
    for (NSInteger i = 0; i < instances.count; i++) {
        
        id statistc = instances[i];
        if([statistc isKindOfClass:[InstanceStatistic class]]) {
            lastVisitedIndex = ((InstanceStatistic *)statistc).isVisited ? i : lastVisitedIndex;
        }
        else {
            lastVisitedIndex = ((PageStatistic *)statistc).isVisited ? i : lastVisitedIndex;
        }
    }

    return lastVisitedIndex;
}


#pragma mark - update level score

- (void)updateLevelScore:(NSInteger)level
                   score:(NSInteger)score
{

    UserStatistic *userStatistic = [self getProgressForUser:[USER_MANAGER userMsisdn]];
    ((LevelStatistic *)userStatistic.levelsArray[level]).score += score;
    [self saveUserStatistic:userStatistic];
}


#pragma mark - scoreForCurrentLevel

- (NSInteger)scoreForCurrentLevel
{
    NSInteger currentLevelIdFromUser = [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]];

    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = (LevelStatistic *)userStatistic.levelsArray[currentLevelIdFromUser];
    return levelStatistic.score;
}


#pragma mark - currentLevelCompleted

- (BOOL)currentLevelCompleted
{
    
    LevelStatistic *levelStatistic = [LevelStatistic new];
    
    NSInteger currentLevelIndex = [PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]];
    levelStatistic = self.userStatistic.levelsArray[currentLevelIndex];
    
    return levelStatistic.levelCompleted;
}


#pragma mark - statusLevelData

- (NSArray *)statusLevelData
{
    NSMutableArray *levelDataArray = [[NSMutableArray alloc] init];
    NSArray *levelDataItems = [USER_MANAGER takeLevelData];
    for(NSDictionary *level in levelDataItems) {
    
        if(level) {
            LevelStatusData *levelDataItem = [[LevelStatusData alloc] initWithDict:level];
            [levelDataArray addObject:levelDataItem];
        }
    }
    
    return [levelDataArray mutableCopy];
}


#pragma mark - statusUnitData

- (NSArray *)statusUnitData
{
    NSMutableArray *unitsDataArray = [NSMutableArray new];
    NSArray *data = [self statusLevelData];
    for (LevelStatusData *ld in data) {
        
        if(ld)
            [unitsDataArray addObjectsFromArray:ld.unitDataArray];
    }
    
    return [unitsDataArray mutableCopy];
}


#pragma mark - findLevelDataById

- (LevelStatusData *)findLevelDataById:(NSInteger)levelId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"levelId == %d",levelId];
    return  [[[self statusLevelData] filteredArrayUsingPredicate:predicate] lastObject];
}


#pragma mark - findUnitDataById

- (UnitData *)findUnitDataById:(NSInteger)unitId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unitID == %d",unitId];
    return [[[self statusUnitData] filteredArrayUsingPredicate:predicate] lastObject];
}


#pragma mark - updateProgressModel

- (void)updateProgressModel
{
    UserStatistic *statistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    if(statistic) {
    if(![USER_MANAGER isUserRegister] || ![USER_MANAGER isNative] || ([USER_MANAGER isNative] && statistic.isSync) || ([USER_MANAGER isNative] && ![self getGuestUserStatistic])) {

    
        for(LevelStatistic *ls in statistic.levelsArray) {
        
            LevelStatusData *levelData = [self findLevelDataById:ls.levelID];

            ls.diplomaAchieved = levelData.diploma;
            ls.score           = levelData.score;
            ls.maxUnitId       = levelData.maxUnitId;

            NSArray *visitedLevels = levelData.unitDataArray;
            for (SectionStatistic *ss in ls.sectionsArray) {
            
                for(UnitStatistic *us in ss.unitsArray) {
                
                    UnitData *unitData = [self findUnitDataById:us.unitID];
                    us.lessonProgress  = unitData.progress;
                    us.achievement     = unitData.achieved;
                    for (UnitData *ud in visitedLevels) {
                        if(us.unitID == ud.unitID)
                            us.isVisited = YES;
                    }
                }
            }
        }
    
        [statistic setIsSync:YES];
        [self saveUserStatistic:statistic];
    }
    }
    else if ([USER_MANAGER isNative] && !statistic.isSync){
    
        statistic = [self getGuestUserStatistic];
        statistic.isSync = YES;
        statistic.userID = [USER_MANAGER userMsisdn];
        
        if(statistic)
            [self saveUserStatistic:statistic];
    }
    
}


#pragma mark - setIsUnitOpen

- (void)setIsUnitOpen:(NSInteger)level
              section:(NSInteger)section
                 unit:(NSInteger)unit
{
    UserStatistic *userStatistic       = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];

    [unitStatistic setIsOpen:YES];
    [self saveUserStatistic:userStatistic];
}


#pragma mark - unitInstancesDownloadCompleted download manager delegate

- (void)unitInstancesDownloadCompleted:(NSInteger)levelOrder
                          sectionIndex:(NSInteger)sectionIndex
                             unitIndex:(NSInteger)unitIndex
{

    NSArray *instances = ((Unit *)((Section *)((LevelData *)DATA_SERVICE.dataModel.courseData.levelsArray[levelOrder]).sectionsArray[sectionIndex]).unitsArray[unitIndex]).instancesArray;
   
    UserStatistic *userStatistic = [self getProgressForUser:[USER_MANAGER userMsisdn]];
    for (NSInteger i = 0; i < instances.count; i++) {
        
        BaseInstance *instance = instances[i];
        BaseStatistic *statistic = [ProgressFactory buildStatisticFromObject:instance childObject:instance.refObjectsArray statisticType:InstanceStatisticType];
        

        if(![self hasIstanceStatistic:(InstanceStatistic *)statistic inLevel:levelOrder sectionIndex:sectionIndex unit:unitIndex]) {
            [((UnitStatistic *)((SectionStatistic *)((LevelStatistic *)userStatistic.levelsArray[levelOrder]).sectionsArray[sectionIndex]).unitsArray[unitIndex]).instancesArray addObject:(InstanceStatistic *)statistic];
        }
        
        [self saveUserStatistic:userStatistic];
    }
    
    
    LevelData *level       = DATA_SERVICE.dataModel.courseData.levelsArray[levelOrder];
    Section *section       = level.sectionsArray[sectionIndex];
    Unit *unit             = section.unitsArray[unitIndex];

    if(unit.numberOfInstances == unit.instancesArray.count) {
    
        [self updateUnitAvailability:YES level:levelOrder section:sectionIndex unit:unitIndex];
    }

}


#pragma mark - hasIstanceStatistic

- (BOOL)hasIstanceStatistic:(InstanceStatistic *)statistic
                    inLevel:(NSInteger)level
               sectionIndex:(NSInteger)section
                       unit:(NSInteger)unit
{

    LevelStatistic *levelStatistic     = PROGRESS_MANAGER.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    NSArray *instanceStatistic         = unitStatistic.instancesArray;
    
    if(instanceStatistic.count > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"instanceID == %d",statistic.instanceID];
        NSArray *resultList = [instanceStatistic filteredArrayUsingPredicate:predicate];
        BOOL status = resultList.count == 0 ? NO : YES;
        return status;
    }
    
    else
        return NO;
}

#pragma mark - updateUnitAvailability

- (void)updateUnitAvailability:(BOOL)status
                         level:(NSInteger)level
                       section:(NSInteger)section
                          unit:(NSInteger)unit
{
 
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    
    ((UnitStatistic *)((SectionStatistic *)((LevelStatistic *)userStatistic.levelsArray[level]).sectionsArray[section]).unitsArray[unit]).isAvailable = status;
    
    [self saveUserStatistic:userStatistic];
}


#pragma mark - isUnitAvaliable

- (BOOL)isUnitAvailable:(NSInteger)level
                section:(NSInteger)section
                   unit:(NSInteger)unit
{

    UserStatistic *userStatistic       = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    
    return (unitStatistic.isAvailable);
}


#pragma mark - resetInstanceValues

- (void)resetInstanceValues:(NSInteger)level
                    section:(NSInteger)section
                       unit:(NSInteger)unit
{

    UserStatistic *userStatistic       = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    NSMutableArray *instanceStatistic  = unitStatistic.instancesArray;
    
    for (InstanceStatistic *statistic in instanceStatistic) {
        statistic.numberOfCorrectAnswers       = 0;
        statistic.numberOfCorrectAnswers       = 0;
        statistic.numberOfIncorrectAnswers     = 0;
        statistic.numberOfSkippedQuestions     = 0;
        statistic.numberOfNotAnsweredQuestions = 0;
        statistic.numberOfTotalQuestions       = 0;
        statistic.status                       = NO;
        
        for (PageStatistic *pageStatistic in statistic.pagesArray) {
            pageStatistic.numberOfCorrectAnswers       = 0;
            pageStatistic.numberOfCorrectAnswers       = 0;
            pageStatistic.numberOfIncorrectAnswers     = 0;
            pageStatistic.numberOfSkippedQuestions     = 0;
            pageStatistic.numberOfNotAnsweredQuestions = 0;
            pageStatistic.numberOfTotalQuestions       = 0;
            pageStatistic.pageStatus                   = NO;

        }
    }
    
    ((UnitStatistic *)((SectionStatistic *)((LevelStatistic *)userStatistic.levelsArray[level]).sectionsArray[section]).unitsArray[unit]).instancesArray = instanceStatistic;
    
    [self saveUserStatistic:userStatistic];
}


#pragma mark - lessonSummary

- (NSDictionary *)lessonSummary:(NSInteger)level
                        section:(NSInteger)section
                           unit:(NSInteger)unit
{
    
    NSDictionary *resultDictionary = [NSDictionary new];
    
    LevelStatistic *levelStatistic     = self.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    NSMutableArray *instanceStatistics = unitStatistic.instancesArray;
    
    NSInteger      numberOfCorrectAnswers       = 0;
    NSInteger      numberOfIncorrectAnswers     = 0;
    NSInteger      numberOfSkippedQuestions     = 0;
    NSInteger      numberOfNotAnsweredQuestions = 0;
    NSInteger      numberOfTotalQuestions       = 0;
    
    for (InstanceStatistic *instance in instanceStatistics) {
        
        if (instance.type == kInstanceSimplePageProfile || instance.type == kInstanceSimplePageTextAndVideo || instance.type == kInstanceSimplePageBubbleDialog) {
            continue;
        }

        if(!instance.pagesArray) {
            
            if (instance.type == kInstanceMultipleChoiceImage) {
                continue;
            }
            numberOfCorrectAnswers       += instance.numberOfCorrectAnswers;
            numberOfIncorrectAnswers     += instance.numberOfIncorrectAnswers;
            numberOfSkippedQuestions     += instance.numberOfSkippedQuestions;
            numberOfNotAnsweredQuestions += instance.numberOfNotAnsweredQuestions;
            numberOfTotalQuestions       += 1;
        }
        else {
            
            for (PageStatistic *page in instance.pagesArray) {
                
                numberOfCorrectAnswers       += page.numberOfCorrectAnswers;
                numberOfIncorrectAnswers     += page.numberOfIncorrectAnswers;
                numberOfSkippedQuestions     += page.numberOfSkippedQuestions;
                numberOfNotAnsweredQuestions += page.numberOfNotAnsweredQuestions;
                numberOfTotalQuestions       += 1;
            }
            
        }
        
    }
    
    NSArray *keys    = [NSArray arrayWithObjects:@"numberOfCorrectAnswers",@"numberOfIncorrectAnswers",@"numberOfSkippedQuestions",@"numberOfNotAnsweredQuestions",@"numberOfTotalQuestions", nil];
    NSArray *values  = [NSArray arrayWithObjects:[NSNumber numberWithInteger:numberOfCorrectAnswers],[NSNumber numberWithInteger:numberOfIncorrectAnswers],[NSNumber numberWithInteger:numberOfSkippedQuestions],[NSNumber numberWithInteger:numberOfNotAnsweredQuestions],[NSNumber numberWithInteger:numberOfTotalQuestions], nil];
    
    resultDictionary = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
    return resultDictionary;
}


#pragma mark - is lesson completed succesffull > 80 percent

- (BOOL)isLessonCompleted:(NSInteger)level
                  section:(NSInteger)section
                     unit:(NSInteger)unit
{
    
    
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[level];
    
    if (levelStatistic == nil) {
        return NO;
    }
    
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    
    if(unitStatistic.achievement == YES)
        return YES;
    
    NSMutableArray *instanceStatistics = unitStatistic.instancesArray;
    
    NSInteger      numberOfCorrectAnswers       = 0;
    NSInteger      numberOfIncorrectAnswers     = 0;
    NSInteger      numberOfSkippedQuestions     = 0;
    NSInteger      numberOfTotalQuestions       = 0;
    
    for (InstanceStatistic *instance in instanceStatistics) {
                
        if (instance.type == kInstancePresentationDialog || instance.type == kInstanceSimplePageProfile || instance.type == kInstanceSimplePageTextAndVideo || instance.type == kInstanceSimplePageBubbleDialog) {
            continue;
        }
        
        if(!instance.pagesArray) {
            
            if (instance.type == kInstanceMultipleChoiceImage) {
                continue;
            }
            numberOfCorrectAnswers       += instance.numberOfCorrectAnswers;
            numberOfIncorrectAnswers     += instance.numberOfIncorrectAnswers;
            numberOfSkippedQuestions     += instance.numberOfSkippedQuestions;
            numberOfTotalQuestions       += 1;
        }
        else {
            
            for (PageStatistic *page in instance.pagesArray) {
                
                numberOfCorrectAnswers       += page.numberOfCorrectAnswers;
                numberOfIncorrectAnswers     += page.numberOfIncorrectAnswers;
                numberOfSkippedQuestions     += page.numberOfSkippedQuestions;
                numberOfTotalQuestions       += 1;
            }
            
        }
        
    }
    
    float percentAnsweredQuestion = (float)numberOfCorrectAnswers * 100.0 / (float)numberOfTotalQuestions;

    if (percentAnsweredQuestion >= kPercentSuccess) {

        return YES;
    }
    else {
        return NO;
    }
}


#pragma mark - check for module certificate

- (BOOL)checkForModuleCertificate:(NSInteger)moduleNumber inLevel:(NSInteger)level
{
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    
    if(level >= [userStatistic.levelsArray count]){
        return NO;
    }
    
    LevelStatistic *levelStatistic  = userStatistic.levelsArray[level];
    
    if (levelStatistic == nil) {
        return NO;
    }
    
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[moduleNumber];
    
    for (int i = 0; i < sectionStatistic.unitsArray.count; i++) {
        
        if (![self isLessonCompleted:level section:moduleNumber unit:i]) {
            
            return NO;
        }
    }
    
    return YES;
}


#pragma mark - setLastVisitedInstance

- (void)setLastVisitedInstance:(NSInteger)level
                       section:(NSInteger)section
                          unit:(NSInteger)unit
                 instanceIndex:(NSInteger)instanceIndex
{

    UserStatistic *userStatistic       = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    unitStatistic.lastVisitedInstance  = instanceIndex;
    
    if(instanceIndex != -1 && unitStatistic) {
        [(((SectionStatistic *)((LevelStatistic *)userStatistic.levelsArray[level]).sectionsArray[section]).unitsArray) replaceObjectAtIndex:unit withObject:unitStatistic];
        [self saveUserStatistic:userStatistic];
    }
}


#pragma mark - getLastVisitedInstance

- (NSInteger)getLastVisitedInstance:(NSInteger)level
                            section:(NSInteger)section
                               unit:(NSInteger)unit
{
    
    LevelStatistic *levelStatistic     = self.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    
    return unitStatistic.lastVisitedInstance;
    
}


#pragma mark - Are the instances shown 100%

- (BOOL)areAllInstancesVisited:(NSInteger)level
                       section:(NSInteger)section
                          unit:(NSInteger)unit
{
    
    
    LevelStatistic *levelStatistic     = self.userStatistic.levelsArray[level];    
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    NSMutableArray *instanceStatistics = unitStatistic.instancesArray;
  
    if([instanceStatistics count] <= 0)
      return NO;
  
    InstanceStatistic *instance = [instanceStatistics lastObject];
    
    if(!instance.pagesArray || instance.pagesArray.count == 0) {
        
        if (instance.isVisited) {
            
            return YES;
        }
    
    }
    else {
        
        PageStatistic *page = [instance.pagesArray lastObject];
        
        if (page.isVisited) {
            
            return YES;
        }
            
    }
    
    return NO;
}


#pragma mark - Check if instance is visited

- (BOOL)isInstanceVisited:(NSInteger)level
                  section:(NSInteger)section
                     unit:(NSInteger)unit
                 instance:(NSInteger)instanceIndex
                     page:(NSInteger)pageNumber {
    
    
    UserStatistic *userStatistic       = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic     = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
  
    NSMutableArray *instanceStatistics = unitStatistic.instancesArray;
    
    if([instanceStatistics count] <= 0)
        return NO;
    
    if(instanceIndex > instanceStatistics.count - 1) {
        return NO;
    }
    else {
        InstanceStatistic *instance = [instanceStatistics objectAtIndex:instanceIndex];
    
        if(instance.pagesArray && instance.pagesArray.count > 0) {
            
            if(pageNumber <= instance.pagesArray.count -1) {
                PageStatistic *pageStats = [instance.pagesArray objectAtIndex:pageNumber];
                return pageStats.pageStatus;
            }
            else return NO;

        }
        else {
            return instance.status;
        }
    }
}


#pragma mark - Is correct answer

- (BOOL)isInstanceFinishedWithCorrectAnswer:(NSInteger)level
                                    section:(NSInteger)section
                                       unit:(NSInteger)unit
                                   instance:(NSInteger)instanceIndex
                                 pageNumber:(NSInteger)pageIndex {
    
    
    @try {
        
        LevelStatistic *levelStatistic     = self.userStatistic.levelsArray[level];
        SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
        UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
        NSMutableArray *instanceStatistics = unitStatistic.instancesArray;
        
        if([instanceStatistics count] <= 0)
            return NO;
        
        InstanceStatistic *instance = [instanceStatistics objectAtIndex:instanceIndex];
        if ([instance.pagesArray count] > 0 && instance.pagesArray !=nil) {
            
            int numOfCorrectAnswers = 0;
            for (PageStatistic *pageStatistics in instance.pagesArray) {
                
                if (pageStatistics.numberOfCorrectAnswers > 0) {
                    
                    numOfCorrectAnswers++;
                }
            }
            
            
            if (numOfCorrectAnswers == instance.pagesArray.count)
                return YES;
            else
                return NO;
        }
        
        return instance.numberOfCorrectAnswers > 0 ? YES : NO;
    }
    @catch (NSException *exception) {

        return NO;
    }
    @finally {
        
    }
   
}

- (BOOL)isPageFinishedWithCorrectAnswer:(NSInteger)level
                                    section:(NSInteger)section
                                       unit:(NSInteger)unit
                                   instance:(NSInteger)instanceIndex
                                 pageNumber:(NSInteger)pageIndex {
    
    
    @try {
        
        LevelStatistic *levelStatistic     = self.userStatistic.levelsArray[level];
        SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
        UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
        NSMutableArray *instanceStatistics = unitStatistic.instancesArray;
        
        if([instanceStatistics count] <= 0)
            return NO;
        
        InstanceStatistic *instance = [instanceStatistics objectAtIndex:instanceIndex];
        if ([instance.pagesArray count] > 0 && instance.pagesArray !=nil) {
            
            PageStatistic *pageStatistics = instance.pagesArray[pageIndex];
            
            if (pageStatistics.numberOfCorrectAnswers>0)
                return YES;
            else
                return NO;
        }
        
    }
    @catch (NSException *exception) {
        
        return NO;
    }
    @finally {
        
    }
    
}



#pragma mark - is the level completed

- (BOOL)isLevelCompleted:(NSInteger)level
{
    
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[level];
    
    if (levelStatistic == nil) {
        return NO;
    }
    
    SectionStatistic *sectionStatistic;

    for (int i=0; i<[levelStatistic.sectionsArray count]; i++) {
        
        sectionStatistic = levelStatistic.sectionsArray[i];
        
        for (int j=0; j<[sectionStatistic.unitsArray count]; j++) {
            
            if (![self isLessonCompleted:level section:i unit:j]) {
                
                return NO;
            }
            
        }
        
    }
    
    return YES;
}


#pragma mark - number of lessons to complete the level

- (NSInteger)getNumberOfLessonsToCompleteLevel:(NSInteger)level
{
    
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[level];
    
    if (levelStatistic == nil) {
        return -1;
    }
    
    SectionStatistic *sectionStatistic;
    NSInteger numberOfLesson = 0;
    
    for (int i=0; i<[levelStatistic.sectionsArray count]; i++) {
        
        sectionStatistic = levelStatistic.sectionsArray[i];
        
        for (int j=0; j<[sectionStatistic.unitsArray count]; j++) {
            
            if (![self isLessonCompleted:level section:i unit:j]) {
                
                numberOfLesson++;
            }
            
        }
        
    }
    
    return numberOfLesson;
    
}


#pragma mark - Get progres in lesson

- (double)getLessonProgression:(NSInteger)level
                      section:(NSInteger)section
                         unit:(NSInteger)unit
{
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];

    LevelStatistic *levelStatistic     = PROGRESS_MANAGER.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic       = sectionStatistic.unitsArray[unit];
    
    if (userStatistic == nil) {
        
        userStatistic = [self getProgressForUser:[USER_MANAGER userMsisdn]];

    }
    
    NSArray *tempArray = [self getLessonInstanceStatisticsForLevel:level section:section unit:unit userStatistic:userStatistic];
    
    NSInteger indexOfVisitedInstances = 0;
    int counter = 0;
    for (InstanceStatistic *instance in tempArray) {
        
        
        if (![instance isKindOfClass:[PageStatistic class]]) {
            
            if (instance.type == kInstanceSimplePageProfile || instance.type == kInstanceSimplePageTextAndVideo || instance.type == kInstanceSimplePageBubbleDialog || instance.type == kInstanceMultipleChoiceImage) {
                
                if (instance.isVisited) {
                    indexOfVisitedInstances++;
                }
            }
            else {
                
                if ((instance.numberOfIncorrectAnswers > 0 || instance.numberOfCorrectAnswers > 0) && instance.isVisited) {
                    indexOfVisitedInstances++;
                    
                }
            }
            
        }
        else {
            
            
            if ((instance.numberOfIncorrectAnswers > 0 || instance.numberOfCorrectAnswers > 0) && instance.isVisited) {
                indexOfVisitedInstances++;
            }
        }
        
        counter++;
    }
    
    double progress = (double)(indexOfVisitedInstances) * 100.0 / (double)[tempArray count];
    
    if(isnan(progress)) progress = 0;
    
    double progressFromServer = unitStatistic.lessonProgress;
    
    if(progressFromServer > progress) {
        return  progressFromServer;
    }
    else {
        if (indexOfVisitedInstances == 0)
            return 0.0;
        else
            return progress;
    }

}


#pragma mark - getTheFirstSkippedOrWrongAnsweredInstance

- (NSInteger)getTheFirstSkippedOrWrongAnsweredInstance:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit
{
    
    UserStatistic *userStatistic = self.userStatistic;
    
    if (userStatistic == nil) {
        
        userStatistic = [self getProgressForUser:[USER_MANAGER userMsisdn]];
        
    }
    
    NSArray *arrayInstances = [self getLessonInstanceStatisticsForLevel:level section:section unit:unit userStatistic:userStatistic];
    
    
    for (int i=0; i<arrayInstances.count; i++) {
     
        InstanceStatistic *instance = [arrayInstances objectAtIndex:i];
        
        if (instance.numberOfIncorrectAnswers > 0 || instance.numberOfSkippedQuestions > 0) {
            
            return i;
        }
        
    }
    
    return -1;
}


#pragma mark - alreadyCompletedLessonsInCurrentLevel

- (NSInteger)alreadyCompletedLessonsInCurrentLevel
{
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger numberOfCompletedLessons = 0;
    
    for (int section = 0; section < levelStatistic.sectionsArray.count; section ++) {
        
        for (int unit = 0; unit < [((SectionStatistic *)levelStatistic.sectionsArray[section]).unitsArray count] ; unit ++) {
            
            if([self isLessonCompleted:[PROGRESS_MANAGER currentLevelIndex] section:section unit:unit]) numberOfCompletedLessons ++;
        }
        
    }
    
    return numberOfCompletedLessons;
}


#pragma mark - lessonsForDiplomaInCurrentLevel

- (NSInteger)lessonsForDiplomaInCurrentLevel
{
    
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
    
    NSInteger lessonsForDiploma = 0;
    
    for (int i = 0; i < levelStatistic.sectionsArray.count; i++) {
        
        SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[i];
        
        lessonsForDiploma+=sectionStatistic.unitsArray.count;
        
    }
    
    return lessonsForDiploma;
}


#pragma mark - lessonsInCurrentModule

- (NSInteger)lessonsInCurrentModule
{
    if (self.userStatistic.levelsArray == nil || self.userStatistic.levelsArray.count == 0) {
        return 0;
    }
    
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[[PROGRESS_MANAGER currentLevelIndex]];
    
    NSInteger currentModule = [PROGRESS_MANAGER currentSectionIndex];
    
    NSInteger lessonsInCurrentModule = 0;
    
    for (int i = 0; i < levelStatistic.sectionsArray.count; i++) {
        
        
        if (currentModule == i) {
            
            SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[i];
            lessonsInCurrentModule = sectionStatistic.unitsArray.count;
            break;
            
        }
        
        
    }
    
    return lessonsInCurrentModule;
  
}


#pragma mark - currentModule

- (NSInteger)currentModule
{
  return 1 + [PROGRESS_MANAGER currentSectionIndex];
}


#pragma mark - takeCurentLevelName

- (NSString *)takeCurentLevelName:(int)levelNum;
{
    
  LevelData *level;
  NSArray *arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
  
    if(levelNum < [arrayCourseLevels count]) {
        level = [arrayCourseLevels objectAtIndex:levelNum];
        return level.levelName;
    }
  
    return @"";
}


#pragma mark - takeCurentLevelName

- (NSString *)takeCurentLevelName
{
    
  LevelData *level;
  NSArray *arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
  level = [arrayCourseLevels objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
 
    return level.levelName;
}


#pragma mark - earnedStars

- (NSInteger)earnedStars
{
    
  LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[PROGRESS_MANAGER.currentLevelIndex];
  
  int numberOfStars = 0;
  
    for(int i=0; i<levelStatistic.sectionsArray.count; i++) {
        
        SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[i];
        
        for (int j=0; j<sectionStatistic.unitsArray.count; j++) {
            
            BOOL isLessonCompleted = [self isLessonCompleted:[PROGRESS_MANAGER currentLevelIndex] section:i unit:j];
            
            if (isLessonCompleted) {
                numberOfStars++;
            }
            
        }
    }
    
    
//  for(SectionStatistic *sectionStatistic in levelStatistic.sectionsArray) {
//    
//      for (UnitStatistic *unitStatistic in sectionStatistic.unitsArray) {
//          if(unitStatistic.achievement)
//              numberOfStars ++;
//      }
//  }
    
  return numberOfStars;
}


#pragma mark - weekPracticeTime

- (NSInteger)weekPracticeTime
{
  NSNumber *weekPracticeTime =[self.userStatistic.userUsageTimeArray valueForKeyPath:@"@sum.self"];
  return [weekPracticeTime intValue] / D_MINUTE;
}


#pragma mark - dailyAverageTime

- (NSInteger)dailyAverageTime
{
  return [self weekPracticeTime] / 7;
}


#pragma mark - isLevelCompletedWithHonor

- (BOOL)isLevelCompletedWithHonor:(NSInteger)level
{
    return NO;
}


#pragma mark - isLevelCompletedWithoutHonor

- (BOOL)isLevelCompletedWithoutHonor:(NSInteger)level
{
    return NO;
}


#pragma mark - haveEnoughPointsInLevel

- (BOOL)haveEnoughPointsInLevel:(NSInteger)level
{
    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[level];

    if(levelStatistic.score < kParam043)
        return NO;
    else
        return YES;
}


#pragma mark - numberOfVisitedLessonsInLevel

- (NSInteger)numberOfVisitedLessonsInLevel:(NSInteger)level
{
    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[level];
    
    int numberOfVisitedLessons = 0;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        
        for (UnitStatistic *unit in section.unitsArray) {
            
            if(unit.lastVisitedInstance != 0)
                numberOfVisitedLessons ++;
        }
   
    }

    return numberOfVisitedLessons;
}


#pragma mark - areAllLessonsVisitedInLevel

- (BOOL)areAllLessonsVisitedInLevel:(NSInteger)level
{
    if(PROGRESS_MANAGER.userStatistic.levelsArray.count == 0) {
        
        [self initProgressModel];
    }
    
    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[level];
    
    int numberOfVisitedLessons = 0;
    int allLessonsNumber = 0;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        
        for (UnitStatistic *unit in section.unitsArray) {
            allLessonsNumber ++;
            
            if(unit.isVisited)
                numberOfVisitedLessons ++;
        }
        
    }
    
    return (allLessonsNumber == numberOfVisitedLessons) ? YES : NO;
}


#pragma mark - areAllLessonsAvaliableInLevel

- (BOOL)areAllLessonsAvaliableInLevel:(NSInteger)level
{
    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[level];

    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        
        for (UnitStatistic *unit in section.unitsArray) {
           
            if(!unit.isOpen) {
                return NO;
                break;
            }
        }
        
    }
    
    return  YES;
}


#pragma mark - areAllInstancesVisited

- (BOOL)areAllInstancesVisited:(NSInteger)level
{

    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[level];
    
    int sectionIndex = 0;
    int unitIndex = 0;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        
        for (UnitStatistic *unit in section.unitsArray) {
            
            if(![self areAllInstancesVisited:level section:sectionIndex unit:unitIndex]) {
                
                return NO;
            }
            unitIndex ++;
        }
        sectionIndex ++;
    }

    return YES;
}


#pragma mark - haveEnoughStarsInLevel

- (BOOL)haveEnoughStarsInLevel:(NSInteger)level
{
    int numberOfLessons = 0;
    int numberOfLessonsWithStars = 0;
    
    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[level];

    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        for (UnitStatistic *unit in section.unitsArray) {
            
            numberOfLessons ++;
            
            if (unit.achievement) {
                numberOfLessonsWithStars ++;
            }
        }
    }
    
    float percent = (float)numberOfLessonsWithStars * 100.0 / (float)numberOfLessons;
    
    if(percent >= 99)
        return YES;
    else
        return NO;
}


#pragma mark - add play Time

- (void)addPlayTime:(CFTimeInterval)elapsedTime
{
 
  if(![REQUEST_MANAGER groupID]){
    return;
  }
  NSDate *lastSaveDateDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastSaveDateDate];
  
  if(lastSaveDateDate){
    
    //NSDate* curDate = [NSDate date];
    //NSInteger dayInt = [[[NSCalendar currentCalendar] components: NSWeekdayCalendarUnit fromDate: curDate] weekday];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit fromDate:lastSaveDateDate];
    NSUInteger weekdayToday = [components weekday];//for kLastSaveDateDate !!! not today
    NSInteger daysToSunday = (8 - weekdayToday) % 7;//for kLastSaveDateDate !!! not today
    NSDate *nextSunday = [lastSaveDateDate dateByAddingTimeInterval:60*60*24*daysToSunday];//for kLastSaveDateDate !!! not today
    DLog(@"[nextSunday timeIntervalSinceNow] %f",[nextSunday timeIntervalSinceNow]);
    if ([nextSunday timeIntervalSinceNow] < -1 * D_DAY) {
       DLog(@"POMINA");
      self.userStatistic.userUsageTimeArray  = [[NSMutableArray alloc] initWithObjects:@0,@0,@0,@0,@0,@0,@0,@0,@0,@0, nil];
      NSDate *now = [NSDate date];
      [[NSUserDefaults standardUserDefaults] setObject:now forKey:kLastSaveDateDate];
    }
    /*if(dayInt == 2){
      //is monday
      DLog(@"[lastSaveDateDate timeIntervalSinceNow] %f",[lastSaveDateDate timeIntervalSinceNow]);
      if ([lastSaveDateDate timeIntervalSinceNow] < -1 * D_DAY) {
        self.userStatistic.userUsageTimeArray = [[NSMutableArray alloc] initWithObjects:@0,@0,@0,@0,@0,@0,@0,@0,@0,@0, nil];
        NSDate *now = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:now forKey:kLastSaveDateDate];
      }
    }else if ([lastSaveDateDate timeIntervalSinceNow] < -1 * D_WEEK) {
      self.userStatistic.userUsageTimeArray  = [[NSMutableArray alloc] initWithObjects:@0,@0,@0,@0,@0,@0,@0,@0,@0,@0, nil];
      NSDate *now = [NSDate date];
      [[NSUserDefaults standardUserDefaults] setObject:now forKey:kLastSaveDateDate];
    }*/
  }
  else {
    NSDate *now = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:now forKey:kLastSaveDateDate];
  }
 
  NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
  int weekDay = (int)components.weekday - 1;
  if(weekDay == 0)  weekDay = 7;
  
  NSNumber *currentDayUsage =  [self.userStatistic.userUsageTimeArray objectAtIndex:weekDay];
  NSNumber *newDayValue = @([currentDayUsage integerValue]+elapsedTime);

  if(self.userStatistic.userUsageTimeArray) {
      [self.userStatistic.userUsageTimeArray replaceObjectAtIndex:weekDay withObject:newDayValue];
      [PROGRESS_MANAGER saveUserStatistic:self.userStatistic];
  }
  //NSLog(@"addPlayTime userUsageTimeArray %@",self.userStatistic.userUsageTimeArray);
  
  NSString *UserId = @"guest";
  if([USER_MANAGER isActive])
    UserId =[USER_MANAGER userMsisdn];
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"YYYY.MM.dd.HH.mm.ss"];
  NSString *startDate =  [dateFormatter stringFromDate:[APP_DELEGATE startSessionDate]];
  NSString *endDate =  [dateFormatter stringFromDate:[NSDate date]];
  
  if(elapsedTime != 0)
  [API_REQUEST_MANAGER sessionStatisticForUser:UserId groupId:[REQUEST_MANAGER groupID] contentId:[REQUEST_MANAGER contentID] deviceId:[USER_MANAGER deviceIdentifier] startDate:startDate endDate:endDate CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {

  }];
  
}


#pragma mark - numOfLessonToCompleteTheLevel

- (NSInteger)numOfLessonToCompleteTheLevel:(NSInteger)level
{
  
  NSInteger numberOfLesson = 0;
  LevelStatistic *levelStatistic = self.userStatistic.levelsArray[level];
  SectionStatistic *sectionStatistic;
  
  for (int i=0; i<[levelStatistic.sectionsArray count]; i++) {
    
     sectionStatistic = levelStatistic.sectionsArray[i];
      
     for (int j=0; j<[sectionStatistic.unitsArray count]; j++) {
      
         if ([self areAllInstancesVisited:level section:i unit:j]) {
             numberOfLesson++;
         }
      
     }
 }

  return numberOfLesson;
}

- (NSInteger)numOfStartLesson:(NSInteger)level {
  
    NSInteger numberOfLesson = 0;

    NSDictionary *dictMaxLessonInLevel = [self maxUnitIndexForLevel:level];
    
    NSInteger maxModuleIndex = [[dictMaxLessonInLevel objectForKey:@"modulIndex"] integerValue];
    NSInteger maxUnitIndex = [[dictMaxLessonInLevel objectForKey:@"unitIndex"] integerValue];
    
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic;
    
    if (maxModuleIndex == 0) {
        
        if (maxUnitIndex == 0) {
        
            //check if first lesson of the level is opened
            
            BOOL isFirstLessonVisited = [self isUnitVisited:level section:0 unit:0];

            if (isFirstLessonVisited) {
                
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return maxUnitIndex;
        }
    }
    else {
        
        for (int i=0; i<maxModuleIndex; i++) {
            
            sectionStatistic = levelStatistic.sectionsArray[i];
            numberOfLesson+=[sectionStatistic.unitsArray count];
            
        }
        
        if (maxModuleIndex == [levelStatistic.sectionsArray count]-1) {
        
            sectionStatistic = levelStatistic.sectionsArray[maxModuleIndex];

            if (maxUnitIndex == [sectionStatistic.unitsArray count]-1) {
                
                UnitStatistic *unitStat = [[sectionStatistic unitsArray] objectAtIndex:maxUnitIndex];
                
                if ([unitStat.instancesArray count]>0) {
                    
                    BOOL isLastLessonVisited = [self isUnitVisited:level section:maxModuleIndex unit:maxUnitIndex];
                    
                    if (isLastLessonVisited) {
                    
                        numberOfLesson+=(maxUnitIndex+1);

                    }
                    else {
                        
                        numberOfLesson+=(maxUnitIndex);
                        
                    }
                }
                else {
                    
                    numberOfLesson+=(maxUnitIndex);

                }
            }
            else {
                
                numberOfLesson+=maxUnitIndex;
            }
            
        }
        else {
            
            numberOfLesson+=(maxUnitIndex);
        
        }
        
    }
    
    
    return numberOfLesson;
}

#pragma mark - numberOfLessonsInLevel

- (NSInteger)numberOfLessonsInLevel:(NSInteger)level
{
    
    NSInteger numberOfLesson = 0;
    LevelStatistic *levelStatistic = self.userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic;
    
    for (int i=0; i<[levelStatistic.sectionsArray count]; i++) {
        
        sectionStatistic = levelStatistic.sectionsArray[i];
        
        for (int j=0; j<[sectionStatistic.unitsArray count]; j++) {
            
            numberOfLesson++;
        }
    }
    
    return numberOfLesson;
}


#pragma mark - setIsUnitVisited

- (void)setIsUnitVisited:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit
{
    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic      = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic  = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic        = sectionStatistic.unitsArray[unit];
    
    unitStatistic.isVisited = YES;
    
    [PROGRESS_MANAGER saveUserStatistic:self.userStatistic];
}


#pragma mark - isUnitVisited

- (BOOL)isUnitVisited:(NSInteger)level section:(NSInteger)section unit:(NSInteger)unit
{
    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic      = userStatistic.levelsArray[level];
    SectionStatistic *sectionStatistic  = levelStatistic.sectionsArray[section];
    UnitStatistic *unitStatistic        = sectionStatistic.unitsArray[unit];
    
    return unitStatistic.isVisited;
}


#pragma mark - maxUnitIndexForLevel

- (NSDictionary *)maxUnitIndexForLevel:(NSInteger)level
{
    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
   
    LevelStatistic *levelStatistic;
    if(level + 1 > userStatistic.levelsArray.count || userStatistic.levelsArray == nil || userStatistic.levelsArray.count == 0 ){
    
        [APP_DELEGATE buildLoginStack];
        return nil;
    }
    else {
        levelStatistic = userStatistic.levelsArray[level];
    }
    
    NSInteger maxUnitId = levelStatistic.maxUnitId;
    NSInteger modulIndex = 0;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        for (int i = 0; i < section.unitsArray.count; i++) {
            
            UnitStatistic *statistic = section.unitsArray[i];
            if(statistic.unitID == maxUnitId) {
            
                NSArray *keys = @[@"modulIndex", @"unitIndex"];

                NSString *sectionIndex = [NSString stringWithFormat:@"%ld", (long)modulIndex];
                NSString *maxUnitIndex = [NSString stringWithFormat:@"%ld", (long)i];
                
                NSArray *values = @[sectionIndex, maxUnitIndex];
                
                NSDictionary *resultDictionary = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
                return resultDictionary;
            }
                
                
        }
        
        modulIndex ++;
    }
    
    return nil;
    
}


#pragma mark - nextMaxUnitIndexForLevel

- (NSDictionary *)nextMaxUnitIndexForLevel:(NSInteger)level
{
    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic      = userStatistic.levelsArray[level];
    
    NSInteger maxUnitId = levelStatistic.maxUnitId;
    NSInteger modulIndex = 0;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        for (int i = 0; i < section.unitsArray.count; i++) {
            
            UnitStatistic *statistic = section.unitsArray[i];
            if(statistic.unitID == maxUnitId) {
                
                NSString *sectionIndex;
                NSString *maxUnitIndex;
                
                NSArray *keys = @[@"modulIndex", @"unitIndex"];
                if((levelStatistic.sectionsArray.count >= modulIndex + 1) && (section.unitsArray.count == (i+1))) {
                
                    sectionIndex = [NSString stringWithFormat:@"%ld", (long)(modulIndex + 1)];
                    maxUnitIndex = [NSString stringWithFormat:@"%d", 0];
                }
               
                else if((levelStatistic.sectionsArray.count >= modulIndex + 1) && (section.unitsArray.count > (i+1))) {
                    
                    sectionIndex = [NSString stringWithFormat:@"%ld", (long)(modulIndex)];
                    maxUnitIndex = [NSString stringWithFormat:@"%ld", (long)(i + 1)];
                }
                else {
                    sectionIndex = [NSString stringWithFormat:@"%ld", (long)modulIndex];
                    maxUnitIndex = [NSString stringWithFormat:@"%ld", (long)i];
                }
                
                NSArray *values = @[sectionIndex, maxUnitIndex];
                
                NSDictionary *resultDictionary = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
                return resultDictionary;
            }
            
            
        }
        
        modulIndex ++;
    }
    
    return nil;
    
}


#pragma mark - section index and unit index for given max unit id

- (NSDictionary *)getModuleUnitIndex:(NSInteger)levelIndex maxUnitId:(NSInteger)maxUnitId {
    
    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic      = userStatistic.levelsArray[levelIndex];
    
    NSInteger modulIndex = 0;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        for (int i = 0; i < section.unitsArray.count; i++) {
            
            UnitStatistic *statistic = section.unitsArray[i];
            if(statistic.unitID == maxUnitId) {
                
                NSArray *keys = @[@"modulIndex", @"unitIndex"];
                
                NSString *sectionIndex = [NSString stringWithFormat:@"%ld", (long)modulIndex];
                NSString *maxUnitIndex = [NSString stringWithFormat:@"%ld", (long)i];
                
                NSArray *values = @[sectionIndex, maxUnitIndex];
                
                NSDictionary *resultDictionary = [[NSDictionary alloc] initWithObjects:values forKeys:keys];
                return resultDictionary;
            }
            
            
        }
        
        modulIndex ++;
    }
    
    return nil;
}


#pragma mark - maxUnitIndexForLevel

- (void)setMaxUnitForLevel:(NSInteger)level maxUnitId:(NSInteger)nextMaxUnitId
{
    
    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic      = userStatistic.levelsArray[level];
    
    NSDictionary *dictCurrentUnitIndices = [self getModuleUnitIndex:[self currentLevelIndex] maxUnitId:levelStatistic.maxUnitId];
    NSDictionary *dictNextUnitIndices = [self getModuleUnitIndex:[self currentLevelIndex] maxUnitId:nextMaxUnitId];
    
    NSInteger currentSectionIndex = [[dictCurrentUnitIndices objectForKey:@"modulIndex"] integerValue];
    NSInteger currentUnitIndex = [[dictCurrentUnitIndices objectForKey:@"unitIndex"] integerValue];
    NSInteger nextSectionIndex = [[dictNextUnitIndices objectForKey:@"modulIndex"] integerValue];
    NSInteger nextUnitIndex = [[dictNextUnitIndices objectForKey:@"unitIndex"] integerValue];

    
    if (nextSectionIndex < currentSectionIndex) {
        return;
    }
    else if (nextSectionIndex == currentSectionIndex && nextUnitIndex <= currentUnitIndex) {
        
        return;
    }
    else {
        
        levelStatistic.maxUnitId = nextMaxUnitId;
        
        [self saveUserStatistic:userStatistic];
    }
    
    
    
}


#pragma mark - isLastLessonInLevel

- (BOOL)isLastLessonInLevel:(NSInteger)level
                    section:(NSInteger)section
                       unit:(NSInteger)unitIndex
{

    
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];

    if(section != levelStatistic.sectionsArray.count - 1)
        return NO;
    
    else {
       
        SectionStatistic *sectionStatistic = levelStatistic.sectionsArray[section];
        
        if(unitIndex != sectionStatistic.unitsArray.count - 1) return NO;
    }
    return YES;
}


#pragma mark - Set and get current index

- (void)saveCurrentLevelIndex:(NSInteger)currentLevelIndex forUser:(NSString *)user
{
        
    NSData *data                  = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserLevelIndex"];
    NSMutableDictionary *dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(dataDict.count > 0 && [dataDict objectForKey:user]) {
    
        [dataDict setValue:[NSString stringWithFormat: @"%ld", (long)currentLevelIndex] forKey:user];
    }
    else {
        
        if(!dataDict) dataDict = [NSMutableDictionary new];
        [dataDict setObject:[NSString stringWithFormat: @"%ld", (long)currentLevelIndex] forKey:user];
    }
    
    NSData *dataStore         = [NSKeyedArchiver archivedDataWithRootObject:dataDict];
    [[NSUserDefaults standardUserDefaults] setObject:dataStore forKey:@"UserLevelIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)saveCurrentModulIndex:(NSInteger)currentModulIndex forUser:(NSString *)user
{
    
    NSData *data                  = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserModulIndex"];
    NSMutableDictionary *dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(dataDict.count > 0 && [dataDict objectForKey:user]) {
        
        [dataDict setValue:[NSString stringWithFormat: @"%ld", (long)currentModulIndex] forKey:user];
    }
    else {
        
        if(!dataDict) dataDict = [NSMutableDictionary new];
        [dataDict setObject:[NSString stringWithFormat: @"%ld", (long)currentModulIndex] forKey:user];
    }
    
    NSData *dataStore         = [NSKeyedArchiver archivedDataWithRootObject:dataDict];
    [[NSUserDefaults standardUserDefaults] setObject:dataStore forKey:@"UserModulIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)saveCurrentUnitIndex:(NSInteger)currentUnitIndex forUser:(NSString *)user
{
    
    NSData *data                  = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserUnitIndex"];
    NSMutableDictionary *dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(dataDict.count > 0 && [dataDict objectForKey:user]) {
        
        [dataDict setValue:[NSString stringWithFormat: @"%ld", (long)currentUnitIndex] forKey:user];
    }
    else {
        
        if(!dataDict) dataDict = [NSMutableDictionary new];
        [dataDict setObject:[NSString stringWithFormat: @"%ld", (long)currentUnitIndex] forKey:user];
    }
    
    NSData *dataStore         = [NSKeyedArchiver archivedDataWithRootObject:dataDict];
    [[NSUserDefaults standardUserDefaults] setObject:dataStore forKey:@"UserUnitIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (NSInteger)getCurrentLevelIndexForUser:(NSString *)user
{
    NSData *data                  = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserLevelIndex"];
    NSMutableDictionary *dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(dataDict && [dataDict objectForKey:user])
        return [[dataDict objectForKey:user] integerValue];
    else
        return 0;
}


- (NSInteger)getCurrentModulIndexForUser:(NSString *)user
{
    NSData *data                  = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserModulIndex"];
    NSMutableDictionary *dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(dataDict && [dataDict objectForKey:user])
        return [[dataDict objectForKey:user] integerValue];
    else
        return 0;
}


- (NSInteger)getCurrentUnitndexForUser:(NSString *)user
{
    NSData *data                  = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserUnitIndex"];
    NSMutableDictionary *dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(dataDict && [dataDict objectForKey:user])
        return [[dataDict objectForKey:user] integerValue];
    else
        return 0;
}


#pragma mark - Set and get last visited section and modul

- (void)setLastVisitedUnitIndexForLevel:(NSInteger)level index:(NSInteger)index
{
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];
    
    levelStatistic.lastVisitedUnitIndex = index;
    
    [self saveUserStatistic:userStatistic];
}


- (void)setLastVisitedSectionIndexForLevel:(NSInteger)level index:(NSInteger)index
{
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];
    
    levelStatistic.lastVisitedSectionIndex = index;
    
    [self saveUserStatistic:userStatistic];
}


- (NSInteger)getLastVisitedUnitIndexForLevel:(NSInteger)level
{
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];
    
    return levelStatistic.lastVisitedUnitIndex;
}


- (NSInteger)getLastVisitedSectionIndexForLevel:(NSInteger)level
{
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];
    
    return levelStatistic.lastVisitedSectionIndex;
}


#pragma mark - getFirstLessonWithoutStar

- (NSInteger)getFirstLessonWithoutStar:(NSInteger)level
{
    
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];
    
    NSInteger index = -1;
    
    for (SectionStatistic *section in levelStatistic.sectionsArray) {
        
        for(int i = 0;i < section.unitsArray.count; i++){
    
            UnitStatistic *unitStatistic = section.unitsArray[i];
        
            if(!unitStatistic.achievement) {
                index = i;
                return index;
            }
        }
    }
    
    return index;
}


#pragma mark - getFirstSectionWithoutStar

- (NSInteger)getFirstSectionWithoutStar:(NSInteger)level
{
    
    UserStatistic *userStatistic   = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic = userStatistic.levelsArray[level];
    
    NSInteger index = -1;
    
    for (int j=0; j<levelStatistic.sectionsArray.count; j++) {
        
        SectionStatistic *section = (SectionStatistic *)levelStatistic.sectionsArray[j];
        
        for(int i = 0; i < section.unitsArray.count; i++){
            
            UnitStatistic *unitStatistic = section.unitsArray[i];
            
            if(!unitStatistic.achievement) {
                index = j;
                return index;
            }
        }
    }
    return index;
}

#pragma mark -

- (NSInteger)getTheNumberOfTheLesson:(NSInteger)module unit:(NSInteger)unit {

    UserStatistic *userStatistic        = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *levelStatistic      = userStatistic.levelsArray[[self currentLevelIndex]];
    
    NSInteger numberOfTheLesson = 0;
    
    if (module == 0) {
        
        return unit+1;
        
    }
    
    SectionStatistic *section;
    for (int i=0; i<module; i++) {
        
        section = [levelStatistic.sectionsArray objectAtIndex:i];
        
        numberOfTheLesson+=[section.unitsArray count];
        
    }
    
    numberOfTheLesson+=unit;
    numberOfTheLesson+=1;
    
    return numberOfTheLesson;
    
}


#pragma mark - reinit after crash

- (void)initProgressModel
{
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN];
    if(!userStatistic || userStatistic.levelsArray.count == 0) {
        ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
        [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
    }
    [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN]];
    
    
    NSLog(@"Was logged as VIVO user - %@", [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"] ? @"Yes" : @"No");
    
    if ([USER_MANAGER isNative]) {
        
        [PROGRESS_MANAGER updateProgressModel];
        
    }
    else if ([USER_MANAGER isGuest] && [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userWasLogged"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [PROGRESS_MANAGER updateProgressModel];
        
    }
}

@end
