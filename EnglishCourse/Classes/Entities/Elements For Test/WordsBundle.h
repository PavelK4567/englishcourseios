//
//  WordsBundle.h
//  K1000
//
//  Created by Action Item on 4/22/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Element.h"
#import "WordElement.h"

enum {
    wordsBundleAvailableOffline,
    wordsBundleOnline,
    wordsBundleDownloading,
    wordsBundleZipAvailableOffline
};
typedef NSInteger WordsBundleStatus;

@interface WordsBundle : Element

- (NSURL *)voiceForWordAtIndex:(int)wordOrder;

// helper functions
- (BOOL)allowOpenning;
- (BOOL)isOneAboveCurrentUserAllowedBundleIndex; // you need to also check user.canConsumeMoreBundlesToday to be sure you can open it
- (BOOL)isLocked;
- (BOOL)isDownloading;
- (BOOL)isAvailableOffline;

@property(nonatomic, unsafe_unretained) int bundleIndex;
@property(nonatomic, unsafe_unretained) WordsBundleStatus status;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSURL *imageURL;
@property(nonatomic, strong) NSURL *videoURL;
@property(nonatomic, strong) NSMutableArray *wordElements;

@property(nonatomic, strong) NSString *bundlePath;

@end
