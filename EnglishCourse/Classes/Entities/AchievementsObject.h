//
//  AchievementsObject.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 3/6/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AchievementsObject : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *fbLink;
@property (nonatomic, retain) NSString *nameOfModule;
@property (nonatomic, retain) NSString *type;
@property (nonatomic) BOOL withHonor;
@property (nonatomic, assign) NSInteger levelId;

@end
