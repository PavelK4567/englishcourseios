//
//  CourseProperties.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "CourseProperties.h"

@implementation CourseProperties

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.instructionSound = [aDecoder decodeObjectForKey:@"instructionSound"];
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.correctFeedbackSound = [aDecoder decodeObjectForKey:@"correctFeedbackSound"];
    self.incorrectFeedbackSound = [aDecoder decodeObjectForKey:@"incorrectFeedbackSound"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.instructionSound forKey:@"instructionSound"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeObject:self.correctFeedbackSound forKey:@"correctFeedbackSound"];
    [aCoder encodeObject:self.incorrectFeedbackSound forKey:@"incorrectFeedbackSound"];
    
}

- (void)updateResources {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [[Utilities sharedInstance] moveFileObjectWithName:self.instructionSound fromLocation:NSTemporaryDirectory() toLocation:documentsDirectory];
    [[Utilities sharedInstance] moveFileObjectWithName:self.correctFeedbackSound fromLocation:NSTemporaryDirectory() toLocation:documentsDirectory];
    [[Utilities sharedInstance] moveFileObjectWithName:self.incorrectFeedbackSound fromLocation:NSTemporaryDirectory() toLocation:documentsDirectory];
    
}

- (void)removeResources {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [[Utilities sharedInstance] removeFileFromPath:[NSString stringWithFormat:@"%@/%@",documentsDirectory,self.instructionSound]];
    [[Utilities sharedInstance] removeFileFromPath:[NSString stringWithFormat:@"%@/%@",documentsDirectory,self.correctFeedbackSound]];
    [[Utilities sharedInstance] removeFileFromPath:[NSString stringWithFormat:@"%@/%@",documentsDirectory,self.incorrectFeedbackSound]];
    
}

- (void)replaceIfDifferent:(NSDictionary *)dataDict
{
 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    if (![self.instructionSound isEqualToString:dataDict[kInstructionSound]]) {
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];
        
        self.instructionSound = dataDict[kInstructionSound];
    }
    
    if (![self.correctFeedbackSound isEqualToString:dataDict[kCorrectFeedbackSound]]) {
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.correctFeedbackSound];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];
        
        self.correctFeedbackSound = dataDict[kCorrectFeedbackSound];
    }
    
    if (![self.incorrectFeedbackSound isEqualToString:dataDict[kIncorrectFeedbackSound]]) {
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.incorrectFeedbackSound];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];
        
        self.incorrectFeedbackSound = dataDict[kIncorrectFeedbackSound];
    }
    
}

+ (CourseProperties *)buildCourseProperties:(NSDictionary *)dataDict {
    
    CourseProperties *courseProperties = [CourseProperties new];
    
    courseProperties.mID = [dataDict[kIDprop] integerValue];
    courseProperties.name = dataDict[kName];
    courseProperties.instructionSound = dataDict[kInstructionSound];
    courseProperties.correctFeedbackSound = dataDict[kCorrectFeedbackSound];
    courseProperties.incorrectFeedbackSound = dataDict[kIncorrectFeedbackSound];
    
    return courseProperties;
    
}

@end
