//
//  InstanceInfo.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 12/3/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface InstanceInfo : BaseEntity

@property (nonatomic, assign) NSInteger instanceID;
@property (nonatomic, assign) NSInteger order;
@property (nonatomic, assign) NSInteger pages;
@property (nonatomic, assign) NSInteger levelOrder;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) BOOL isInitial;

+ (InstanceInfo *)buildInstnaceInfo:(NSDictionary *)dataDict;

@end
