//
//  LevelData.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Section.h"
#import "Level.h"

@interface LevelData : BaseEntity

@property (nonatomic, assign) NSInteger levelID;
@property (nonatomic, copy) NSString  *levelName;
@property (nonatomic, copy) NSString  *levelDescription;
@property (nonatomic, copy) NSString  *image;
@property (nonatomic, copy) NSString  *fbLink;
@property (nonatomic, copy) NSString  *placementTestDescription;
@property (nonatomic, strong) NSArray   *sectionsArray;
@property (nonatomic, strong) Level *levelInfo;


+ (LevelData *)buildLevelData:(NSDictionary *)dataDict;

- (void)replaceResourceIfDifferent:(NSString *)stringResource;

- (BOOL)isLevelFinish;

@end
