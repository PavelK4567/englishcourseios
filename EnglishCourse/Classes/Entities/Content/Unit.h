//
//  Unit.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "InstanceInfo.h"
#import "InstanceFactory.h"
#import "SyncService.h"
#import "BaseInstance.h"


@interface Unit : BaseEntity

@property (nonatomic, assign) NSInteger unitID;
@property (nonatomic, assign) NSInteger sectionID;
@property (nonatomic, assign) NSInteger order;
@property (nonatomic, copy) NSString  *name;
@property (nonatomic, copy) NSString  *teaser;
@property (nonatomic, assign) NSInteger characterID;
@property (nonatomic, assign) NSInteger wordCount;
@property (nonatomic, assign) NSInteger grammarCount;
@property (nonatomic, copy) NSString  *untouchedImage;
@property (nonatomic, copy) NSString  *touchedImage;
@property (nonatomic, strong) NSMutableArray *instancesArray;
@property (nonatomic, strong) NSArray *instancesInfosArray;

@property (nonatomic, assign) NSInteger numberOfInstances;

+ (Unit *)buildUnit:(NSDictionary *)dataDict;

- (BOOL)replaceDifferentResources:(NSDictionary *)unitDict;

@end
