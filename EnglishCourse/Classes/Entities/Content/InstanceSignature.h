//
//  InstanceSignature.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface InstanceSignature : BaseEntity

@property (nonatomic, assign) NSInteger instanceID;
@property (nonatomic, assign) BOOL isInitial;
@property (nonatomic, copy) NSString  *md5Hash;
@property (nonatomic, assign) NSInteger type;

+ (InstanceSignature *)buildInstanceSignature:(NSDictionary *)dataDict;

@end
