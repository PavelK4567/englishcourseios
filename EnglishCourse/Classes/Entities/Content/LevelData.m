//
//  LevelData.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LevelData.h"

@implementation LevelData

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.levelID = [aDecoder decodeIntegerForKey:@"levelID"];
    self.levelName = [aDecoder decodeObjectForKey:@"levelName"];
    self.levelDescription = [aDecoder decodeObjectForKey:@"levelDescription"];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.fbLink = [aDecoder decodeObjectForKey:@"fbLink"];
    self.placementTestDescription = [aDecoder decodeObjectForKey:@"placementTestDescription"];
    self.sectionsArray = [aDecoder decodeObjectForKey:@"sectionsArray"];
    self.levelInfo = [aDecoder decodeObjectForKey:@"levelInfo"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.levelID forKey:@"levelID"];
    [aCoder encodeObject:self.levelName forKey:@"levelName"];
    [aCoder encodeObject:self.levelDescription forKey:@"levelDescription"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.fbLink forKey:@"fbLink"];
    [aCoder encodeObject:self.placementTestDescription forKey:@"placementTestDescription"];
    [aCoder encodeObject:self.sectionsArray forKey:@"sectionsArray"];
    [aCoder encodeObject:self.levelInfo forKey:@"levelInfo"];
    
}

+ (LevelData *)buildLevelData:(NSDictionary *)dataDict {
    
    LevelData *levelData = [LevelData new];
    
    levelData.levelID = [dataDict[kId] integerValue];
    levelData.levelName = dataDict[kName];
    levelData.levelDescription = dataDict[kDescription];
    levelData.image = dataDict[kImage];
    levelData.fbLink = dataDict[kFbLink];
    levelData.placementTestDescription = dataDict[kPlacementTestDescription];
    
    NSArray *sectionsArray = dataDict[kSections];
    
    NSMutableArray *sectionsDataArray = nil;
    
    if (sectionsArray && [sectionsArray count] > 0) {
        
        sectionsDataArray = [NSMutableArray arrayWithCapacity:1];
        
        for (NSDictionary *sectionDict in sectionsArray) {
            
            Section *section = [Section buildSection:sectionDict];
            
            [sectionsDataArray addObject:section];
            
        }
        
        levelData.sectionsArray = [sectionsDataArray copy];
        
    }
    
    return levelData;
    
}

- (void)replaceResourceIfDifferent:(NSString *)stringResource
{
    if (![self.image isEqualToString:stringResource]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];
        
        self.image = stringResource;
    }
}

/*- (BOOL)isLevelFinish{
  if([PROGRESS_MANAGER  areAllLessonsVisitedInLevel:self])
  return YES;
  
   return NO;
}*/
@end
