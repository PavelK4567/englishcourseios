//
//  Unit.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Unit.h"

@implementation Unit

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.unitID = [aDecoder decodeIntegerForKey:@"unitID"];
    self.sectionID = [aDecoder decodeIntegerForKey:@"sectionID"];
    self.order = [aDecoder decodeIntegerForKey:@"order"];
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.teaser = [aDecoder decodeObjectForKey:@"teaser"];
    self.characterID = [aDecoder decodeIntegerForKey:@"characterID"];
    self.wordCount = [aDecoder decodeIntegerForKey:@"wordCount"];
    self.grammarCount = [aDecoder decodeIntegerForKey:@"grammarCount"];
    self.untouchedImage = [aDecoder decodeObjectForKey:@"untouchedImage"];
    self.touchedImage = [aDecoder decodeObjectForKey:@"touchedImage"];
    self.instancesArray = [aDecoder decodeObjectForKey:@"instances"];
    self.instancesInfosArray = [aDecoder decodeObjectForKey:@"instancesInfos"];
    
    self.numberOfInstances = [aDecoder decodeIntegerForKey:@"numberOfInstances"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.unitID forKey:@"unitID"];
    [aCoder encodeInteger:self.sectionID forKey:@"sectionID"];
    [aCoder encodeInteger:self.order forKey:@"order"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.teaser forKey:@"teaser"];
    [aCoder encodeInteger:self.characterID forKey:@"characterID"];
    [aCoder encodeInteger:self.wordCount forKey:@"wordCount"];
    [aCoder encodeInteger:self.grammarCount forKey:@"grammarCount"];
    [aCoder encodeObject:self.untouchedImage forKey:@"untouchedImage"];
    [aCoder encodeObject:self.touchedImage forKey:@"touchedImage"];
    
    [aCoder encodeInteger:self.numberOfInstances forKey:@"numberOfInstances"];
    //[aCoder encodeObject:self.instancesArray forKey:@"instances"];
    //[aCoder encodeObject:self.instancesInfosArray forKey:@"instancesInfos"];
    /* NSMutableArray *instancesArrayCopy = [self.instancesArray copy];
   [aCoder encodeObject:instancesArrayCopy forKey:@"instances"];
   NSMutableArray *instancesInfosArrayCopy = [self.instancesInfosArray copy];
   [aCoder encodeObject:instancesInfosArrayCopy forKey:@"instancesInfos"];*/
    @try {
      [aCoder encodeObject:self.instancesArray forKey:@"instances"];
    }
    @catch (NSException *exception) {
      NSMutableArray *instancesArrayCopy = [self.instancesArray copy];
      [aCoder encodeObject:instancesArrayCopy forKey:@"instances"];
    }
  
    @try {
      [aCoder encodeObject:self.instancesInfosArray forKey:@"instancesInfos"];
    }
    @catch (NSException *exception) {
      NSMutableArray *instancesInfosArrayCopy = [self.instancesInfosArray copy];
      [aCoder encodeObject:instancesInfosArrayCopy forKey:@"instancesInfos"];
    }
  
}

+ (Unit *)buildUnit:(NSDictionary *)dataDict {
    
    Unit *unit = [Unit new];
    
    unit.sectionID = [dataDict[kSectionID] integerValue];
    unit.unitID = [dataDict[kId] integerValue];
    unit.untouchedImage = dataDict[kUntouchedImage];
    unit.touchedImage = dataDict[kTouchedImage];
    unit.wordCount = [dataDict[kWordCount] integerValue];
    unit.characterID = [dataDict[kCharacter] integerValue];
    unit.order = [dataDict[kOrder] integerValue];
    unit.teaser = dataDict[kTeaser];
    unit.name = dataDict[kName];
    unit.grammarCount = [dataDict[kGrammarCount] integerValue];
    
    NSArray *instancesArray = dataDict[kInstances];
    
    NSMutableArray *instancesDataArray = nil;
    
    if (instancesArray && [instancesArray count] > 0) {
        
        instancesDataArray = [NSMutableArray arrayWithCapacity:1];
        
        for (NSDictionary *instanceInfoDict in instancesArray) {
            
            InstanceInfo *instanceInfo = [InstanceInfo buildInstnaceInfo:instanceInfoDict];
            
            NSString *instanceArchiveFileName = [[NSString stringWithFormat:@"%ld", (long)[instanceInfo order]] stringByAppendingString:@".zip"];
            
            NSString *instnaceArchivePath = [[SyncService sharedInstance].sharedDataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld", (long)[instanceInfo levelOrder]]];
            
            NSString *instanceArchiveWithPathFileName = [instnaceArchivePath stringByAppendingPathComponent:instanceArchiveFileName];
            
            NSDictionary *instanceDataDict = [[Utilities sharedInstance] extractArchiveAndReturnData:instanceArchiveWithPathFileName];
            
            BaseInstance *instanceData = [InstanceFactory createInstanceForType:[instanceInfo type] instanceDataDict:instanceDataDict];
            instanceData.pages = instanceInfo.pages;
            instanceData.levelOrder = instanceInfo.levelOrder;
            instanceData.isInitial = instanceInfo.isInitial;
            
            [instancesDataArray addObject:instanceData];
            
        }
        
        unit.instancesArray = instancesDataArray;
        
    }
    
    return unit;
    
}

- (BOOL)replaceDifferentResources:(NSDictionary *)unitDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    BOOL isChangeMade = NO;
    
    if (![self.touchedImage isEqualToString:unitDict[kTouchedImage]]) {
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.touchedImage];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];

        self.touchedImage = unitDict[kTouchedImage];
        
        isChangeMade = YES;
    }
    
    if (![self.untouchedImage isEqualToString:unitDict[kUntouchedImage]]) {
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.untouchedImage];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];
        
        self.untouchedImage = unitDict[kUntouchedImage];
        
        isChangeMade = YES;
    }
    
    return isChangeMade;
}

@end
