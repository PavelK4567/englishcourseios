//
//  InstanceSignature.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "InstanceSignature.h"

@implementation InstanceSignature

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.instanceID = [aDecoder decodeIntegerForKey:@"instanceID"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.md5Hash = [aDecoder decodeObjectForKey:@"md5Hash"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.instanceID forKey:@"instanceID"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.md5Hash forKey:@"md5Hash"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (InstanceSignature *)buildInstanceSignature:(NSDictionary *)dataDict {
    
    InstanceSignature *instanceSig = [InstanceSignature new];
    
    instanceSig.instanceID = [dataDict[kId] integerValue];
    instanceSig.isInitial = [dataDict[kInitial] boolValue];
    instanceSig.md5Hash = dataDict[kMD5];
    instanceSig.type = [dataDict[kType] integerValue];
    
    return instanceSig;
    
}

@end
