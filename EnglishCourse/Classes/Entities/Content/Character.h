//
//  Character.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/19/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface Character : BaseEntity<NSCoding>

@property (nonatomic, assign) NSInteger mID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *color;

+ (Character *)buildCharacter:(NSDictionary *)dataDict;

- (void)replaceCharacterIfDifferent:(NSDictionary *)dataDict;

@end
