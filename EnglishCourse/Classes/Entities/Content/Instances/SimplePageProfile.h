//
//  SimplePageProfile.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"

@interface SimplePageProfile : BaseInstance

@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *audio;
@property (nonatomic, copy) NSString *text;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
