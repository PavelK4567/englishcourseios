//
//  Writing.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Writing.h"

@implementation Writing

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    self.questionText = [aDecoder decodeObjectForKey:@"questionText"];
    self.questionAudio = [aDecoder decodeObjectForKey:@"questionAudio"];
    self.isCaseSensitive = [aDecoder decodeBoolForKey:@"isCaseSensitive"];
    self.layout = [aDecoder decodeIntegerForKey:@"layout"];
    self.correctAnswers = [aDecoder decodeObjectForKey:@"correctAnswers"];
    self.correctFeedback = [aDecoder decodeObjectForKey:@"correctFeedback"];
    self.incorrectFeedback = [aDecoder decodeObjectForKey:@"incorrectFeedback"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.questionText forKey:@"questionText"];
    [aCoder encodeObject:self.questionAudio forKey:@"questionAudio"];
    [aCoder encodeBool:self.isCaseSensitive forKey:@"isCaseSensitive"];
    [aCoder encodeInteger:self.layout forKey:@"layout"];
    [aCoder encodeObject:self.correctAnswers forKey:@"correctAnswers"];
    [aCoder encodeObject:self.correctFeedback forKey:@"correctFeedback"];
    [aCoder encodeObject:self.incorrectFeedback forKey:@"incorrectFeedback"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    Writing *instance = nil;
    
    if (instanceDataDict) {
        instance = [Writing new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.isCaseSensitive = [instanceDataDict[kCaseSensitive] boolValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.questionAudio = instanceDataDict[kQuestionAudio];
        instance.questionText = instanceDataDict[kQuestionText];
        instance.instructionText = instanceDataDict[kInstructionText];
        instance.instructionSound = instanceDataDict[kInstructionSound];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.name = instanceDataDict[kName];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.layout = [instanceDataDict[kLayout] integerValue];
        
        NSArray *correctAnswersArray = instanceDataDict[kCorrectAnswers];
        
        NSMutableArray *correctAnswersObjectsArray = nil;
        
        if (correctAnswersArray && [correctAnswersArray count] > 0) {
            correctAnswersObjectsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSString *correctAnswer in correctAnswersArray) {
                
                [correctAnswersObjectsArray addObject:correctAnswer];
            }
            
        }
        
        instance.correctAnswers = [correctAnswersObjectsArray copy];
        
        instance.correctFeedback = [Feedback createInstanceWithData:instanceDataDict[kCorrectFeedback]];
        instance.incorrectFeedback = [Feedback createInstanceWithData:instanceDataDict[kIncorrectFeedback]];
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources:(Writing *)tempWriting
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempWriting.instructionSound isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    if (![tempWriting.questionAudio isEqualToString:self.questionAudio]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.questionAudio];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    [self.correctFeedback removeDifferentResources:tempWriting.correctFeedback.audio];
    [self.incorrectFeedback removeDifferentResources:tempWriting.incorrectFeedback.audio];
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    [self.correctFeedback removeUnusedResources:instanceDict[kCorrectFeedback]];
    [self.incorrectFeedback removeUnusedResources:instanceDict[kIncorrectFeedback]];
}

- (void)replaceWithInstance:(Writing *)instance {
    [super replaceWithInstance:instance];
    
    self.questionText = instance.questionText;
    self.questionAudio = instance.questionAudio;
    self.isCaseSensitive = instance.isCaseSensitive;
    self.layout = instance.layout;
    self.correctAnswers = [instance.correctAnswers copy];
    self.correctFeedback = instance.correctFeedback;
    self.incorrectFeedback = instance.incorrectFeedback;
    self.isInitial = instance.isInitial;
    self.title = instance.title;
}

@end
