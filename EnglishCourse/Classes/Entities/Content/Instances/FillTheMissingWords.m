//
//  FillTheMissingWords.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "FillTheMissingWords.h"

@implementation FillTheMissingWords

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    self.correctFeedback = [aDecoder decodeObjectForKey:@"correctFeedback"];
    self.leftCharacterID = [aDecoder decodeIntegerForKey:@"leftCharacterID"];
    self.rightCharacterID = [aDecoder decodeIntegerForKey:@"rightCharacterID"];
    self.segmentsArray = [aDecoder decodeObjectForKey:@"segments"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.correctFeedback forKey:@"correctFeedback"];
    [aCoder encodeInteger:self.leftCharacterID forKey:@"leftCharacterID"];
    [aCoder encodeInteger:self.rightCharacterID forKey:@"rightCharacterID"];
    [aCoder encodeObject:self.segmentsArray forKey:@"segments"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    FillTheMissingWords *instance = nil;
    
    if (instanceDataDict) {
        instance = [FillTheMissingWords new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        instance.instructionText = instanceDataDict[kInstructionText];
        instance.instructionSound = instanceDataDict[kInstructionSound];
        instance.leftCharacterID = [instanceDataDict[kLeftCharacterID] integerValue];
        instance.rightCharacterID = [instanceDataDict[kRightCharacterID] integerValue];
        
        NSArray *segmentsArray = instanceDataDict[kSegments];
        
        NSMutableArray *segmentsDataObjectsArray = nil;
        
        if (segmentsArray && [segmentsArray count] > 0) {
            segmentsDataObjectsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *segmentDict in segmentsArray) {
                SegmentMissingWords *segment = [SegmentMissingWords createInstanceWithData:segmentDict];
                
                [segmentsDataObjectsArray addObject:segment];
            }
            
        }
        
        instance.segmentsArray = [segmentsDataObjectsArray copy];
        
        instance.correctFeedback = [Feedback createInstanceWithData:instanceDataDict[kCorrectFeedback]];

        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    [self.correctFeedback removeUnusedResources:instanceDict[kCorrectFeedback]];
}

- (void)removeDifferentInstanceResources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![self.tempInstance.instructionSound isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    [self.correctFeedback removeDifferentResources:((FillTheMissingWords *)self.tempInstance).correctFeedback.audio];
}

- (void)replaceWithInstance:(FillTheMissingWords *)instance {
    [super replaceWithInstance:instance];
    
    self.correctFeedback = instance.correctFeedback;
    self.leftCharacterID = instance.leftCharacterID;
    self.rightCharacterID = instance.rightCharacterID;
    self.segmentsArray = [instance.segmentsArray copy];
    self.isInitial = instance.isInitial;
    self.title = instance.title;

}

@end
