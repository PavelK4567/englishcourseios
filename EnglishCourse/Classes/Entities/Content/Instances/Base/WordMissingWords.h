//
//  WordMissingWords.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface WordMissingWords : BaseEntity

@property (nonatomic, copy  ) NSString *word;
@property (nonatomic, copy  ) NSString *translation;
@property (nonatomic, assign) BOOL     isCorrect;

+ (WordMissingWords *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
