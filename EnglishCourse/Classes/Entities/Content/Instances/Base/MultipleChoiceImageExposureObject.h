//
//  MultipleChoiceImageExposure.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseInstance.h"

@interface MultipleChoiceImageExposureObject : BaseInstance

@property (nonatomic, strong) NSArray *wordMultipleChoiceImagesArray;

@end
