//
//  MultipleChoiceImagePractice.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Feedback.h"

@interface MultipleChoiceImagePractice : BaseEntity

@property (nonatomic, assign) NSInteger orderNumber;
@property (nonatomic, copy) NSString  *question;
@property (nonatomic, copy) NSString  *audio;
@property (nonatomic, assign) NSInteger answer;
@property (nonatomic, strong) Feedback  *correctFeedback;
@property (nonatomic, strong) Feedback  *incorrectFeedback;

+ (MultipleChoiceImagePractice *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(MultipleChoiceImagePractice *)tempPractice;

@end
