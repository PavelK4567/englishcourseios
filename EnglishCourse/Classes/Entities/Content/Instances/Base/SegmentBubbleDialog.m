//
//  SegmentBubbleDialog.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SegmentBubbleDialog.h"

@implementation SegmentBubbleDialog

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.orderNumber = [aDecoder decodeIntegerForKey:@"orderNumber"];
    self.characterID = [aDecoder decodeIntegerForKey:@"characterID"];
    self.text = [aDecoder decodeObjectForKey:@"text"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.orderNumber forKey:@"orderNumber"];
    [aCoder encodeInteger:self.characterID forKey:@"characterID"];
    [aCoder encodeObject:self.text forKey:@"text"];
    
}

@end
