//
//  WordMultipleChoiceImage.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "WordMultipleChoiceImage.h"

@implementation WordMultipleChoiceImage

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.orderNumber = [aDecoder decodeIntegerForKey:@"orderNumber"];
    self.targetText = [aDecoder decodeObjectForKey:@"targetText"];
    self.nativeText = [aDecoder decodeObjectForKey:@"nativeText"];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.sound = [aDecoder decodeObjectForKey:@"sound"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.orderNumber forKey:@"orderNumber"];
    [aCoder encodeObject:self.targetText forKey:@"targetText"];
    [aCoder encodeObject:self.nativeText forKey:@"nativeText"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.sound forKey:@"sound"];
    
}

+ (WordMultipleChoiceImage *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    
    WordMultipleChoiceImage *instance = nil;
    
    if (instanceDataDict) {
        instance = [WordMultipleChoiceImage new];
        
        instance.orderNumber = [instanceDataDict[kOrderNr] integerValue];
        instance.image = instanceDataDict[kImage];
        instance.sound = instanceDataDict[kSound];
        instance.nativeText = instanceDataDict[kNativeText];
        instance.targetText = instanceDataDict[kTargetText];
    }
    
    return instance;
    
}

- (void)removeDifferentInstanceResources:(WordMultipleChoiceImage *)tempSegment
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempSegment.image isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![tempSegment.sound isEqualToString:self.sound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.sound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kImage] isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![instanceDict[kSound] isEqualToString:self.sound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.sound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
}

@end
