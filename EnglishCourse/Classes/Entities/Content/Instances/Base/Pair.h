//
//  Pair.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface Pair : BaseEntity

@property (nonatomic, copy) NSString *nativeText;
@property (nonatomic, copy) NSString *targetText;

@end
