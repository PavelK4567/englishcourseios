//
//  Feedback.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface Feedback : BaseEntity

@property (nonatomic, copy) NSString *header;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSString *audio;
@property (nonatomic, copy) NSString *footer;

+ (Feedback *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentResources:(NSString *)resourceForRemove;

@end
