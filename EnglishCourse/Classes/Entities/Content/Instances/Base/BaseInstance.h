//
//  BaseInstance.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"
#import "Utilities.h"

@interface BaseInstance : BaseEntity

@property (nonatomic, assign) NSInteger mID;
@property (nonatomic, assign) NSInteger unitID;
@property (nonatomic, assign) NSInteger order;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) NSString  *name;
@property (nonatomic, copy) NSString  *instructionText;
@property (nonatomic, copy) NSString  *instructionSound;
@property (nonatomic, assign) NSInteger pages;
@property (nonatomic, assign) NSInteger levelOrder;
@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) BaseEntity *tempDataObj;
@property (nonatomic, strong) BaseInstance *tempInstance;

@property (nonatomic, strong) NSArray *refObjectsArray;
@property (nonatomic, assign) BOOL hasExposure;

@property (nonatomic, assign) BOOL isInitial;

- (void)removeInstanceObjects;
- (void)removeDifferentInstanceResources;
- (void)removeSpecificInstanceObject:(NSString *)instanceObject;
- (void)replaceWithInstance:(BaseInstance *)instance;
- (void)replaceWithTempDataObject;
- (void)replaceWithTempInstance;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;


@property (nonatomic, assign) NSInteger instanceNumber;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, assign) BOOL isExposure;

@end
