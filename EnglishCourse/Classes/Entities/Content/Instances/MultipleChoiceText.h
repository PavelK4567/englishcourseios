//
//  MultipleChoiceText.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "Feedback.h"
#import "Answer.h"

@interface MultipleChoiceText : BaseInstance

@property (nonatomic, copy) NSString *image;
@property (nonatomic, assign) BOOL     isLongLayout;
@property (nonatomic, copy) NSString *question;
@property (nonatomic, strong) Feedback  *correctFeedback;
@property (nonatomic, strong) Feedback  *incorrectFeedback;
@property (nonatomic, strong) NSArray  *answersArray;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;

@end
