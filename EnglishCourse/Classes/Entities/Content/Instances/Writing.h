//
//  Writing.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "Feedback.h"

@interface Writing : BaseInstance

@property (nonatomic, copy) NSString  *questionText;
@property (nonatomic, copy) NSString  *questionAudio;
@property (nonatomic, assign) BOOL      isCaseSensitive;
@property (nonatomic, assign) NSInteger layout;
@property (nonatomic, strong) NSArray   *correctAnswers;
@property (nonatomic, strong) Feedback  *correctFeedback;
@property (nonatomic, strong) Feedback  *incorrectFeedback;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(Writing *)tempWriting;

@end
