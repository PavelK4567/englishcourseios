//
//  SimplePageBubbleDialog.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SimplePageBubbleDialog.h"

@implementation SimplePageBubbleDialog

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    self.segmentsArray = [aDecoder decodeObjectForKey:@"segmentsArray"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.segmentsArray forKey:@"segmentsArray"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    SimplePageBubbleDialog *instance = nil;
    
    if (instanceDataDict) {
        instance = [SimplePageBubbleDialog new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        
        NSArray *instanceSegments = instanceDataDict[kSegments];
        
        NSMutableArray *instanceSegmentsArray = nil;
        
        if (instanceSegments && [instanceSegments count] > 0) {
            instanceSegmentsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *segmentDict in instanceSegments) {
                SegmentBubbleDialog *dialogSegment = [SegmentBubbleDialog new];
                
                dialogSegment.orderNumber = [segmentDict[kOrderNr] integerValue];
                dialogSegment.text = segmentDict[kText];
                dialogSegment.characterID = [segmentDict[kCharacterID] integerValue];
                
                [instanceSegmentsArray addObject:dialogSegment];
            }
            
            instance.segmentsArray = [instanceSegmentsArray copy];
        }
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)replaceWithInstance:(SimplePageBubbleDialog *)instance {
    [super replaceWithInstance:instance];
    
    self.segmentsArray =  [instance.segmentsArray copy];
    self.isInitial = instance.isInitial;
    self.title =  instance.title;
}
@end
