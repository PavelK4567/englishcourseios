//
//  SimplePageTextAndVideo.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"

@interface SimplePageTextAndVideo : BaseInstance

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *video;
@property (nonatomic, copy) NSString *videoPreview;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
