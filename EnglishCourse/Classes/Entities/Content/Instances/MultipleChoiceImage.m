//
//  MultipleChoiceImage.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MultipleChoiceImage.h"

@implementation MultipleChoiceImage

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    self.wordMultipleChoiceImagesArray = [aDecoder decodeObjectForKey:@"wordMultipleChoiceImages"];
    self.exposureSection = [aDecoder decodeObjectForKey:@"exposureSection"];
    self.practiceSection = [aDecoder decodeObjectForKey:@"practiceSection"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    self.refObjectsArray = [aDecoder decodeObjectForKey:@"refObjectsArray"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.wordMultipleChoiceImagesArray forKey:@"wordMultipleChoiceImages"];
    [aCoder encodeObject:self.exposureSection forKey:@"exposureSection"];
    [aCoder encodeObject:self.practiceSection forKey:@"practiceSection"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
    [aCoder encodeObject:self.refObjectsArray forKey:@"refObjectsArray"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    MultipleChoiceImage *instance = nil;
    
    if (instanceDataDict) {
        instance = [MultipleChoiceImage new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        
        NSArray *wordsArray = instanceDataDict[kWords];
        
        NSMutableArray *wordsDataObjectsArray = nil;
        
        if (wordsArray && [wordsArray count] > 0) {
            wordsDataObjectsArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *wordDict in wordsArray) {
                WordMultipleChoiceImage *wordMultipleChoiceImage = [WordMultipleChoiceImage createInstanceWithData:wordDict];

                [wordsDataObjectsArray addObject:wordMultipleChoiceImage];
            }
            
        }
        
        instance.wordMultipleChoiceImagesArray = [wordsDataObjectsArray copy];
        
        instance.practiceSection = [MultipleChoiceImagePracticeSection createInstanceWithData:instanceDataDict[kPractiseSection]];
        instance.exposureSection = [ExposureSection createInstanceWithData:instanceDataDict[kExposureSection]];
        
        if (instance.exposureSection) {
            instance.hasExposure = YES;
        }
        
        instance.refObjectsArray = instance.practiceSection.practiceSections;
        
        DLog(@"MultipleChoiceImage instance ID:%ld REF OBJ:%ld", (long)instance.mID, [instance.refObjectsArray count]);
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((MultipleChoiceImage *)self.tempInstance).instructionSound isEqualToString:self.instructionSound]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    NSArray *instanceSegments = ((MultipleChoiceImage *)self.tempInstance).wordMultipleChoiceImagesArray;
    
    NSArray *segmentsArray = self.wordMultipleChoiceImagesArray;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
        
        WordMultipleChoiceImage *segment = segmentsArray[index];
        WordMultipleChoiceImage *tempSegment = instanceSegments[index];
        
        [segment removeDifferentInstanceResources:tempSegment];
        
    }
    
    [self.practiceSection removeDifferentInstanceResources:((MultipleChoiceImage *)self.tempInstance).practiceSection];
    [self.exposureSection removeDifferentInstanceResources:((MultipleChoiceImage *)self.tempInstance).exposureSection];
    
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    NSArray *instanceSegments = instanceDict[kSegments];
    
    NSArray *segmentsArray = self.wordMultipleChoiceImagesArray;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
        
        WordMultipleChoiceImage *segment = segmentsArray[index];
        NSDictionary *segmentDict = instanceSegments[index];
        
        [segment removeUnusedResources:segmentDict];
        
    }
    
    [self.practiceSection removeUnusedResources:instanceDict[kPractiseSection]];
    [self.exposureSection removeUnusedResources:instanceDict[kExposureSection]];
    
}

- (void)replaceWithInstance:(MultipleChoiceImage *)instance {
    [super replaceWithInstance:instance];
    
    self.wordMultipleChoiceImagesArray = [instance.wordMultipleChoiceImagesArray copy];
    self.exposureSection = instance.exposureSection;
    self.practiceSection = instance.practiceSection;
    self.isInitial = instance.isInitial;
    
 }


@end
