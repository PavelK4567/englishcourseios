//
//  Practice.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "PractiseSection.h"

@interface Practice : BaseInstance

@property (nonatomic, strong) NSArray *practiceSections;

+ (Practice *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;

@end
