//
//  Section.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Section.h"

@implementation Section

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.sectionID = [aDecoder decodeIntForKey:@"sectionID"];
    self.sectionName = [aDecoder decodeObjectForKey:@"sectionName"];
    self.sectionDescription = [aDecoder decodeObjectForKey:@"sectionDescription"];
    self.sectionOrder = [aDecoder decodeIntegerForKey:@"sectionOrder"];
    self.unitsArray = [aDecoder decodeObjectForKey:@"units"];
    self.fbLink = [aDecoder decodeObjectForKey:@"fbLink"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.sectionID forKey:@"secctionID"];
    [aCoder encodeObject:self.sectionName forKey:@"sectionName"];
    [aCoder encodeObject:self.sectionDescription forKey:@"sectionDescription"];
    [aCoder encodeInteger:self.sectionOrder forKey:@"sectionOrder"];
    [aCoder encodeObject:self.unitsArray forKey:@"units"];
    [aCoder encodeObject:self.fbLink forKey:@"fbLink"];
    
}

+ (Section *)buildSection:(NSDictionary *)dataDict {
    
    Section *section = [Section new];
    
    section.sectionID = [dataDict[kSectionID] integerValue];
    section.sectionOrder = [dataDict[kSectionOrder] integerValue];
    section.sectionName = dataDict[kSectionName];
    section.sectionDescription = dataDict[kSectionDescription];
    section.fbLink = dataDict[kFbLink];
    
    NSArray *unitsArray = dataDict[kUnits];
    
    NSMutableArray *unitsDataArray = nil;
    
    if (unitsArray && [unitsArray count] > 0) {
        
        unitsDataArray = [NSMutableArray arrayWithCapacity:1];
        
        for (NSDictionary *unitDict in unitsArray) {
            
            Unit *unit = [Unit buildUnit:unitDict];
            
            [unitsDataArray addObject:unit];
            
        }
        
        section.unitsArray = [unitsDataArray copy];
        
    }
    
    return section;
    
}

@end
