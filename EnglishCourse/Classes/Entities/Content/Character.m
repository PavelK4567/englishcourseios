//
//  Character.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/19/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Character.h"

@implementation Character

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.mID = [aDecoder decodeIntegerForKey:@"id"];
    self.name = [aDecoder decodeObjectForKey:@"name"];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.color = [aDecoder decodeObjectForKey:@"color"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.mID forKey:@"id"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.color forKey:@"color"];
    
}

- (void)replaceCharacterIfDifferent:(NSDictionary *)dataDict
{
    
    if (![self.image isEqualToString:dataDict[kImage]]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *fileToBeRemoved = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:fileToBeRemoved];
        
        self.image = dataDict[kImage];
    }
    
    self.name = dataDict[kName];
    self.color = dataDict[kColor];
    
}

+ (Character *)buildCharacter:(NSDictionary *)dataDict {
    
    Character *characterInfo = [Character new];
    
    characterInfo.mID = [dataDict[kId] integerValue];
    characterInfo.name = dataDict[kName];
    characterInfo.image = dataDict[kImage];
    characterInfo.color = dataDict[kColor];
    
    return characterInfo;
    
}

@end
