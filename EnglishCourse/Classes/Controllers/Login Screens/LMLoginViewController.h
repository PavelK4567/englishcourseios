//  LMLoginViewController.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMBaseViewController.h"
#import "PurchaseManager.h"
#import "TPKeyboardAvoidingScrollView.h"

#import "Constants.h"
#import "Utilities.h"
#import "SyncService.h"
#import "SSZipArchive.h"

@interface LMLoginViewController : LMBaseViewController<SSZipArchiveDelegate, SyncServiceDelegate>

@property (nonatomic, assign) BOOL isInitialDataGenerated;

@end
