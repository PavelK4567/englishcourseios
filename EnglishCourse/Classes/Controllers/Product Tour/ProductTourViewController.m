//
//  ProductTourViewController.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "ProductTourViewController.h"

@interface ProductTourViewController (){
  NSInteger numOfPage;
}

@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIPageControl *mainPageControl;
@end

@implementation ProductTourViewController

- (void)configureAppearance
{
  [super configureAppearance];
  [self addImages];
}

- (void)configureUI
{
  
  [super configureUI];
  self.view.backgroundColor = [UIColor clearColor];
  UIView* backView = [[UIView alloc] initWithFrame:self.view.frame];
  backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
  [self.view insertSubview:backView atIndex:0];
  
  /*numOfPage = 10;
  self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, LM_WIDTH, LM_DEMO_SCROLL_HEIGHT)];
  //[self.mainScrollView scrollRectToVisible:CGRectMake(0, 0, 320, LM_SCROLL_HEIGHT) animated:YES];
  [self.mainScrollView setBackgroundColor:[UIColor clearColor]];
  [self.mainScrollView setDelegate:self];
  [self.mainScrollView setContentMode:UIViewContentModeCenter];
  self.mainScrollView.userInteractionEnabled = YES;
  self.mainScrollView.contentSize = CGSizeMake(LM_WIDTH*numOfPage, LM_DEMO_SCROLL_HEIGHT);
  //[self.mainScrollView setScrollEnabled:NO];
  [self.mainScrollView setShowsHorizontalScrollIndicator:FALSE];
  [self.mainScrollView setShowsVerticalScrollIndicator:FALSE];
  self.mainScrollView.scrollsToTop = NO;
  self.mainScrollView.pagingEnabled = YES;
  [self.view addSubview:self.mainScrollView];
  
  self.mainPageControl = [[UIPageControl alloc] init];
  self.mainPageControl.frame = CGRectMake(0,self.view.height-100,self.view.width,100);
  self.mainPageControl.numberOfPages = numOfPage;
  self.mainPageControl.currentPage = 0;
  self.mainPageControl.backgroundColor = [UIColor clearColor];
  self.mainPageControl.currentPageIndicatorTintColor = [APPSTYLE colorForType:@"Purple_Main_Color"];
  self.mainPageControl.pageIndicatorTintColor =  [UIColor whiteColor];
  [self.view addSubview:self.mainPageControl];
*/
  UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
  [button addTarget:self
             action:@selector(closeProductTour:)
              forControlEvents:UIControlEventTouchUpInside];
  [button setTitle:@"X" forState:UIControlStateNormal];
  button.frame = CGRectMake(10.0, 20.0, 44.0, 44.0);
  [self.view addSubview:button];
}

- (void)configureObservers
{
  [super configureObservers];
}
-(void)addImages{
  for(int i=0; i<numOfPage;i++){
    for(int j=0; j<3;j++){

    UIImageView *tempImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_star"]];
    [tempImageView setOrigin:CGPointMake((LM_WIDTH*i)+(LM_WIDTH/2), 132+j*100)];
    [self.mainScrollView addSubview:tempImageView];
    }
  }
}
- (void)configureNavigation
{
  [super configureNavigation];
  // self.navigationController.navigationBarHidden = YES;
}

- (void)configureData
{
  [super configureData];
 
}

- (void)loadData
{
  [super loadData];
}


#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  CGFloat pageWidth = self.mainScrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
  float fractionalPage = self.mainScrollView.contentOffset.x / pageWidth;
  NSInteger page = lround(fractionalPage);
  self.mainPageControl.currentPage = page; // you need to have a **iVar** with getter for pageControl
}

#pragma mark ProductTour
-(void) closeProductTour:(UIButton*)sender
{
  [self dismissViewControllerAnimated:YES  completion:nil];
}
@end
