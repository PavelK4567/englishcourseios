//
//  UINavigationController+Orientation.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/17/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Orientation)

@end
