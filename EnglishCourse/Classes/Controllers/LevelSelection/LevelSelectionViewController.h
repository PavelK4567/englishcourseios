//
//  LevelSelectionViewController.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/1/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LessonSelectionViewController.h"
#import "Constants.h"
#import "Utilities.h"
#import "SyncService.h"
#import "SSZipArchive.h"
#import "LevelSelectionView.h"

@interface LevelSelectionViewController : LMBaseViewController<LevelSelectionViewDelegate>{
  __weak IBOutlet UIScrollView *MainScrollView;
  __weak IBOutlet UILabel *titleLabel;
  __weak IBOutlet UILabel *screenDescriptionLabel;
  __weak IBOutlet UIButton *placementTestButton;
}

@property (nonatomic, strong) NSArray *arrayCourseLevels;

@end
