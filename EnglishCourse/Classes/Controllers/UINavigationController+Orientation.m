//
//  UINavigationController+Orientation.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/17/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "UINavigationController+Orientation.h"

@implementation UINavigationController (Orientation)

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate
{
    return YES;
}

@end
