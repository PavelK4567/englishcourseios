//
//  UserDetailsViewController.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/20/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfirmationDialog.h"
#import "TPKeyboardAvoidingScrollView.h"

@protocol UserDetailsDelegate <NSObject>
- (void)closeLevelCompletition;
@end

@interface UserDetailsViewController : LMBaseViewController <UITextFieldDelegate,ConfirmationDialogDelegate>
{
    __weak IBOutlet UITextField *prefixText;
    __weak IBOutlet UITextField *firstNameText;
    __weak IBOutlet UITextField *familyNameText;
    __weak IBOutlet UITextField *emailText;
    __weak IBOutlet UIButton *continueButton;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *descriptionLabel;
    __weak IBOutlet UILabel *firstnameLabel;
    __weak IBOutlet UILabel *familyNameLabel;
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet UILabel *mainTitle;    
    __weak IBOutlet UILabel *mailLabel;
    __weak IBOutlet TPKeyboardAvoidingScrollView *scrollView;
    __weak IBOutlet UIView *titleView;

}

@property (nonatomic) NSInteger levelId;
@property (nonatomic) BOOL resend;
@property (nonatomic,weak) id<UserDetailsDelegate>userDetailsDelegate;
@end
