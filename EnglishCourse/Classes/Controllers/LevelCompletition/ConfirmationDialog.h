//
//  ConfirmationDialog.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/23/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ConfirmationDialogDelegate

- (void)isApproved:(BOOL)status;

@end


@interface ConfirmationDialog : UIView <UIAlertViewDelegate>
{
    __weak IBOutlet UIButton *closeButton;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIButton *cancelButton;
    __weak IBOutlet UIButton *approveButton;
    __weak IBOutlet UILabel *descriptionLabel;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *nameValueLabel;
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet UILabel *emailValueLabel;
    __weak IBOutlet UIView *containerView;
    
}

@property (readonly, nonatomic, getter = isShown) BOOL shown;
@property (nonatomic, strong) NSString  *name;
@property (nonatomic, strong) NSString  *familyName;
@property (nonatomic, strong) NSString  *email;
@property (nonatomic, strong) NSString  *prefix;
@property (nonatomic)         NSInteger levelId;
@property (nonatomic)         BOOL      resend;
@property (nonatomic, weak) id<ConfirmationDialogDelegate> delegate;

- (void)showAnimated:(BOOL)animated;
- (void)showAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock;
- (void)hideAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock;

- (IBAction)hidePopup:(id)sender;
- (void)bindGUI;

@end
