//
//  MainViewController.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/1/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//test from home

#import "LMBaseViewController.h"
#import "REMenu.h"
#import "SyncService.h"

#import "TutorialViewController.h"
#import "SSZipArchive.h"

#import <ImageRecognitionFramework/ImageRecognitionFramework.h>

@interface MainViewController : LMBaseViewController<SSZipArchiveDelegate, UIScrollViewDelegate, SyncServiceDelegate, UIAlertViewDelegate>{
  REMenu *upMenu;
  UIView *upMenuTempView,*tapView;
  UITapGestureRecognizer *tap;
  BOOL menuIsOpen;
    BOOL arak;
    
    
}

@property(nonatomic, strong) CameraViewController *customCam;

@property (nonatomic, strong) UILabel *lblPoints;

@property (nonatomic, strong) TutorialViewController *tutorial;

@property NSInteger levelIndexForFirstController;
- (void)checkForChanges;

- (void)goOnTop;

@end
