//
//  CustomTitleView.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 2/12/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTitleView : UIView

@property (nonatomic, strong) UITextView *lblTitle;
- (void)setTitleOftheInstance:(NSString *)title;
-(void)setTitleForFramePresentation:(NSString *)title;

@end
