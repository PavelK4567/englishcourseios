//
//  ProgressCustomView.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "ProgressCustomView.h"

@implementation ProgressCustomView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code

    UIView *viewItemDot;
    UILabel *lblItemText;
    
    NSArray *arrayColors = @[[UIColor colorWithRed:165.0/255.0 green:211.0/255.0 blue:143.0/255.0 alpha:1.0],
                             [UIColor colorWithRed:232.0/255.0 green:125.0/255.0 blue:126.0/255.0 alpha:1.0],
                             [UIColor colorWithRed:179.0/255.0 green:177.0/255.0 blue:182.0/255.0 alpha:1.0]
                             ];
    
    NSArray *arrayMapText = @[Localized(@"T496"),
                             Localized(@"T497"),
                             Localized(@"T498")
                             ];
    
    float yPos = (rect.size.height-arrayColors.count*17)/2;
    
    for (int i=0; i<arrayColors.count; i++) {
        
        viewItemDot = [[UIView alloc] initWithFrame:CGRectMake(0, yPos+4+i*17, 9, 9)];
        [viewItemDot setBackgroundColor:arrayColors[i]];
        [self.mapContentView addSubview:viewItemDot];
        
        lblItemText = [[UILabel alloc] initWithFrame:CGRectMake(13, yPos+i*17, 100, 17)];
        [lblItemText setBackgroundColor:[UIColor clearColor]];
        [lblItemText setFont:[UIFont systemFontOfSize:12.0]];
        lblItemText.text = arrayMapText[i];
        [self.mapContentView addSubview:lblItemText];
        
        
    }
   
}



@end
