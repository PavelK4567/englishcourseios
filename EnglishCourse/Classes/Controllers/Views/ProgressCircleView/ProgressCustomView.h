//
//  ProgressCustomView.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface ProgressCustomView : UIView

@property (weak, nonatomic) IBOutlet UIView *mapContentView;
@property (weak, nonatomic) IBOutlet KAProgressLabel *progressLabel;


@end
