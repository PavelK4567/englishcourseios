//
//  LMCustomPagingControll.h
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/10/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageDotView.h"

@protocol LMCustomPagingControllDelegate <NSObject>

@required

- (void)nextButtonSelected:(id)sender;
- (void)nextButtonAction:(id)sender withSpeed:(float)speed;

- (void)previousButtonSelected:(id)sender withSpeed:(float)speed;

@end

@interface LMCustomPagingControll : UIView {
    
    BOOL isFirstInstall;
    
}

- (instancetype)initWithFrame:(CGRect)frame userProgress:(NSInteger)userProgress instances:(NSArray *)arrayInstances andDots:(NSInteger)dots;

@property (nonatomic, assign) NSInteger dotsNumber;
@property (nonatomic, assign) NSInteger pageCurrent;

@property (nonatomic, strong) UIButton *btnNext;
@property (nonatomic, strong) UIButton *btnPrevious;
@property (nonatomic, strong) UIView *viewDotsContent;
//@property (nonatomic, strong) NSMutableArray *arrayLessonInstancesStats;
@property (nonatomic, assign) NSArray *arrayLessonInstances;

@property (nonatomic, weak) id <LMCustomPagingControllDelegate> delegate;

- (void)refreshControlWithCurrentPage:(NSInteger)currentPage;
- (void)recreateControllWithCurrentPage:(NSInteger)currentPage instances:(NSArray *)arrayInstance andDots:(NSInteger)dots;

- (void)updateDotAtIndex:(int)dotIndex;


@end
