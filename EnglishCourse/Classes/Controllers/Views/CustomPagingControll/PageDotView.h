//
//  PageDotView.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/21/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageDotView : UIView

@property (nonatomic, assign) BOOL isFinished;

@end
