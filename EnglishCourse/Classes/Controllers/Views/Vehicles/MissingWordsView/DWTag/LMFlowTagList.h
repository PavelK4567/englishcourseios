//
//  LMFlowTagList.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/16/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "DWTagList.h"
#import "LMFlowTagView.h"

@interface LMFlowTagList : DWTagList<DWTagViewDelegate>


@end
