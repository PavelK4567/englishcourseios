//
//  QuestionListInterface.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/16/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol QuestionListDelegate <NSObject>

- (void)selectedPlaceholderAtPoint:(CGPoint)point
                              word:(NSString *)word
                         wordIndex:(NSInteger)index;


- (void)gameOver:(BOOL)status;

@end



