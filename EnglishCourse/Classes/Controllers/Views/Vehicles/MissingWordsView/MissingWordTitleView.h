//
//  MissingWordTitleView.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/20/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Character.h"

@interface MissingWordTitleView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *firstPersonImageView;
@property (weak, nonatomic) IBOutlet UIImageView *secondPersonImageView;
@property (weak, nonatomic) IBOutlet UILabel *firstPersonLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondPersonLabel;

@property (strong, nonatomic) Character *firstPersonItem;
@property (strong, nonatomic) Character *secondPersonItem;

- (void)bindGUI;
@end
