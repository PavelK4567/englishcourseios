//
//  MissingWordView.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MissingWordView.h"
#import "LMHelper.h"


@implementation MissingWordView


#pragma mark - loadGUI

- (void)loadGUI
{
    self.correctWordIndexes = [NSMutableArray new];
    
    [self layoutGUI];
    [self addSubview:_tagList];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{

    self.tagList = [[LMMissingWordQuestion alloc] initWithFrame:self.frame];
    [self.tagList setAutomaticResize:YES];
    
    self.array = [[LMHelper splitQuestionIntoWords:self.questionString] mutableCopy];
    self.placeholderArray = [[LMHelper placeholdersWords:self.array] mutableCopy];
    
    [self.tagList setAllWords:self.array];
    [self.tagList setTags:self.placeholderArray];
    [self.tagList setTagDelegate:self];
    [self.tagList setTagBackgroundColor:[UIColor clearColor]];
    [self.tagList setCornerRadius:kZeroPointValue];
    [self.tagList setBorderWidth:kZeroPointValue];
    [self.tagList setLabelMargin:0.0];

}


#pragma mark - addCorrectWordIndex

- (void)addCorrectWordIndex:(NSInteger)index
{
    [self.correctWordIndexes addObject:[NSNumber numberWithInteger:index]];
    self.placeholderArray[index] = [LMHelper removeCharsFromString:self.array[index]];
    [self.tagList setCorrectWordsIndexes:self.correctWordIndexes];
    [self.tagList setTags:self.placeholderArray];
    [self.tagList layoutCorrectTags];
    
    if(![self hasPlaceholderTags]) {
    
        [self setIsFinish:YES];
        [self insertAnswerView];
    }
}


#pragma mark - insert answer view

- (void)insertAnswerView
{
    UILabel *answerLabel = [UILabel new];
    
    [answerLabel setText:[[LMHelper removeSpecialChars:self.answer] stringByReplacingOccurrencesOfString:@"\n" withString:@" "]];
    
    [answerLabel setNumberOfLines:0];
    [answerLabel adjustHeightToFit];
    [answerLabel placeCenterAndBelowView:self.tagList withPadding:3.0];
    [APPSTYLE applyStyle:@"Answer_Label_Font" toLabel:answerLabel];
    [answerLabel setX:5];
    [answerLabel setWidth:self.superview.width - 20];
    answerLabel.height = 0;
    
    [self addSubview:answerLabel];
    [self resizeView];

    UIView *firstView = [self.tagList.subviews firstObject];
    if([firstView isKindOfClass:[LMTagView class]] && [((LMTagView *)firstView).word isEqualToString:@""]) {
       
        firstView.width = 0;
        
        if(self.tagList.subviews.count >= 1) {
            LMTagView *secondView = self.tagList.subviews[1];
            secondView.x = 0;
        }
        
    }
    
    [self.delegate gameOver:YES];
}


#pragma mark - MissingWord delegate

- (void)selectedTag:(NSString *)tagName
           tagIndex:(NSInteger)tagIndex
{
    LMTagView *tagView = (LMTagView *)[self.tagList.subviews objectAtIndex:tagIndex];
    
    [tagView setSelected:tagView.selected];
    [self.tagList layoutSelection:tagIndex];
    [self.delegate selectedPlaceholderAtPoint:[tagView.label convertPoint:tagView.label.center toView:self.superview.window]
                                         word:tagView.word
                                    wordIndex:tagIndex];
   
}


#pragma mark - resizeView

- (void)resizeView
{
    UIView *lastSubview = [self.subviews lastObject];
    [self setHeight:(lastSubview.y + lastSubview.height)];
}


#pragma mark - hasPlaceholderTags

- (BOOL)hasPlaceholderTags
{
    if([self.placeholderArray containsObject:kUnderlineWord])
        return YES;
    else
        return NO;
}

#pragma mark - setters

- (void)setQuestionString:(NSString *)questionString
{
    _questionString = [questionString stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    [self loadGUI];
}


#pragma mark - resizeAllViews

- (void)resizeAllViews
{
    for(UIView *view in self.subviews) {
    
        if([view isKindOfClass:[UILabel class]]) {
            
            [view sizeToFit];
            [self setHeight:(view.y + view.height)];
            break;
        }
    }
}


- (void)correctIndexOfWord
{
    int index = 0;
    
    for (int i= 0; i < self.array.count; i++) {
        if([[NSString stringWithFormat:@"<%@>",self.correctWord] isEqualToString:self.array[i]]) {
            index = i;
        }
    }
    
    
    [self addCorrectWordIndex:index];
}


#pragma mark -
@end
