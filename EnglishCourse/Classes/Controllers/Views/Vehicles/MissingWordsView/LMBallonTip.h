//
//  LMBallonTip.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/14/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMBallonTip : UIView

@property(nonatomic,strong) NSString *translatedWord;
@property (weak, nonatomic) IBOutlet UILabel *translatedWordLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImageView;
@property (nonatomic) BOOL flipLeft;
- (void)loadGUI;
@end
