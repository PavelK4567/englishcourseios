//
//  WordListInterface.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/18/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WordListDelegate <NSObject>

@optional
- (void)correctWordIndex:(NSInteger)index;
- (void)answerPoints:(int)points;
- (void)feedback:(BOOL)status;
@end

