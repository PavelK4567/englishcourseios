//
//  LMBallonTip.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/14/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMBallonTip.h"


@implementation LMBallonTip

#pragma mark - initWithFrame

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}


#pragma mark - loadGUI

- (void)loadGUI
{

    [self.translatedWordLabel setText:self.translatedWord];
    [self.translatedWordLabel sizeToFit];
    [self.translatedWordLabel setTextColor:[UIColor whiteColor]];
    [self setWidth:self.translatedWordLabel.width + 25];
    [self.translatedWordLabel setCenter:self.center];
    [self.translatedWordLabel setY:kButtonInnerPadding + 3];
    [self bringSubviewToFront:self.translatedWordLabel];
}


#pragma mark - setter

- (void)setTranslatedWord:(NSString *)translatedWord
{
    _translatedWord = translatedWord;
    
    [self loadGUI];
}

- (void)setFlipLeft:(BOOL)flipLeft
{
    _flipLeft = flipLeft;
    if(_flipLeft) {
        
        UIImage* sourceImage = self.bubbleImageView.image;
        UIImage* flippedImage = [UIImage imageWithCGImage:sourceImage.CGImage
                                                    scale:sourceImage.scale
                                              orientation:UIImageOrientationUpMirrored];
        
        [self.bubbleImageView setImage:flippedImage];
    }
}

#pragma mark -
@end
