//
//  FramesPresentationDialogScreen.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"
#import "LMAudioManager.h"

typedef enum {
  PlaybackIntroStatus = 1,
  CharacterSelectionStatus = 2,
  SpeakingRecordingModeStatus = 3,
  SpeakingFeedbackModeStatus = 4,
  DialogSummaryStatus = 5,
} FramesPresentationStatus;

@interface FramesPresentationDialogScreen : ECVehicleDisplay  <LMAudioManagerDelegate> {
  UIView *baseView;
  
  UIImageView *mainImageView;
  NSArray *segmentsArray;
  UIButton *profileSoundButton;
  UIButton *continueButton;
  UIButton *recordAgainButton;
  UIButton *recorderButton;
  
  UIProgressView *progressView;
  UIProgressView *progressViewForLongVersion;
  NSMutableArray *progresButtonsArrayForLongVersion;
  int curentFrameDialogForLongVersion;
  int numOfRecordForLongVersion;
  
  
  UIView *textView;
  NSArray *wordsFull;
  UIScrollView *mainScroll;
  int curentFrameDialog;
  int maxFrameDialog;
  NSMutableArray *progresButtonsArray;
  FramesPresentationStatus currentGameStatus;
  NSArray *allCharactersArray;
  NSMutableArray *charactersArray;
  UIScrollView *charactersView;
  Character *selectedCharacter;
  BOOL soundPlay;
  int numberOfTry;
  int numberOfCorrectWords;
  UIImageView *nuanseImage;
  
  NSTimer *stopRecordSoundTimer;
}


- (void)loadData;

@end
