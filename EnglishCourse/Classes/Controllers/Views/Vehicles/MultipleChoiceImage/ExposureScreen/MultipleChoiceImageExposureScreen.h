//
//  MultipleChoiceImageExposureScreen.h
//  ECMultiImage
//
//  Created by Dimitar Shopovski on 11/18/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InteractiveImageAnswer.h"
#import "MultipleChoiceImagePracticeScreen.h"
#import "ECVehicleDisplay.h"

@protocol MultipleChoiceImageExposureDelegate <NSObject>


@end

@interface MultipleChoiceImageExposureScreen : ECVehicleDisplay <InteractiveImageAnswerDelegate>

@property (nonatomic, strong) NSMutableArray *arrayAnswers;
@property (nonatomic, strong) NSDictionary *dictExposureInfo;
@property float width;
@property float widthBig;

@property (nonatomic) CGRect rectAnswerCenter;
@property (nonatomic) CGRect rectAnswerCenterSmall;

//This property will be set after the question label is fill with text. The height of the label + some space
@property float yPosAnswers;

@property (nonatomic, strong) NSMutableArray *arrayInteractiveImages;
@property (nonatomic, strong) InteractiveImageAnswer *currentActiveAnswer;


- (void)showTheQuestion;
- (void)startAnimationWithAnswers:(BOOL)isFirstTime;
- (void)startAnimatingTheElements;
- (void)initializeElements;

@property (nonatomic, readwrite) BOOL isAnimationFinished;
@property (nonatomic, strong) id <MultipleChoiceImageExposureDelegate> exposureDelegate;

@end
