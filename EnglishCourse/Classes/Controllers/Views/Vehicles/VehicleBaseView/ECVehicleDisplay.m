//
//  ECVehicleDisplay.m
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/13/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "ECVehicleDisplay.h"

@implementation ECVehicleDisplay

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.viewTitleLabel = [[CustomTitleView alloc] initWithFrame:CGRectMake(0, 0, LM_WIDTH_INSTACE, 82)];
        [self addSubview:self.viewTitleLabel];
                        
        [self createFeedbackView:frame];
    }
    
    return self;
}

#pragma mark - Create Feedback View

- (void)createFeedbackView:(CGRect)rect {
    
    if (self.feedbackView == nil) {
        
        self.feedbackView = [[FeedbackView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        self.feedbackView.delegateFeedback = self;
        self.feedbackView.hidden = YES;
        [self addSubview:self.feedbackView];
        
        
        self.feedbackView.viewBg.center = CGPointMake(rect.size.width/2, rect.size.height/2);

    }
}

- (void)feedbackClose:(id)sender {
    
    [self.vehicleDelegate closeFeedbackandSwipePage:[sender intValue]];
    
}

- (void)feedbackViewWasShown:(id)sender {
    
    [self.vehicleDelegate changeNavigationButtons:self];

}

#pragma mark - Reset instance and completed instance layout

- (void)resetTheInstance {
    
}

- (void)showLayoutForCompletedInstance {
    
    
}

#pragma mark enable/Disable
- (void)disableVehicleInteractions {
    
    [AUDIO_MANAGER stopRecording];
    [AUDIO_MANAGER stopPlaying];
}

- (void)enableVehicleInteractions {
    
    
}

- (void)feedSwipe
{

}
@end
