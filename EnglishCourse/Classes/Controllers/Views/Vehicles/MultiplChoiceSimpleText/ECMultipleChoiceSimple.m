//
//  ECMultipleChoiceSimple.m
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/13/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "ECMultipleChoiceSimple.h"

@implementation ECMultipleChoiceSimple

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)initializeElements {
    
    self.nPosibleAttempts = 2;
    
    self.yPosAnswersArea = 49.0;
    
    self.arrayPossibleAnswers = [[NSMutableArray alloc] init];
    
    [self drawGUI];
}

- (void)drawGUI {
    
    self.imageQuestion = [[UIImageView alloc] initWithFrame:CGRectMake((LM_WIDTH_INSTACE-widthImageMultiChoiceText)/2, 0, widthImageMultiChoiceText, heightImageMultiChoiceText)];
    [self.imageQuestion setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:self.imageQuestion];
    
    [self.viewTitleLabel setTitleOftheInstance:[(MultipleChoiceText *)self.dictVehicleInfo question]];
    self.yPosAnswersArea = self.viewTitleLabel.y + self.viewTitleLabel.height + 5;
    
    NSInteger numberoOfAnswers = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] count];
    [self showLayoutWithAnswers:numberoOfAnswers :[(MultipleChoiceText *)self.dictVehicleInfo isLongLayout]];
    
    [self bringSubviewToFront:self.feedbackView];
}


- (void)showLayoutWithAnswers:(NSInteger)numberOfAnswers :(BOOL)isLongLayout {
    
    CustomVehicleButton *customButton = nil;
    
    float widthOfAnswers;
    float heightOfAnswers = 36.0;
    float xPos, yPos = self.yPosAnswersArea;
    float spaceBetweenAnswers = 11;
    
    Answer *answer;
    
    if (isLongLayout) {
        
        widthOfAnswers = 296.0;
        xPos = (LM_WIDTH_INSTACE-widthOfAnswers)/2;

        
        if (numberOfAnswers == 3) {
            //yPos = 39;
            spaceBetweenAnswers = 13;
        }
        else if (numberOfAnswers == 4) {
            
            //yPos = 49;
            spaceBetweenAnswers = 11;
        }
        
        for (int i=0; i<numberOfAnswers; i++) {
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:i];
            
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            customButton.dictAnswerInfo = answer;
            customButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [customButton setTitleEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];

            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.delegate = self;
            [self addSubview:customButton];
            
            [self.arrayPossibleAnswers addObject:customButton];
            
            yPos+=heightOfAnswers+spaceBetweenAnswers;
        }
        
    }
    else {
        
        widthOfAnswers = (self.bounds.size.width-30)/2;
        
        if (numberOfAnswers == 3) {
        
            widthOfAnswers = 120.0;
            xPos = (LM_WIDTH_INSTACE-widthOfAnswers)/2;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:0];
            
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            customButton.dictAnswerInfo = answer;
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];

            yPos+=heightOfAnswers+15;
            xPos = (LM_WIDTH_INSTACE-2*widthOfAnswers-11)/2;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:1];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];

            xPos+=widthOfAnswers+11;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:2];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];

            
        }
        else if (numberOfAnswers == 4) {
            
            widthOfAnswers = 120.0;
            xPos = (LM_WIDTH_INSTACE-2*widthOfAnswers-15)/2;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:0];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];

            xPos+=widthOfAnswers+15;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:1];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];

            
            yPos+=heightOfAnswers+15;
            xPos = (LM_WIDTH_INSTACE-2*widthOfAnswers-15)/2;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:2];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];
            
            xPos+=widthOfAnswers+15;
            
            answer = [(NSArray *)[(MultipleChoiceText *)self.dictVehicleInfo answersArray] objectAtIndex:3];
            customButton = [CustomVehicleButton buttonWithType:UIButtonTypeCustom];
            customButton.frame = CGRectMake(xPos, yPos, widthOfAnswers, heightOfAnswers);
            [customButton setTitle:[answer text] forState:UIControlStateNormal];
            customButton.dictAnswerInfo = answer;
            customButton.delegate = self;
            [self addSubview:customButton];
            [self.arrayPossibleAnswers addObject:customButton];

        }
        
        yPos+=heightOfAnswers+spaceBetweenAnswers;
    }
    self.imageQuestion.frame = CGRectMake((LM_WIDTH_INSTACE-widthImageMultiChoiceText)/2, yPos, widthImageMultiChoiceText, heightImageMultiChoiceText);
    [self.imageQuestion setImageFromDocumentsResourceFile:[(MultipleChoiceText *)self.dictVehicleInfo image]];
    
    if (yPos + heightImageMultiChoiceText > LM_SCROLL_HEIGHT) {
        
        [self setContentSize:CGSizeMake(LM_WIDTH_INSTACE, yPos + heightImageMultiChoiceText + 20)];
    }
}

#pragma mark - Implementation delegate custom button

- (void)customButtonSelected:(id)sender {
    
    if (self.nPosibleAttempts == 0 || self.isCompleted) {
        return;
    }
    
    CustomVehicleButton *cb = (CustomVehicleButton *)sender;
    
//    if (cb == self.currentActiveAnswer)
//        return;
    
    self.currentActiveAnswer = cb;
    
    for (CustomVehicleButton *customButton in self.arrayPossibleAnswers) {
        
        if (customButton != cb && [customButton isKindOfClass:[CustomVehicleButton class]]) {
            
            [customButton resetToNormalState];
            
        }
    }
    
    if ([cb.dictAnswerInfo isCorrect]) {
        
        self.isCompleted = YES; 
        
        [self.feedbackView setInformation:YES];
        [self.feedbackView showWithAnimation];
        
        [cb setStateOfTheAnswer:YES];
        
        if (self.nPosibleAttempts == 2)
            [self.vehicleDelegate addPoints:kParam010 isLastAnswer:YES];
        else if (self.nPosibleAttempts == 1)
            [self.vehicleDelegate addPoints:kParam011 isLastAnswer:YES];

    }
    else {
                
        [cb setStateOfTheAnswer:NO];
        [AUDIO_MANAGER setDelegate:nil];
        [AUDIO_MANAGER playWrongSound];

        if (self.nPosibleAttempts == 1) {
            
            self.isCompleted = YES; 
            [self.feedbackView setInformation:NO];
            [self.feedbackView showWithAnimation];
            [self markTheRightAnswer];
            
            [self.vehicleDelegate addPoints:kParam012 isLastAnswer:NO];
            [self.vehicleDelegate finishInstanceWithIncorrectAnswer];
        }
        else {
            
            self.isCompleted = YES;
            [self performSelector:@selector(resetAfterFirstAnswer) withObject:nil afterDelay:2.0];
        }

        self.nPosibleAttempts--;
    }
    
}


#pragma mark - States

- (void)changeTheState:(id)sender {
    
    CustomVehicleButton *cb = (CustomVehicleButton *)sender;
    [cb setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    if (self.feedbackView) {
        
        [self.feedbackView fadeOutWithAnimation];
        
    }
    
}


- (void)markTheRightAnswer {
    
    for (CustomVehicleButton *customButton in self.arrayPossibleAnswers) {
        
        if ([customButton.dictAnswerInfo isCorrect]) {
            
            [customButton setStateOfTheAnswer:YES];
            //break;
        }
        
    }
    
    [self.vehicleDelegate setIsPracticeVehicleActive:NO];

}

#pragma mark - Reset the instance

- (void)resetTheInstance {
    
    self.isCompleted = NO;
    self.nPosibleAttempts = 2;
    self.currentActiveAnswer = nil;
    
    for (CustomVehicleButton *customButton in self.arrayPossibleAnswers) {
        
        [customButton resetToNormalState];
        
    }
    
}

- (void)showLayoutForCompletedInstance {

    for (CustomVehicleButton *customButton in self.arrayPossibleAnswers) {
        
        [customButton resetToNormalState];
        
    }
    [self markTheRightAnswer];

}

#pragma mark - Reset after first answer

- (void)resetAfterFirstAnswer {
    
    self.isCompleted = NO;
    [self.currentActiveAnswer resetToNormalState];
    
}

#pragma mark - Feedback delegate methods

- (void)feedbackClose:(id)sender {
    
    [self showLayoutForCompletedInstance];
    [self.vehicleDelegate closeFeedbackandSwipePage:[sender intValue]];

}

@end
