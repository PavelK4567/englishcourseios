//
//  SimplePageDialogScreen.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"

#import "NSBubbleData.h"
#import "LMBubbleView.h"

#import "Character.h"

@interface SimplePageDialogScreen : ECVehicleDisplay<LMBubbleViewDelegate> {
    
    BOOL isViewGenerated;
    
}

@property (weak, nonatomic) IBOutlet UIScrollView *bubbledScrollView;
@property (strong, nonatomic) UIView *nibView;

@property (nonatomic, strong) Character *characterFirst;
@property (nonatomic, strong) Character *characterSecond;

@property (nonatomic, strong) NSArray *charactersArray;
@property (nonatomic, strong) NSArray *segmentsArray;
@property (nonatomic, strong) NSMutableArray *bubblesDataArray;

- (void)initData;
- (void)drawInstance;

@end
