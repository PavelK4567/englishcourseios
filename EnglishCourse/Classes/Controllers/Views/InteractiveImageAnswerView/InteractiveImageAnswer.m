//
//  InteractiveImageAnswer.m
//  ECMultiImage
//
//  Created by Dimitar Shopovski on 11/18/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "InteractiveImageAnswer.h"
#import "ECVehicleDisplay.h"

@implementation InteractiveImageAnswer

- (void)drawGUIElements {
    
    self.imgAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self.imgAnswer setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.imgAnswer setContentMode:UIViewContentModeScaleToFill];
    self.imgAnswer.layer.cornerRadius = 6.0;
    self.imgAnswer.layer.masksToBounds = YES;
    
    [self addSubview:self.imgAnswer];
    
    self.viewTextAnswer = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-22, self.frame.size.width, 22)];
    [self.viewTextAnswer setBackgroundColor:[APPSTYLE colorForType:@"Bg_Multichoice_Normal"]];
    [self.viewTextAnswer setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    self.lblTextAnswer = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 22)];
    [self.lblTextAnswer setBackgroundColor:[UIColor clearColor]];
    self.lblTextAnswer.textAlignment = NSTextAlignmentCenter;
    self.lblTextAnswer.textColor = [UIColor blackColor];
    [self.lblTextAnswer setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.0]];
    
    [self.lblTextAnswer setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.viewTextAnswer addSubview:self.lblTextAnswer];
    
    if (self.viewTranslationBallon == nil) {
        
        self.viewTranslationBallon = [[UIView alloc] initWithFrame:CGRectMake(0, self.viewTextAnswer.frame.origin.y-38, self.frame.size.width, 38)];
        [self.viewTranslationBallon setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
        self.viewTranslationBallon.alpha = 0.0;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = 1;
        UIImage *normalImage = [UIImage imageNamed:@"bubble"];
        
        
        UIImage *resizableButton = [normalImage resizableImageWithCapInsets:UIEdgeInsetsMake(2, 22, 5, 22)];
        
        [button setBackgroundImage:resizableButton forState:UIControlStateNormal];
        
        [button.titleLabel setFont:[UIFont systemFontOfSize:11.0]];
        [button setTitle:[self.answerInfo nativeText] forState:UIControlStateNormal];
        [button sizeToFit];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(kZeroPointValue, kButtonInnerPadding, kZeroPointValue, kButtonInnerPadding)];
        [button setX:0];
        
        button.width += (2 * kButtonInnerPadding);
        button.height = kMissingWordButtonHeight;
        
        button.x = self.viewTranslationBallon.frame.size.width/2 - button.width/2;
        
        [self.viewTranslationBallon addSubview:button];
        
        [self addSubview:self.viewTranslationBallon];
        
    }
    
    UITapGestureRecognizer *tapTextAnswer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textAnswerTapped:)];
    [self.viewTextAnswer addGestureRecognizer:tapTextAnswer];
    
    [self roundView:self.viewTextAnswer onCorner:UIRectCornerBottomLeft|UIRectCornerBottomRight radius:6.0];
    
    [self addSubview:self.viewTextAnswer];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(answerTapped:)];
    [self addGestureRecognizer:tap];
    
    [self.layer setCornerRadius:6];
    [self.layer setMasksToBounds:NO];
    
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowRadius = 5;
    self.layer.shadowOpacity = 0.5;
    
    
    [self setAutoresizesSubviews:YES];
    
}

- (void)bindData {
    
    self.lblTextAnswer.text = [self.answerInfo targetText];
    
    self.imgAnswer.image = [UIImage imageFromDocumentsResourceFile:[self.answerInfo image]];
    
    self.imgAnswer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self.imgAnswer setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.imgAnswer setContentMode:UIViewContentModeScaleToFill];
    
    if ([self.answerInfo sound] && self.isExposureAnswer) {
        
        if (self.imgSound == nil) {
            
            self.imgSound = [[UIImageView alloc] initWithFrame:CGRectMake(6, 3, 20, 23)];
            [self.imgSound setImage:[UIImage imageNamed:@"ic_small_sound2"]];
            [self.imgSound setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            
            [self addSubview:self.imgSound];
        }
    }
}


#pragma mark -
#pragma mark - Helper method round corners of view

- (void)roundView:(UIView *)view onCorner:(UIRectCorner)rectCorner radius:(float)radius {
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                   byRoundingCorners:rectCorner
                                                         cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    [view.layer setMask:maskLayer];
    
}

- (IBAction)textAnswerTapped:(id)sender {
    
    [self.delegate translationWasSelected:self];
    
}

#pragma mark - Answer selected

- (IBAction)answerTapped:(id)sender {
    
    if (self.answerAnimationInProgress) {
        
        self.answerAnimationInProgress = NO;
        
        [self.layer removeAllAnimations];
        [UIView animateWithDuration:0.2 animations:^{
            
            self.transform = CGAffineTransformMakeScale(1.0, 1.0);
            [self setFrame:self.rectPosition];
            
            
        } completion:^(BOOL finished) {
            
            
        }];
    }
    
    [self.delegate answerWasSelected:self];
    
    ////////////////////////////////////////
    //TO DO: call sound manager
    //play the sound
    
}

- (void)emphasizeAnswerTo:(CGRect)centerRect from:(CGRect)startingRect {
    
    [self playTheSound];
    
    [(ECVehicleDisplay *)[self superview] setAnimationInProgress:YES];
    
    [self.layer setBorderColor:[UIColor greenColor].CGColor];
    
    [UIView animateWithDuration:1.5 animations:^{
        
        self.transform = CGAffineTransformMakeScale(1.8, 1.8);
        self.center = CGPointMake(centerRect.origin.x+centerRect.size.width/2, centerRect.origin.y+centerRect.size.height/2);
        
    } completion:^(BOOL finished) {
        
        [self backAnswerToPreviousState:startingRect];
        
    }];
    
}


#pragma mark - Play Sound of the answer

- (void)playTheSound {
    
    [AUDIO_MANAGER playAudioFromDocumentPath:[self.answerInfo sound] componentSender:self.imgSound];
    
}

#pragma mark - State of answer

- (void)backAnswerToPreviousState:(CGRect)previousRect {
    
    [(ECVehicleDisplay *)[self superview] setAnimationInProgress:NO];
    
    [UIView animateWithDuration:1.0 animations:^{
        
        [self.layer setBorderColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0].CGColor];
        self.center = CGPointMake(previousRect.origin.x+previousRect.size.width/2, previousRect.origin.y+previousRect.size.height/2);
        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
    }];
    
}

- (void)setStateOfTheAnswer:(BOOL)state {
    
    [self.layer setBorderWidth:1];

    
    [self.lblTextAnswer setTextColor:[UIColor whiteColor]];

    if (state) {
        
        [self.layer setBorderColor:[APPSTYLE colorForType:@"Correct_Answer"].CGColor];
        [self.viewTextAnswer setBackgroundColor:[APPSTYLE colorForType:@"Correct_Answer"]];
        
    }
    else {
        
        [self.layer setBorderColor:[APPSTYLE colorForType:@"Incorrect_Answer"].CGColor];
        [self.viewTextAnswer setBackgroundColor:[APPSTYLE colorForType:@"Incorrect_Answer"]];
    }
}


- (void)showTranslationToNativeLanguage {
    
    if (!self.isExposureAnswer) {
        return;
    }
    UIButton *button = (UIButton *)[self.viewTranslationBallon viewWithTag:1];
    if (button)
        [button setTitle:[self.answerInfo nativeText] forState:UIControlStateNormal];
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.viewTranslationBallon.alpha = 1.0;
    }];
    
}

- (void)hideTranslationBallon {
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.viewTranslationBallon.alpha = 0.0;
    }];
}

- (void)backTheAnswerInNormalState {
    
    [self.layer setBorderWidth:0];

    [self setBackgroundColor:[UIColor clearColor]];
    [self.lblTextAnswer setTextColor:[UIColor blackColor]];
    [self.viewTextAnswer setBackgroundColor:[APPSTYLE colorForType:@"Bg_Multichoice_Normal"]];
    [self.layer setBorderColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0].CGColor];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

@end
