//
//  InteractiveImageAnswer.h
//  ECMultiImage
//
//  Created by Dimitar Shopovski on 11/18/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Extras.h"

@protocol InteractiveImageAnswerDelegate <NSObject>

@required

- (void)answerWasSelected:(id)sender;
- (void)translationWasSelected:(id)sender;

@end

@interface InteractiveImageAnswer : UIView

@property (nonatomic, strong) WordMultipleChoiceImage *answerInfo;
@property (nonatomic, assign) CGRect rectPosition;
@property (nonatomic, assign) CGRect rectCentralPosition;
@property (nonatomic, assign) BOOL answerAnimationInProgress;

@property (nonatomic, strong) UIImageView *imgAnswer;
@property (nonatomic, strong) UIImageView *imgSound;
@property (nonatomic, strong) UIView *viewTextAnswer;
@property (nonatomic, strong) UILabel *lblTextAnswer;
@property (nonatomic, assign) BOOL isExposureAnswer;

//floating ballon
@property (nonatomic, strong) UIView *viewTranslationBallon;
@property (nonatomic, strong) UILabel *lblTranslationText;

@property (nonatomic, weak) id<InteractiveImageAnswerDelegate> delegate;

- (void)emphasizeAnswerTo:(CGRect)centerRect from:(CGRect)startingRect;
- (void)showTranslationToNativeLanguage;
- (void)hideTranslationBallon;
- (void)backTheAnswerInNormalState;

- (void)setStateOfTheAnswer:(BOOL)state;

- (void)playTheSound;

- (void)drawGUIElements;
- (void)bindData;

@end
