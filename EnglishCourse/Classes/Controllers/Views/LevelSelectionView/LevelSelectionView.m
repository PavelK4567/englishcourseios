//
//  LevelSelectionView.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LevelSelectionView.h"

@implementation LevelSelectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setViewData:(LevelData *)level :(int)startNumber {
  
  self.levelInfo = level;
  [APPSTYLE applyStyle:@"Label_Title" toLabel:self.levelTitleLabel];
  [APPSTYLE applyStyle:@"Label_Body" toLabel:self.levelInfoLabel];
  self.levelTitleLabel.text = level.levelName;
  
  [self.levelImageView setImageFromDocumentsResourceFile:level.image];
  self.layer.cornerRadius = 2;
  
  int totalNumberOfLessons = startNumber-1;
  
  for (Section *obj in level.sectionsArray) {
    
    totalNumberOfLessons+=obj.unitsArray.count;
  }
  /*if([level isLevelFinish]){
    self.chekImageView.alpha = 1;
  }else{
    self.chekImageView.alpha = 0;
  }*/
  [APPSTYLE applyTitle:Localized(@"T425") toButton:self.moreInfoButton];
  self.levelInfoLabel.text = [NSString stringWithFormat:Localized(@"T424") , startNumber, totalNumberOfLessons];
  
}

- (IBAction)moreInfoButtonClick:(id)sender {
    
    DLog(@"more button clicked");
  [self.delegate moreInfoButtonClick:self];
}
@end
