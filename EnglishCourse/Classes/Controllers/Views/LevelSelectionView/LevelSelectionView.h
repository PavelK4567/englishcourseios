//
//  LevelSelectionView.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LevelSelectionViewDelegate <NSObject>

@required

- (void)moreInfoButtonClick:(id)sender;

@end

@interface LevelSelectionView : UIView

@property(weak,nonatomic) IBOutlet UILabel *levelInfoLabel;
@property(weak,nonatomic) IBOutlet UILabel *levelTitleLabel;
@property(weak,nonatomic) IBOutlet UIImageView *levelImageView;
@property(weak,nonatomic) IBOutlet UIImageView *chekImageView;
@property(weak,nonatomic) IBOutlet UIButton *moreInfoButton;
@property (nonatomic, weak) id <LevelSelectionViewDelegate> delegate;

-(void)setViewData:(LevelData *)level :(int)startNumber;

@property (nonatomic, strong) LevelData *levelInfo;


@end
