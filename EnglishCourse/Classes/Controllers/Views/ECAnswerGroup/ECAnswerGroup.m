//
//  ECAnswerGroup.m
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/26/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "ECAnswerGroup.h"

@implementation ECAnswerGroup

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    
  self = [super initWithFrame:frame];
  
  if (self) {
    
        self.arrayAnswers = [[NSMutableArray alloc] init];
        self.centerPoint = CGPointMake(frame.size.width/2, frame.size.height/2);
        
    }
    
    
    return self;
}

- (void)didAddSubview:(UIView *)subview {
    
    [self.arrayAnswers addObject:subview];
    
}

#pragma mark - Custom Vehicle button delegate

- (void)customButtonSelected:(id)sender {
    
    self.currentSelectedButton = (CustomVehicleButton *)sender;
    
    
}

- (void)selectTheCorrectAnswer {
    
    if (self.isRadioGroup) {
        
        for (CustomVehicleButton *customButton in self.arrayAnswers) {
            
            if (customButton != self.currentSelectedButton) {
                
                customButton.userInteractionEnabled = NO;
            }
            
        }
        
        [self performSelector:@selector(enableAllButtonsInTheGroup) withObject:nil afterDelay:2.0];
    }
}

- (void)enableAllButtonsInTheGroup {
    
    for (CustomVehicleButton *customButton in self.arrayAnswers) {
        
        customButton.userInteractionEnabled = YES;
        
    }
}


@end
