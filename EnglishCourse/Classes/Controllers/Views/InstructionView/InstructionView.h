//
//  InstructionView.h
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/25/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Extras.h"

@protocol InstructionViewDelegate <NSObject>


@end

@interface InstructionView : UIView

@property (nonatomic, strong) IBOutlet UIView *viewInstructionTextBackground;
@property (nonatomic, strong) IBOutlet UIView *viewForGestures;
@property (nonatomic, strong) IBOutlet UITextView *txtInstructionText;
@property (nonatomic, weak) IBOutlet UIButton *btnInstructionButton;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong)  UISwipeGestureRecognizer *swipeGestureRecognizer;

@property (nonatomic, strong) NSString *strInstructionText;
@property (nonatomic, strong) NSString *strInstructionSound;

@property (nonatomic, weak) id<InstructionViewDelegate> instructionDelegate;

@property (nonatomic) BOOL isExpanded;

@property (nonatomic) CGRect originFrame;

- (void)playInstructionSound:(NSString *)instructionSound;
- (void)setTheInstructionText:(NSString *)instructionText;

- (void)collapseInstructionAfterGestureEvent;

- (void)openInstructionView;

@end
