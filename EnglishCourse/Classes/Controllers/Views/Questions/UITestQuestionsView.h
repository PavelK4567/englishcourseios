//
//  UITestQuestionsScrollView.h
//  K1000
//
//  Created by Action-Item on 5/5/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "Question.h"
#import "TestCategory.h"

@class UITestQuestionsView;

@protocol UITestQuestionsViewDelegate <NSObject>

@optional
- (void)testQuestionsView:(UITestQuestionsView *)view answerSelected:(int)index;

@end

@interface UITestQuestionsView : UIView {
    __weak IBOutlet UILabel *_questionLabel;
    __weak IBOutlet UILabel *_questionTitleNoImageLabel;
    __weak IBOutlet UILabel *_questionTitleWithImageLabel;

    __weak IBOutlet UIView *_questionWithImageView;
    __weak IBOutlet UIView *_questionNoImageView;

    __weak IBOutlet UIImageView *_questionImageView;

    __weak IBOutlet UIView *_buttonsView;
    __weak IBOutlet UIButton *_answerButtonOne;
    __weak IBOutlet UIButton *_answerButtonTwo;
    __weak IBOutlet UIButton *_answerButtonThree;
    __weak IBOutlet UIButton *_answerButtonFour;
}

- (IBAction)answerButtonClicked:(UIButton *)button;

- (void)setTestCategory:(TestCategory *)testCategory
           withQuestion:(Question *)question
        andDefaultColor:(UIColor *)defaultColor
       andSelectedColor:(UIColor *)SelectedColor;

@property(nonatomic, weak) id<UITestQuestionsViewDelegate> delegate;
@property(nonatomic, strong, readonly) TestCategory *testCategory;
@property(nonatomic, strong, readonly) Question *question;
@property(nonatomic, strong, readonly) UIColor *defaultColor;
@property(nonatomic, strong, readonly) UIColor *selectedColor;
@property(nonatomic, strong) UIView *tempButtonsView;
@end
