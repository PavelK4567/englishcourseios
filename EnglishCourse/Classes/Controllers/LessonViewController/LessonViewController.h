//
//  LessonViewController.h
//  ECPractices
//
//  Created by Dimitar Shopovski on 12/1/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMCustomPagingControll.h"

#import "LessonSelectionViewController.h"

#import "ECMultipleChoiceSimple.h"
#import "MultiChoiceImages.h"
#import "MultipleChoiceImageExposureScreen.h"
#import "MultipleChoiceImagePracticeScreen.h"
#import "SimplePageProfileScreen.h"
#import "WritingVehicleScreen.h"
#import "InstructionView.h"
#import "SimplePageDialogScreen.h"
#import "SimplePageTextAndVideoScreen.h"
#import "FramesPresentationDialogScreen.h"
#import "MultipleChoiceTextProgressiveChatScreen.h"
#import "MissingWordContainerViewL1.h"
#import "MissingWordContainerViewL2.h"
#import "MultipleChoiceImagePracticeObject.h"
#import "MultipleChoiceImageExposureObject.h"
#import "PracticeQuestionScreen.h"
#import "PracticeSectionExtra.h"
#import "LessonSummary.h"

#import "LMDownloadManager.h"
#import "SyncService.h"


@interface LessonViewController : LMBaseViewController <UIScrollViewDelegate, LMCustomPagingControllDelegate, MultipleChoiceImageExposureDelegate, ECVehicleDisplayDelegate, InstructionViewDelegate, LessonSummaryDelegate, MultipleChoiceTextProgressiveChatDelegate, UIGestureRecognizerDelegate> {
    
    NSArray *arrayStages;
    
    NSInteger kNumberOfPages;
    
    NSInteger lastPage, clickedIndex;
    
    BOOL pageControlUsed;
    BOOL isRefreshInProgress;
    BOOL isPracticeActive;
    BOOL isSummaryPage;
    BOOL isPracticePage;
    
    NSInteger unitScore;
    

}

@property (nonatomic, weak) IBOutlet UIView *viewNavBar;
@property (nonatomic, weak) IBOutlet UILabel *lblPoints;
@property (nonatomic, weak) IBOutlet UILabel *lblLessonTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnCloseTitle;

@property (nonatomic) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (nonatomic, strong) UIScrollView *scrollViewInstanceContainer;

@property (nonatomic, strong) NSArray *arrayDataSource;
@property (nonatomic, assign) NSArray *arrayLevelDataSource;
@property (nonatomic, strong) NSMutableArray *arrayLessonInstanceStats;
@property (nonatomic, assign) NSIndexPath *currentIndex;
@property (nonatomic, assign) NSInteger currentLevelIndex;

@property (nonatomic, assign) BOOL isViedoActive;

@property (nonatomic, strong) LMCustomPagingControll *lmPagingControl;

- (void)loadScrollViewWithPage:(NSInteger)page;
- (void)initialScroll:(NSArray *)arraySource;
- (void)refreshContentWithStartPage:(BOOL)isStartPage;
- (void)jumpToFirstSkippedOrWrongAnswered:(id)sender;


@property (nonatomic, readwrite) BOOL isScrollEnable;
@property (nonatomic, strong) InstructionView *instructionView;

@property (nonatomic, assign) Unit *lessonInfo;

@property (nonatomic, assign) UITextView *actualTextView;

@end
