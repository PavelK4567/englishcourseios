//
//  SplashScreenViewController.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 12/9/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMBaseViewController.h"

#import "Constants.h"
#import "Utilities.h"
#import "SyncService.h"
#import "SSZipArchive.h"

@interface SplashScreenViewController : LMBaseViewController<SSZipArchiveDelegate, SyncServiceDelegate>

@end
