//
//  SplashScreenViewController.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 12/9/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "ProgressMenager.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (void)configureAppearance
{
  [self addActivityIndicator];
  self.view.backgroundColor = [APPSTYLE colorForType:@"Native_Pink_Color"];
}

- (void)loadData {
    [super loadData];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:kDataExtracted]) {
        [[Utilities sharedInstance] extractInitialData:self];
    } else {
        [DATA_SERVICE loadDataModel];
        
        [APP_DELEGATE buildMainStackWithLevelIndex:[PROGRESS_MANAGER currentLevelIndex]];
    }
//
//    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
//    if(!userStatistic) {
//        ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
//        [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
//    }
//    [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]]];
//    [PROGRESS_MANAGER updateProgressModel];
    
}

#pragma mark -
#pragma mark - SSZipArchive delegate

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:kDataExtracted];
    [defaults setBool:NO forKey:kDataCompleteDownload];
    [defaults synchronize];
    
    [[SyncService sharedInstance] buildInitialDataModel:self];
}

#pragma mark -
#pragma mark - SyncService delegate

- (void)returnBuildStatusWithError:(BOOL)hasError errorMessage:(NSString *)errorMessage {
    [APP_DELEGATE buildMainStackWithLevelIndex:0];
}

-(BOOL)shouldAutorotate
{

    
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
