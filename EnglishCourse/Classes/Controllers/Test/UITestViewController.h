//
//  UIQuestionViewController.h
//  K1000
//
//  Created by Action-Item on 5/5/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

//#import "UIBaseViewController.h"
#import "UITestQuestionsView.h"
#import "UIFeedbackViewController.h"
#import "TestCategory.h"

@interface UITestViewController : LMBaseViewController <UITestQuestionsViewDelegate>{
    __weak IBOutlet UIScrollView *_scrollView;

    __weak IBOutlet UILabel *_questionIndexLabel;
    __weak IBOutlet UIButton *_nextQuestionButton;
    __weak IBOutlet UIButton *_previousQuestionButton;

    NSArray *_testCategoryArray;

    NSArray *_selectedColorArray;
    NSArray *_defaultColorArray;
    BOOL FeedbackScreenisOpen;
}

- (IBAction)nextQuestionClicked;
- (IBAction)previousQuestionClicked;

@property(nonatomic, strong) NSIndexPath *currentIndexPath;

@end
