//
//  ModuleCertificateView.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ModuleCertificateView : UIView
{
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIImageView *certificateImageView;
    __weak IBOutlet UILabel *certificateText;
    __weak IBOutlet UIButton *facebookButton;
    __weak IBOutlet UILabel *congratulationsLabel;
    __weak IBOutlet UIView *alertView;
    __weak IBOutlet UILabel *alertLabel;
    __weak IBOutlet UIButton *closeButton;
}

@property (readonly, nonatomic, getter = isShown) BOOL shown;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *shareText;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *congradulationsText;
@property (nonatomic, strong) NSString *alertText;
@property (nonatomic)         NSInteger modulNumber;

- (void)showAnimated:(BOOL)animated;
- (void)showAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock;
- (void)hideAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock;
- (IBAction)hidePopup:(id)sender;

@end
