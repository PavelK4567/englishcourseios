//
//  InstanceFactory.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/24/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

#import "BaseEntity.h"
#import "BaseInstance.h"

#import "SimplePageProfile.h"
#import "SimplePageTextAndVideo.h"
#import "SimplePageBubbleDialog.h"
#import "SegmentBubbleDialog.h"
#import "MultipleChoiceTextProgressiveChat.h"
#import "MultipleChoiceImage.h"
#import "MultipleChoiceText.h"
#import "FillTheMissingWords.h"
#import "Writing.h"
#import "Practice.h"
#import "FramesPresentationDialog.h"

@interface InstanceFactory : NSObject

+ (BaseInstance *)createInstanceForType:(NSInteger)type instanceDataDict:(NSDictionary *)instanceDataDict;

@end
