//
//  GalleryViewController.h
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/12/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface GalleryViewController : UIViewController<UIAlertViewDelegate>

@property (nonatomic, copy) NSString *lang;

@end
