//
//  CameraViewController.h
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/7/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "ImagePreviewViewController.h"
#import "GalleryViewController.h"

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface CameraViewController : UIViewController

@property (nonatomic, retain) ImagePreviewViewController *preview;
@property (nonatomic, retain) GalleryViewController *gallery;

@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, copy) NSString *lng;

@end
