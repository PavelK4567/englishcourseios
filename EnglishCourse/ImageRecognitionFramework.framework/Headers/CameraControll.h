//
//  CameraControll.h
//  ImageRecognitionFramework
//
//  Created by Aleksandar Mitrovski on 6/5/18.
//  Copyright © 2018 Aleksandar Mitrovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface CameraControll : NSObject<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, assign) bool isGallery;
@property (nonatomic, strong) UIViewController *holder;

-(void) openCameraOnViewController: (UIViewController *) vc;
-(void) openPhotoLibOnViewController: (UIViewController *) vc;

@end
