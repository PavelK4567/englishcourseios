//  KeyboardAvoidingScrollView.h
//  Created by Dimitar Tasev on 20140331.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.



@interface KeyboardAvoidingScrollView : UIScrollView <UITextFieldDelegate, UITextViewDelegate>

- (void)contentSizeToFit;
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;

@end
