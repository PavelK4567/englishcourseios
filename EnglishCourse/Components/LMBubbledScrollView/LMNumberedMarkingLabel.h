//
//  LMNumberedMarkingLabel.h
//  LMBubbledScrollView
//
//  Created by Zarko Popovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Extras.h"

#import "Answer.h"

#import "LMAudioManager.h"

@protocol LMNumberedMarkingLabelDelegate <NSObject>

- (void)didPressItemAtIndex:(NSInteger)itemIndex correctAnswer:(BOOL)correctAnswer;
- (void)didPressItemAtIndex:(NSInteger)itemIndex correctAnswer:(BOOL)correctAnswer sender:(id)sender;
- (void)executedItemWithTag:(NSInteger)itemTag;
- (void)enableLabelsForExecuting;

@optional

- (void)returnCalculatedPoints:(NSInteger)points;

@end

@interface LMNumberedMarkingLabel : UIView<LMAudioManagerDelegate>

@property (nonatomic, assign) BOOL validStatus;
@property (nonatomic, assign) NSInteger viewIndex;

@property (nonatomic, copy) NSString *audioForPlay;

@property (weak, nonatomic) IBOutlet UILabel *indexLabel;
@property (weak, nonatomic) IBOutlet UILabel *markedTextLabel;
@property (weak, nonatomic) id<LMNumberedMarkingLabelDelegate>delegate;
@property (strong, nonatomic) Answer *potentialAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnSound;
@property (weak, nonatomic) IBOutlet UIImageView *imgSoundBtn;

@property (nonatomic, assign) BOOL isAnswerExecuted;

@property (assign, nonatomic) __block BOOL isTimerExecuted;

@property (assign, nonatomic) NSInteger labelTag;
@property (assign, nonatomic) BOOL willExecute;

- (void)changeStatus;

- (void)sizeToAnswered;
- (void)drawAnswered;

@end
